﻿using UnityEngine;
using System.Collections;
using System.Runtime.Serialization;
using System;

[Serializable]
public class SerializableV2 : ISerializable  {

    private Vector2 v;

    private const string XValueID = "XValue";
    private const string YValueID = "YValue";

    public Vector2 V
    {
        get
        {
            return v;
        }
    }

    public SerializableV2(Vector2 v)
    {
        this.v = v;
    }

    public SerializableV2(SerializationInfo info, StreamingContext context)
    {
        v.x = (float)info.GetValue(XValueID, typeof(float));
        v.y = (float)info.GetValue(YValueID, typeof(float));
    }

    public void GetObjectData(SerializationInfo info, StreamingContext context)
    {
        info.AddValue(XValueID, v.x);
        info.AddValue(YValueID, v.y);
    }
}
