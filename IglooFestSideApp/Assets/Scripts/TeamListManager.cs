﻿using UnityEngine;
using System.Collections;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public class TeamListManager : MonoBehaviour {

    public static TeamListManager Instance;

    public GameObject[] dropLocations;

    public Button ReadyButton;
    public Button TrashFirstTeamButton;

    public Transform firstTeamLocation;
    public Transform secondTeamLocation;

    public GameObject LeaderboardDatePanel;
    public GameObject EmailAddressPanel;    
    public GameObject InvalidEmailAddressWarning;    

    public InputField LeaderboardDateText;
    public GameObject InvalidDateWarning;
    public InputField CurrentEmailAddress;
    public InputField CurrentName;
    public InputField CurrentGivenName;
    public Toggle CurrentTermsAndConditions;
    [HideInInspector]
    public DragMe objectThatTheEmailBelongsTo;

    public Dictionary<GameObject, float> itemPositions;

    public const string MatchEmailPattern =
            @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
            + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
              + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
            + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";

    public const string MatchDatePattern =
            @"^20([1][6-9]|[2][0-9])-([0][1-9]|[1][0-2])-([0-1-2][0-9]|[3][0-1])$";


    public bool isBetweenLeaderboardAndNextGame = false;

    // Use this for initialization
    void Start ()
    {
        DateTime timeNow = DateTime.Now;
        string monthString = timeNow.Month.ToString();
        if (timeNow.Month < 10)
            monthString = "0" + timeNow.Month;
        string dayString = timeNow.Day.ToString();
        if (timeNow.Day < 10)
            dayString = "0" + timeNow.Day;

        PlayerPrefs.DeleteKey("LeaderboardDate");

        Instance = this;        

        itemPositions = new Dictionary<GameObject, float>();
        
        for (int i = 0; i < dropLocations.Length; i++)
        {
            itemPositions.Add(dropLocations[i], dropLocations[i].GetComponent<RectTransform>().anchoredPosition.y);               
        }

        if (!string.IsNullOrEmpty(PlayerPrefs.GetString("LeaderboardDate")))
            LeaderboardDateText.text = PlayerPrefs.GetString("LeaderboardDate");
        else
            LeaderboardDateText.text = timeNow.Year + "-" + monthString + "-" + dayString;
    }	
    
    void Update ()
    {
        while (true)
        {
            MessageData msg = Server.PopMessage();
            if (msg == null)
                break;

            if (msg.hasTeamStartedPlaying)
            {
                Debug.Log("received message that team has started playing");
                TeamIsCurrentlyPlaying();                
            }
                

            if (msg.hasTeamFinishedPlaying)
            {
                Debug.Log("received message that team has finished playing");
                TeamHasFinishedPlaying();                
            }
                
        }        
    }
    
    public void GoBack()
    {
        Application.LoadLevel("Client");
    }

    public void SendTeamReadyMessage()
    {
        isBetweenLeaderboardAndNextGame = false;
        ReadyButton.interactable = false;
        SendTeamNamesOverNetwork.Instance.FirstTeamName = dropLocations[0].GetComponentInChildren<Text>().text;
        SendTeamNamesOverNetwork.Instance.NextTeamName = dropLocations[1].GetComponentInChildren<Text>().text;
		SendTeamNamesOverNetwork.Instance.CurrentTeamEmail = dropLocations[0].GetComponent<DragMe>().emailAddress;
        SendTeamNamesOverNetwork.Instance.currentTeamLeaderName = dropLocations[0].GetComponent<DragMe>().leaderName;
        SendTeamNamesOverNetwork.Instance.currentTeamLeaderGivenName = dropLocations[0].GetComponent<DragMe>().leaderGivenName;
        SendTeamNamesOverNetwork.Instance.isOkWithTerms = dropLocations[0].GetComponent<DragMe>().isOkWithTerms;
        SendTeamNamesOverNetwork.Instance.IsTeamReady = true;
        SendTeamNamesOverNetwork.Instance.Send();
        Debug.Log(dropLocations[0].GetComponent<DragMe>().leaderName);
    }


	public void ForceResetReadyButon()
	{
		ReadyButton.interactable = true;
	}

    public void OnEndEditSendEmailAddress ()
    {
        if (string.IsNullOrEmpty(CurrentEmailAddress.text) || ValidateEmailAddress(CurrentEmailAddress.text))
        {
            objectThatTheEmailBelongsTo.emailAddress = CurrentEmailAddress.text;
            objectThatTheEmailBelongsTo.leaderName = CurrentName.text;
            objectThatTheEmailBelongsTo.leaderGivenName = CurrentGivenName.text;
            objectThatTheEmailBelongsTo.isOkWithTerms = CurrentTermsAndConditions.isOn;
            EmailAddressPanel.SetActive(false);
        }
        else
            InvalidEmailAddressWarning.SetActive(true);
    }


    public void OnClickAddDay ()
    {
        if (!string.IsNullOrEmpty(LeaderboardDateText.text) && ValidateDate(LeaderboardDateText.text))
        {
            DateTime dateEntered = DateTime.Parse(LeaderboardDateText.text);
            dateEntered = dateEntered.AddDays(1);
            if (dateEntered <= DateTime.Now)
            {
                string monthString = dateEntered.Month.ToString();
                if (dateEntered.Month < 10)
                    monthString = "0" + dateEntered.Month;
                string dayString = dateEntered.Day.ToString();
                if (dateEntered.Day < 10)
                    dayString = "0" + dateEntered.Day;

                LeaderboardDateText.text = dateEntered.Year + "-" + monthString + "-" + dayString;
            }
        }
    }


    public void OnClickRemoveDay ()
    {        
        if (!string.IsNullOrEmpty(LeaderboardDateText.text) && ValidateDate(LeaderboardDateText.text))
        {
            DateTime dateEntered = DateTime.Parse(LeaderboardDateText.text);            
            dateEntered = dateEntered.AddDays(-1);            
            string monthString = dateEntered.Month.ToString();
            if (dateEntered.Month < 10)
                monthString = "0" + dateEntered.Month;
            string dayString = dateEntered.Day.ToString();
            if (dateEntered.Day < 10)
                dayString = "0" + dateEntered.Day;

            LeaderboardDateText.text = dateEntered.Year + "-" + monthString + "-" + dayString;            
        }
    }

    public void OnEndEditSendLeaderboardDate ()
    {
        if (!string.IsNullOrEmpty(LeaderboardDateText.text) && ValidateDate(LeaderboardDateText.text))
        {
            SendTeamNamesOverNetwork.Instance.LeaderboardDate = LeaderboardDateText.text;
            SendTeamNamesOverNetwork.Instance.Send();
            LeaderboardDatePanel.SetActive(false);
            PlayerPrefs.SetString("LeaderboardDate", LeaderboardDateText.text);
            PlayerPrefs.Save();
        }
        else
            InvalidDateWarning.SetActive(true);        
    }

    public bool ValidateEmailAddress(string email)
    {
        if (email != null) return Regex.IsMatch(email, MatchEmailPattern);
        else return false;
    }  

    public bool ValidateDate(string date)
    {
        if (date != null) return Regex.IsMatch(date, MatchDatePattern);
        else return false;
    }
    
    public void TeamIsCurrentlyPlaying ()
    {
        // Lock current team name
        TrashFirstTeamButton.interactable = false;
        dropLocations[0].GetComponent<Button>().interactable = false;
        ReadyButton.interactable = false;

        string firstTeamEmail = dropLocations[0].GetComponent<DragMe>().emailAddress;
        if (string.IsNullOrEmpty(firstTeamEmail))
            SendTeamNamesOverNetwork.Instance.CurrentTeamEmail = "NONE";
        else
            SendTeamNamesOverNetwork.Instance.CurrentTeamEmail = firstTeamEmail;

        SendTeamNamesOverNetwork.Instance.currentTeamLeaderName = dropLocations[0].GetComponent<DragMe>().leaderName;
        SendTeamNamesOverNetwork.Instance.currentTeamLeaderGivenName = dropLocations[0].GetComponent<DragMe>().leaderGivenName;
        SendTeamNamesOverNetwork.Instance.isOkWithTerms = dropLocations[0].GetComponent<DragMe>().isOkWithTerms;


        SendTeamNamesOverNetwork.Instance.IsTeamReady = false;

        Debug.Log("Sending current team email: " + firstTeamEmail);

        SendTeamNamesOverNetwork.Instance.Send();
    }


    public void TeamHasFinishedPlaying ()
    {
        // TODO: Move all teams up one slot
        // Update next team's name
        for (int i = 0; i < dropLocations.Length; i++)
        {
            if ((i + 1) < dropLocations.Length)
            {
                dropLocations[i].GetComponentInChildren<Text>().text = dropLocations[(i + 1)].GetComponentInChildren<Text>().text;
                dropLocations[i].GetComponent<DragMe>().inputTextField.text = dropLocations[i + 1].GetComponent<DragMe>().inputTextField.text;
                dropLocations[i].GetComponent<DragMe>().emailAddress = dropLocations[i + 1].GetComponent<DragMe>().emailAddress;
				dropLocations[i].GetComponent<DragMe>().leaderName = dropLocations[i + 1].GetComponent<DragMe>().leaderName;
				dropLocations[i].GetComponent<DragMe>().leaderGivenName = dropLocations[i + 1].GetComponent<DragMe>().leaderGivenName;
				dropLocations[i].GetComponent<DragMe>().isOkWithTerms = dropLocations[i + 1].GetComponent<DragMe>().isOkWithTerms;
            }                
            else
            {
                dropLocations[i].GetComponentInChildren<Text>().text = "";
                dropLocations[i].GetComponent<DragMe>().inputTextField.text = "";
                dropLocations[i].GetComponent<DragMe>().emailAddress = "";
				dropLocations[i].GetComponent<DragMe>().leaderName = "";
				dropLocations[i].GetComponent<DragMe>().leaderGivenName = "";
				dropLocations[i].GetComponent<DragMe>().isOkWithTerms = false;
            }                
        }

        dropLocations[0].GetComponent<Button>().interactable = true;
        ReadyButton.interactable = true;
        TrashFirstTeamButton.interactable = true;
        isBetweenLeaderboardAndNextGame = true;
    }

}
