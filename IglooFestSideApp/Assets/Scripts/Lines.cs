﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Lines : MonoBehaviour {

    public Image[] Corners;
    public Material Mat;
    public LineRenderer[] Sides;

    private Image corner1;
    private Image corner2;

    // Use this for initialization
    void Start () {
        corner1 = Corners[0];
        corner2 = Corners[1];
    }
	
	// Update is called once per frame
	void Update () {

        Vector3 corner1World = corner1.GetComponent<RectTransform>().position;
        corner1World.z = 0;
        Vector3 corner2World = corner2.GetComponent<RectTransform>().position;
        corner2World.z = 0;

        Vector2 point1 = new Vector2(corner2.GetComponent<RectTransform>().position.x, corner1.GetComponent<RectTransform>().position.y);
        Vector2 point2 = new Vector2(corner1.GetComponent<RectTransform>().position.x, corner2.GetComponent<RectTransform>().position.y);

        Sides[0].SetPosition(0, corner1World);
        Sides[0].SetPosition(1, point1);

        Sides[1].SetPosition(0, point1);
        Sides[1].SetPosition(1, corner2World);

        Sides[2].SetPosition(0, corner2World);
        Sides[2].SetPosition(1, point2);
             
        Sides[3].SetPosition(0, point2);
        Sides[3].SetPosition(1, corner1World);

        Vector2 corner1Screen = Camera.main.WorldToViewportPoint(corner1World);
        Vector2 corner2Screen = Camera.main.WorldToViewportPoint(corner2World);

        MessageData msgData = new MessageData();
        SerializableV2[] corners = new SerializableV2[] { new SerializableV2(corner1Screen), new SerializableV2(corner2Screen) };
        msgData.messageVector2Array = corners;

        Client.singleton.Send(msgData);

    }
}
