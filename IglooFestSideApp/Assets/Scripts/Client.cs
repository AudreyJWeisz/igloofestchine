﻿using UnityEngine;
using System.Collections;
using System.Net.Sockets;
using System.Net;
using System;

public class Client : MonoBehaviour
{
    public UnityEngine.UI.InputField ComputerNameInput;
    public GameObject ErrorMessagePanel;        
    private int port = 0;
    public static Client singleton;
    
    private int connectionAttempts = 0;
    private Socket m_Socket;
    private string computerName = "";

    private int dnsAddressNumber = 0;

    private bool needsToResendMessage = false;
    private MessageData messageToResend = null;

    void Awake ()
    {
        DontDestroyOnLoad(transform.gameObject);
        singleton = this;           
    }

    void Start ()
    {
        StartCoroutine(InitializeVariables());               
    }


    private IEnumerator InitializeVariables()
    {
        while (AppManager.Instance == null)
            yield return null;

        ComputerNameInput.onEndEdit.AddListener(delegate { OnEndEditTryToConnect(); });

        if (AppManager.Instance.ConnectToLaptop)
        {
            ComputerNameInput.transform.parent.gameObject.SetActive(false);
            computerName = "192.168.0.102";
            port = 5000;            
            yield return new WaitForSeconds(1);
            TryToConnectToServer();
        }
        else
        {
            if (!string.IsNullOrEmpty(PlayerPrefs.GetString("ComputerName")))
                ComputerNameInput.text = PlayerPrefs.GetString("ComputerName");            
        }
    }


    void OnApplicationQuit ()
    {
        m_Socket.Close();
        m_Socket = null;
    }

    public void Send (MessageData msgData)
    {
        if (singleton.m_Socket == null)
            return;

        messageToResend = msgData;

        try
        {
            byte[] sendData = MessageData.ToByteArray(msgData);
            byte[] prefix = BitConverter.GetBytes(sendData.Length);
            singleton.m_Socket.Send(prefix);
            singleton.m_Socket.Send(sendData);
        }
        catch (SocketException e)
        {
            needsToResendMessage = true;
            Debug.Log("An exception occurred!!! :(  Message is as follows: " + e.Message);
            if (AppManager.Instance.ConsoleText != null)
                AppManager.Instance.ConsoleText.text += "An exception occurred!!! :(  Message is as follows: " + e.Message + "\n";
            TryToConnectToServer();
        }        
    }

    public void OnEndEditTryToConnect ()
    {
		Debug.Log ("Try To connect");
        computerName = ComputerNameInput.text;
        port = 5000;
        PlayerPrefs.SetString("ComputerName", ComputerNameInput.text);
        PlayerPrefs.Save();
        TryToConnectToServer();        
    }

    private bool isLoading = false;

    private void TryToConnectToServer ()
    {
        m_Socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        connectionAttempts++;
        if (!string.IsNullOrEmpty(computerName) || AppManager.Instance.ConnectToLaptop)
        {
            try
            {                
            // System.Net.PHostEntry ipHostInfo = Dns.Resolve("host.contoso.com");
            // System.Net.IPAddress remoteIPAddress = ipHostInfo.AddressList[0];  
                 
                System.Net.IPAddress remoteIPAddress = Dns.GetHostAddresses(computerName)[dnsAddressNumber];
                
                if (AppManager.Instance.ConsoleText != null)
                    AppManager.Instance.ConsoleText.text += "trying to connect to : " + remoteIPAddress + "\n";
                System.Net.IPEndPoint remoteEndPoint = new System.Net.IPEndPoint(remoteIPAddress, port);
                m_Socket.Connect(remoteEndPoint);    
                
                                            
                SendTeamNamesOverNetwork.Instance.DeviceName = Network.player.ipAddress;
                if (TeamListManager.Instance != null)
                {
                    SendTeamNamesOverNetwork.Instance.FirstTeamName = TeamListManager.Instance.dropLocations[0].GetComponentInChildren<UnityEngine.UI.Text>().text;
                    SendTeamNamesOverNetwork.Instance.NextTeamName = TeamListManager.Instance.dropLocations[1].GetComponentInChildren<UnityEngine.UI.Text>().text;
                }
                SendTeamNamesOverNetwork.Instance.Send();
                if (ComputerNameInput != null)
                    ComputerNameInput.transform.parent.gameObject.SetActive(false);
                
                if (AppManager.Instance.ConsoleText != null)
                    AppManager.Instance.ConsoleText.text += "Successfully connected. Sent : " + Network.player.ipAddress + "\n";

                if (needsToResendMessage && messageToResend != null)
                {
                    needsToResendMessage = false;
                    Send(messageToResend);
                    messageToResend = null;                  
                }

                connectionAttempts = 0;
                            
                if (AppManager.Instance != null && !Application.loadedLevelName.Contains("ClientTeams"))
                    StartCoroutine(AppManager.Instance.GoToTeamManagement());

            }
            catch (Exception e)
            {
                if (ComputerNameInput != null && dnsAddressNumber >= (Dns.GetHostAddresses(computerName.ToUpper()).Length - 1))
                {                    
                    ComputerNameInput.text = "";
                    ErrorMessagePanel.SetActive(true);                    
                    ComputerNameInput.transform.parent.gameObject.SetActive(true);
                    if (AppManager.Instance.ConsoleText != null)
                        AppManager.Instance.ConsoleText.text += "Problem with connection : " + e.Message + "\n";                                        
                }                
                else if (connectionAttempts < 10)
                {
                    if (dnsAddressNumber < Dns.GetHostAddresses(computerName.ToUpper()).Length)
                        dnsAddressNumber++;

                    if (TeamListManager.Instance != null)
                    {
                        TeamListManager.Instance.TrashFirstTeamButton.interactable = true;
                        TeamListManager.Instance.dropLocations[0].GetComponent<UnityEngine.UI.Button>().interactable = true;
                        TeamListManager.Instance.ReadyButton.interactable = true;
                    }                    
                    if (AppManager.Instance.ConsoleText != null)
                        AppManager.Instance.ConsoleText.text += "trying to connect again to " + computerName + "! Because of " + e.Message + "\n";
                    m_Socket.Close();
                    m_Socket = null;
                    m_Socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    StartCoroutine(WaitForOneSecondAndTryAgain());                    
                }
                else
                {                    
                    if (AppManager.Instance.ConsoleText != null)
                        AppManager.Instance.ConsoleText.text += "reached maximum connection attempts!\n";
                }
                    
            }
        }
        else if (string.IsNullOrEmpty(computerName) && !AppManager.Instance.ConnectToLaptop)
        {            
            ErrorMessagePanel.SetActive(true);
            ComputerNameInput.transform.parent.gameObject.SetActive(true);            
            if (AppManager.Instance.ConsoleText != null)
                AppManager.Instance.ConsoleText.text += "Computer name was empty\n";
        }        
    }


    private IEnumerator WaitForOneSecondAndTryAgain()
    {
        yield return new WaitForSeconds(1f);
        TryToConnectToServer();
    }
}