﻿using UnityEngine;
using System.Collections;

public class AppManager : MonoBehaviour {

    public static AppManager Instance;
    public UnityEngine.UI.Text ConsoleText;
    public bool ShowConsoleText = false;
    public bool ConnectToLaptop = false;

    void Awake ()
    {
        Instance = this;
        if (ShowConsoleText)
            ConsoleText.gameObject.SetActive(true);
        else
            ConsoleText.gameObject.SetActive(false);
    }

	public IEnumerator GoToTeamManagement()
    {
        yield return new WaitForEndOfFrame();
        Application.LoadLevel("ClientTeams");
    }
}
