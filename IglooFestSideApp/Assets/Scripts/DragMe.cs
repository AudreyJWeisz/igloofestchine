using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class DragMe : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
	public bool dragOnSurfaces = true;    
    public Image receivingImage;
    public Text receivingText;
    public InputField inputTextField;
    
    public string emailAddress;
    public string leaderName;
    public string leaderGivenName;
    public bool isOkWithTerms;

   
    private Dictionary<int,GameObject> m_DraggingIcons = new Dictionary<int, GameObject>();
	private Dictionary<int, RectTransform> m_DraggingPlanes = new Dictionary<int, RectTransform>();

    private CanvasGroup group;

    private bool firstTrashIsEnabled = false;

    public void OnEnable ()
    {
        GetComponent<Button>().onClick.AddListener(delegate{ OnClickShowInputField(); });
        inputTextField.onEndEdit.AddListener(delegate{ OnEndEditHideInputField(); });
    }

    public void OnBeginDrag(PointerEventData eventData)
	{
        Text initialText = GetComponentInChildren<Text>();
        float initialTextWidth = initialText.GetComponent<RectTransform>().sizeDelta.x;
        float initialTextHeight = initialText.GetComponent<RectTransform>().sizeDelta.y;
        var canvas = FindInParents<Canvas>(gameObject);
		if (canvas == null)
			return;

		// We have clicked something that can be dragged.
		// What we want to do is create an icon for this.
		m_DraggingIcons[eventData.pointerId] = new GameObject("icon");

		m_DraggingIcons[eventData.pointerId].transform.SetParent (canvas.transform, false);
		m_DraggingIcons[eventData.pointerId].transform.SetAsLastSibling();
		
		var image = m_DraggingIcons[eventData.pointerId].AddComponent<Image>();
        var textContainer = new GameObject();
        Text textContent = textContainer.AddComponent<Text>();
        
        textContent.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, initialTextWidth);
        textContent.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, initialTextHeight);
        textContent.text = initialText.text;
        textContent.alignment = initialText.alignment;
        textContent.resizeTextForBestFit = initialText.resizeTextForBestFit;
        textContent.color = initialText.color;
        textContent.font = initialText.font;
        textContent.fontSize = initialText.fontSize;
        textContent.fontStyle = initialText.fontStyle;
        textContainer.transform.SetParent(m_DraggingIcons[eventData.pointerId].transform);
        textContainer.transform.localPosition = Vector3.zero;
        textContent.transform.localScale = initialText.transform.localScale;        

        // The icon will be under the cursor.
        // We want it to be ignored by the event system.
        group = m_DraggingIcons[eventData.pointerId].AddComponent<CanvasGroup>();
		group.blocksRaycasts = false;

		image.sprite = GetComponent<Image>().sprite;
        float imageHeight = GetComponent<RectTransform>().sizeDelta.y;
        float imageWidth = GetComponent<RectTransform>().sizeDelta.x;
        m_DraggingIcons[eventData.pointerId].GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, imageWidth);
        m_DraggingIcons[eventData.pointerId].GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, imageHeight);        
        image.type = Image.Type.Sliced;

        if (dragOnSurfaces)
			m_DraggingPlanes[eventData.pointerId] = transform as RectTransform;
		else
			m_DraggingPlanes[eventData.pointerId]  = canvas.transform as RectTransform;

        SetDraggedPosition(eventData);
	}

	public void OnDrag(PointerEventData eventData)
	{
		if (m_DraggingIcons[eventData.pointerId] != null)
			SetDraggedPosition(eventData);
	}

	private void SetDraggedPosition(PointerEventData eventData)
	{
		if (dragOnSurfaces && eventData.pointerEnter != null && eventData.pointerEnter.transform as RectTransform != null)
			m_DraggingPlanes[eventData.pointerId] = eventData.pointerEnter.transform as RectTransform;
		
		var rt = m_DraggingIcons[eventData.pointerId].GetComponent<RectTransform>();
		Vector3 globalMousePos;
		if (RectTransformUtility.ScreenPointToWorldPointInRectangle(m_DraggingPlanes[eventData.pointerId], eventData.position, eventData.pressEventCamera, out globalMousePos))
		{
			rt.position = globalMousePos;
			rt.rotation = m_DraggingPlanes[eventData.pointerId].rotation;
		}
	}

	public void OnEndDrag(PointerEventData eventData)
	{
        float objectHeight = m_DraggingIcons[eventData.pointerId].GetComponent<RectTransform>().sizeDelta.y;
        objectHeight += (objectHeight/3); // so the user doesn't have to be SUPER precise when dropping the button
        float objectPosY = m_DraggingIcons[eventData.pointerId].gameObject.GetComponent<RectTransform>().anchoredPosition.y;

        bool isFirstTeam = false;
        bool isSecondTeam = false;

        foreach (KeyValuePair<GameObject, float> itemPos in TeamListManager.Instance.itemPositions)
        {            
            if (objectPosY >= (itemPos.Value - (objectHeight/2)) && objectPosY <= (itemPos.Value + (objectHeight/2)))
            {
                if (itemPos.Value == TeamListManager.Instance.firstTeamLocation.GetComponent<RectTransform>().anchoredPosition.y)
                    isFirstTeam = true;

                if (itemPos.Value == TeamListManager.Instance.secondTeamLocation.GetComponent<RectTransform>().anchoredPosition.y)
                    isSecondTeam = true;

                itemPos.Key.GetComponent<DragMe>().OnDrop(eventData, isFirstTeam, isSecondTeam);
                break;
            }
        }

		if (m_DraggingIcons[eventData.pointerId] != null)
			Destroy(m_DraggingIcons[eventData.pointerId]);

		m_DraggingIcons[eventData.pointerId] = null;
        group.blocksRaycasts = true;
    }

	static public T FindInParents<T>(GameObject go) where T : Component
	{
		if (go == null) return null;
		var comp = go.GetComponent<T>();

		if (comp != null)
			return comp;
		
		var t = go.transform.parent;
		while (t != null && comp == null)
		{
			comp = t.gameObject.GetComponent<T>();
			t = t.parent;
		}
		return comp;
	}


    public void OnDrop (PointerEventData data, bool isFirstTeam, bool isSecondTeam)
    {
		Debug.Log ("ondrop");
        if (receivingImage == null)
            return;

        if (!receivingImage.gameObject.GetComponent<Button>().interactable)
            return;

        GameObject receivedData = GetDropData(data);
        Image droppedImage = receivedData.GetComponent<Image>();
        if (droppedImage != null)
        {            
            receivingImage.sprite = droppedImage.sprite;
            receivingImage.color = receivedData.GetComponent<Image>().color;
            float imageHeight = receivedData.GetComponent<RectTransform>().sizeDelta.y;
            float imageWidth = receivedData.GetComponent<RectTransform>().sizeDelta.x;
            receivingImage.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, imageWidth);
            receivingImage.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, imageHeight);
            receivingImage.type = Image.Type.Sliced;
            
            Text receivedText = GetDropData(data).GetComponentInChildren<Text>();
            
            string previousText = receivingText.text;
            string previousEmail = emailAddress;
			string previousLeaderName = leaderName;
			string previousLeaderGivenName = leaderGivenName;
			bool previousOkWithTerms = isOkWithTerms;
			//string previous

            emailAddress = GetDropData(data).GetComponent<DragMe>().emailAddress;
            leaderName = GetDropData(data).GetComponent<DragMe>().leaderName;
            leaderGivenName = GetDropData(data).GetComponent<DragMe>().leaderGivenName;
            isOkWithTerms = GetDropData(data).GetComponent<DragMe>().isOkWithTerms;
            receivingText.text = receivedText.text;
            receivingText.alignment = receivedText.alignment;
            receivingText.color = receivedText.color;
            receivingText.font = receivedText.font;
            receivingText.fontSize = receivedText.fontSize;
            receivingText.fontStyle = receivedText.fontStyle;
            inputTextField.text = receivedText.text;
            receivedText.text = previousText;
            GetDropData(data).GetComponent<DragMe>().inputTextField.text = previousText;
            GetDropData(data).GetComponent<DragMe>().emailAddress = previousEmail;
			GetDropData (data).GetComponent<DragMe> ().leaderName = previousLeaderName;
			GetDropData (data).GetComponent<DragMe> ().leaderGivenName = previousLeaderGivenName;
			GetDropData (data).GetComponent<DragMe> ().isOkWithTerms = previousOkWithTerms;

            if (isFirstTeam && !TeamListManager.Instance.isBetweenLeaderboardAndNextGame)
            {   
                SendTeamNamesOverNetwork.Instance.FirstTeamName = receivingText.text;
                SendTeamNamesOverNetwork.Instance.IsTeamReady = false;
                SendTeamNamesOverNetwork.Instance.CurrentTeamEmail = "";
                SendTeamNamesOverNetwork.Instance.Send();                
            }
            else if (isSecondTeam && !TeamListManager.Instance.isBetweenLeaderboardAndNextGame)
            {
                SendTeamNamesOverNetwork.Instance.NextTeamName = receivingText.text;
                SendTeamNamesOverNetwork.Instance.IsTeamReady = false;
                SendTeamNamesOverNetwork.Instance.CurrentTeamEmail = "";
                SendTeamNamesOverNetwork.Instance.Send();
            }
            else if (isFirstTeam && TeamListManager.Instance.isBetweenLeaderboardAndNextGame)
            {
                SendTeamNamesOverNetwork.Instance.NextTeamName = receivingText.text;
                SendTeamNamesOverNetwork.Instance.IsTeamReady = false;
                SendTeamNamesOverNetwork.Instance.CurrentTeamEmail = "";
                SendTeamNamesOverNetwork.Instance.Send();                
            }


            if (GetDropData(data).gameObject.name.Contains("1") && !string.IsNullOrEmpty(receivedText.text) && !TeamListManager.Instance.isBetweenLeaderboardAndNextGame)
            {
                SendTeamNamesOverNetwork.Instance.FirstTeamName = receivedText.text;
                SendTeamNamesOverNetwork.Instance.IsTeamReady = false;
                SendTeamNamesOverNetwork.Instance.CurrentTeamEmail = "";
                SendTeamNamesOverNetwork.Instance.Send();                
            }
            else if (GetDropData(data).gameObject.name.Contains("2") && !string.IsNullOrEmpty(receivedText.text) && !TeamListManager.Instance.isBetweenLeaderboardAndNextGame)
            {
                SendTeamNamesOverNetwork.Instance.NextTeamName = receivedText.text;
                SendTeamNamesOverNetwork.Instance.CurrentTeamEmail = "";
                SendTeamNamesOverNetwork.Instance.IsTeamReady = false;
                SendTeamNamesOverNetwork.Instance.Send();
            }
            else if (GetDropData(data).gameObject.name.Contains("1") && !string.IsNullOrEmpty(receivedText.text) && TeamListManager.Instance.isBetweenLeaderboardAndNextGame)
            {
                SendTeamNamesOverNetwork.Instance.NextTeamName = receivedText.text;
                SendTeamNamesOverNetwork.Instance.CurrentTeamEmail = "";
                SendTeamNamesOverNetwork.Instance.IsTeamReady = false;
                SendTeamNamesOverNetwork.Instance.Send();
            }
        }
        
    }


    public void RemoveObject ()
    {
        receivingText.text = "";
        inputTextField.text = "";
        emailAddress = "";
		leaderName = "";
		leaderGivenName = "";
		isOkWithTerms = false;

        if (gameObject.name.Contains("1") && !TeamListManager.Instance.isBetweenLeaderboardAndNextGame)
        {
            SendTeamNamesOverNetwork.Instance.FirstTeamName = "";
            SendTeamNamesOverNetwork.Instance.IsTeamReady = false;
            SendTeamNamesOverNetwork.Instance.Send();
        }
        else if (gameObject.name.Contains("2") && TeamListManager.Instance.isBetweenLeaderboardAndNextGame)
        {
            SendTeamNamesOverNetwork.Instance.NextTeamName = "";
            SendTeamNamesOverNetwork.Instance.IsTeamReady = false;
            SendTeamNamesOverNetwork.Instance.Send();
        }
        else if (gameObject.name.Contains("1") && TeamListManager.Instance.isBetweenLeaderboardAndNextGame)
        {
            SendTeamNamesOverNetwork.Instance.NextTeamName = "";
            SendTeamNamesOverNetwork.Instance.IsTeamReady = false;
            SendTeamNamesOverNetwork.Instance.Send();
        }
    }

    private GameObject GetDropData (PointerEventData data)
    {
        var originalObj = data.pointerDrag;
        if (originalObj == null)
            return null;

        var dragMe = originalObj.GetComponent<DragMe>();
        if (dragMe == null)
            return null;

        var srcImage = originalObj.GetComponent<Image>();
        if (srcImage == null)
            return null;
        
        return originalObj;
    }

    public void OnClickShowInputField()
    {
        firstTrashIsEnabled = TeamListManager.Instance.TrashFirstTeamButton.IsInteractable();
        foreach (GameObject button in GameObject.FindGameObjectsWithTag("TrashButton"))
        {
            button.GetComponent<Button>().interactable = false;
        }

        inputTextField.gameObject.SetActive(true);        
    }

    public void OnEndEditHideInputField ()
    {        
        receivingText.text = inputTextField.text;
        
        if (gameObject.name.Contains("1") && !TeamListManager.Instance.isBetweenLeaderboardAndNextGame)
        {            
            SendTeamNamesOverNetwork.Instance.FirstTeamName = receivingText.text;
            SendTeamNamesOverNetwork.Instance.IsTeamReady = false;
            SendTeamNamesOverNetwork.Instance.Send();
            
        }
        else if (gameObject.name.Contains("2") && !TeamListManager.Instance.isBetweenLeaderboardAndNextGame)
        {
            SendTeamNamesOverNetwork.Instance.NextTeamName = receivingText.text;
            SendTeamNamesOverNetwork.Instance.IsTeamReady = false;
            SendTeamNamesOverNetwork.Instance.Send();
        }
        else if (gameObject.name.Contains("1") && TeamListManager.Instance.isBetweenLeaderboardAndNextGame)
        {
            SendTeamNamesOverNetwork.Instance.NextTeamName = receivingText.text;
            SendTeamNamesOverNetwork.Instance.IsTeamReady = false;
            SendTeamNamesOverNetwork.Instance.Send();            
        }

        TeamListManager.Instance.EmailAddressPanel.SetActive(true);
        TeamListManager.Instance.objectThatTheEmailBelongsTo = this;
        TeamListManager.Instance.CurrentEmailAddress.text = "";
        TeamListManager.Instance.CurrentName.text = "";
        TeamListManager.Instance.CurrentGivenName.text = "";
        TeamListManager.Instance.CurrentTermsAndConditions.isOn = false;

     inputTextField.gameObject.SetActive(false);

        foreach (GameObject button in GameObject.FindGameObjectsWithTag("TrashButton"))
        {
            button.GetComponent<Button>().interactable = true;
        }

        if (!firstTrashIsEnabled)
            TeamListManager.Instance.TrashFirstTeamButton.interactable = false;

    }

}
