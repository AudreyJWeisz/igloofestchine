﻿using UnityEngine;
using System.Collections;

public class SendTeamNamesOverNetwork : MonoBehaviour {

    public static SendTeamNamesOverNetwork Instance;

    [HideInInspector]
    public string DeviceName, FirstTeamName, NextTeamName, CurrentTeamEmail,currentTeamLeaderName,currentTeamLeaderGivenName, LeaderboardDate = "";
    [HideInInspector]
    public bool IsTeamReady = false;
    [HideInInspector]
    public bool isOkWithTerms = false;

    // Use this for initialization
    void Start () {
        Instance = this;
    }
	
	// Update is called once per frame
	public void Send ()
    {
        MessageData msgData = new MessageData();
        msgData.sideAppDeviceName = DeviceName;
        msgData.currentTeamName = FirstTeamName;
        msgData.nextTeamName = NextTeamName;
        msgData.isTeamReady = IsTeamReady;
        msgData.currentTeamEmail = CurrentTeamEmail;
        msgData.isOkWithTerms = isOkWithTerms;
        msgData.currentTeamLeaderName = currentTeamLeaderName;
        msgData.currentTeamLeaderGivenName = currentTeamLeaderGivenName;
        msgData.leaderboardDate = LeaderboardDate;
        Debug.Log("sent message : " + msgData.currentTeamName + ", " + msgData.nextTeamName + ", " + msgData.isTeamReady + ", " + msgData.currentTeamEmail + ", leader name" + msgData.currentTeamLeaderName+ ", " + msgData.leaderboardDate);
        Client.singleton.Send(msgData);        
    }
}
