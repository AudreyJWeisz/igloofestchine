﻿using UnityEngine;

using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace OpenCVForUnity
{
		abstract public class DisposableObject : IDisposable
		{

				public bool IsDisposed { get; protected set; }
		
				public bool IsEnabledDispose { get; set; }
				
				protected DisposableObject ()
            : this(true)
				{
				}
			
				protected DisposableObject (bool isEnabledDispose)
				{
						IsDisposed = false;
						IsEnabledDispose = isEnabledDispose;
				}

				public void Dispose ()
				{
						Dispose (true);
						GC.SuppressFinalize (this);
				}

				protected virtual void Dispose (bool disposing)
				{
						if (!IsDisposed) {             
								// releases managed resources
								if (disposing) {
								}

								IsDisposed = true;
						}
				}

				~DisposableObject ()
				{
						Dispose (false);
				}

				public void ThrowIfDisposed ()
				{
						if (IsDisposed) 
								throw new ObjectDisposedException (GetType ().FullName);
				}
      
		}
}
