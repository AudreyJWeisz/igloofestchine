﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(GameParameters))]
class GameParamsDrawer : Editor
{
    private static bool isDataLoaded;

    public override void OnInspectorGUI()
    {
        GameParameters parameters = target as GameParameters;
        if (!isDataLoaded)
        {
            parameters.LoadData();
            isDataLoaded = true;
        }
        DrawDefaultInspector();
        if (GUILayout.Button("Save"))
        {
            parameters.SaveData();
        }
        if (GUILayout.Button("Load"))
        {
            parameters.LoadData();
        }
    }
}