/* Copyright (c) 2017 ExT (V.Sigalkin) */

using UnityEngine;
using UnityEditor;

using System;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;

using extOSC.Core;
using extOSC.Core.Internals;

namespace extOSC.Editor
{
    public static class OSCEditorLayout
    {
        #region Static Private Vars

        private static Dictionary<OSCMessage, OSCValueType> _valueTypeTemp = new Dictionary<OSCMessage, OSCValueType>();

        private static readonly GUIContent _transmitterAddressContent = new GUIContent("OSC Transmitter Address:");

        private static readonly GUIContent _transmitterAddressContentSmall = new GUIContent("Transmitter Address:");

        private static readonly GUIContent _receiverAddressContent = new GUIContent("OSC Receiver Address:");

        private static readonly GUIContent _receiverAddressContentSmall = new GUIContent("Receiver Address:");

        private static readonly GUIContent _transmitterContent = new GUIContent("OSC Transmitter:");

        private static readonly GUIContent _receiverContent = new GUIContent("OSC Receiver:");

        private static readonly GUIContent _bundleEmptyContent = new GUIContent("Bundle is empty!");

        private static readonly GUIContent _bundleContent = new GUIContent("Bundle:");

        private static readonly GUIContent _addressContent = new GUIContent("Address:");

        private static readonly GUIContent _addBundleContent = new GUIContent("Add Bundle");

        private static readonly GUIContent _addMessageContent = new GUIContent("Add Message");

        private static readonly GUIContent _addValueContent = new GUIContent("Add Value");

        #endregion

        #region Static Public Methods

        public static void DrawLogo()
        {
            if (OSCEditorTextures.IronWall != null)
            {
                EditorGUILayout.Space();

                var rect = GUILayoutUtility.GetRect(0, 0);
                var width = OSCEditorTextures.IronWall.width * 0.2f;
                var height = OSCEditorTextures.IronWall.height * 0.2f;

                rect.x = rect.width * 0.5f - width * 0.5f;
                rect.y = rect.y + rect.height * 0.5f - height * 0.5f;
                rect.width = width;
                rect.height = height;

                GUI.DrawTexture(rect, OSCEditorTextures.IronWall);
                EditorGUILayout.Space();
            }
        }

        public static OSCReceiver ReceiversPopup(OSCReceiver receiver, GUIContent content)
        {
            return OSCPopup(OSCEditorUtils.GetReceivers(), receiver, content);
        }

        public static void ReceiversPopup(SerializedProperty property, GUIContent content)
        {
            property.objectReferenceValue = OSCPopup(OSCEditorUtils.GetReceivers(), property.objectReferenceValue as OSCReceiver, content);
        }

        public static OSCTransmitter TransmittersPopup(OSCTransmitter transmitter, GUIContent content)
        {
            return OSCPopup(OSCEditorUtils.GetTransmitters(), transmitter, content);
        }

        public static void TransmittersPopup(SerializedProperty property, GUIContent content)
        {
            property.objectReferenceValue = OSCPopup(OSCEditorUtils.GetTransmitters(), property.objectReferenceValue as OSCTransmitter, content);
        }

        public static string PropertiesPopup(object target, string property, Type propertyType, GUIContent content)
        {
            var properties = OSCReflection.GetProperties(target, propertyType);
            var propertiesName = new List<string>();
            var currentIndex = 0;

            for (int index = 0; index < properties.Length; index++)
            {
                if (properties[index] is PropertyInfo)
                {
                    var propertyInfo = properties[index] as PropertyInfo;

                    if (!propertyInfo.CanWrite || !propertyInfo.CanRead)
                    {
                        continue;
                    }
                }

                if (properties[index].Name == property)
                {
                    currentIndex = index;
                }

                propertiesName.Add(properties[index].Name);
            }

            if (propertiesName.Count <= 0)
            {
                propertiesName.Add("- None -");
            }

            var names = propertiesName.ToArray();

            if (content != null)
            {
                var contentNames = new GUIContent[names.Length];

                for (var index = 0; index < names.Length; index++)
                {
                    contentNames[index] = new GUIContent(propertiesName[index]);
                }

                currentIndex = EditorGUILayout.Popup(content, currentIndex, contentNames);
            }
            else
            {
                currentIndex = EditorGUILayout.Popup(currentIndex, names);
            }

            return propertiesName[currentIndex];
        }

        public static void Packet(OSCPacket packet)
        {
            if (packet.IsBundle())
            {
                var bundle = packet as OSCBundle;
                if (bundle != null)
                {

                    if (bundle.Packets.Count > 0)
                    {
                        foreach (var bundlePacket in bundle.Packets)
                        {
                            EditorGUILayout.LabelField(_bundleContent, EditorStyles.boldLabel);

                            EditorGUILayout.BeginVertical("box");
                            Packet(bundlePacket);
                            EditorGUILayout.EndVertical();
                        }
                    }
                    else
                    {
                        EditorGUILayout.BeginVertical("box");
                        EditorGUILayout.LabelField(_bundleEmptyContent, OSCEditorStyles.CenterLabel);
                        EditorGUILayout.EndVertical();
                    }
                }
            }
            else
            {
                var message = packet as OSCMessage;
                if (message != null)
                {
                    EditorGUILayout.LabelField(_addressContent, EditorStyles.boldLabel);

                    EditorGUILayout.BeginVertical("box");
                    EditorGUILayout.SelectableLabel(message.Address, GUILayout.Height(EditorGUIUtility.singleLineHeight));
                    EditorGUILayout.EndVertical();

                    if (message.Values.Count > 0)
                    {
                        var firstColumn = 40f;
                        var secondColumn = 60f;

                        EditorGUILayout.LabelField(string.Format("Values ({0}):", message.GetTags()), EditorStyles.boldLabel);

                        EditorGUILayout.BeginVertical();

                        foreach (var value in message.Values)
                        {
                            // FIRST COLUMN
                            EditorGUILayout.BeginHorizontal();
                            EditorGUILayout.BeginVertical("box");

                            GUILayout.Label(string.Format("Tag: {0}", value.GetTag()), OSCEditorStyles.CenterLabel, GUILayout.Width(firstColumn));

                            EditorGUILayout.EndVertical();

                            EditorGUILayout.BeginHorizontal();

                            if (value.Type == OSCValueType.Blob ||
                                value.Type == OSCValueType.Impulse ||
                                value.Type == OSCValueType.Null)
                            {
                                EditorGUILayout.BeginHorizontal("box");
                                EditorGUILayout.LabelField(value.Type.ToString(), OSCEditorStyles.CenterLabel);
                                EditorGUILayout.EndHorizontal();
                            }
                            else
                            {
                                EditorGUILayout.BeginHorizontal("box");
                                EditorGUILayout.LabelField(value.Type + ":", GUILayout.Width(secondColumn));
                                EditorGUILayout.EndHorizontal();

                                EditorGUILayout.BeginHorizontal("box");

                                switch (value.Type)
                                {
                                    case OSCValueType.Color:
                                        EditorGUILayout.ColorField(value.ColorValue);
                                        break;
                                    case OSCValueType.True:
                                    case OSCValueType.False:
                                        EditorGUILayout.Toggle(value.BoolValue);
                                        break;
                                    default:
                                        EditorGUILayout.SelectableLabel(value.Value.ToString(), GUILayout.Height(EditorGUIUtility.singleLineHeight));
                                        break;
                                }
                                EditorGUILayout.EndHorizontal();
                            }

                            EditorGUILayout.EndHorizontal();

                            EditorGUILayout.EndHorizontal();
                        }

                        EditorGUILayout.EndVertical();
                    }
                }
            }
        }

        public static void EditablePacket(OSCPacket packet)
        {
            var defaultColor = GUI.color;

            if (packet.IsBundle())
            {
                var bundle = packet as OSCBundle;
                if (bundle != null)
                {
                    if (bundle.Packets.Count > 0)
                    {
                        OSCPacket removePacket = null;

                        foreach (var bundlePacket in bundle.Packets)
                        {
                            EditorGUILayout.BeginHorizontal();
                            EditorGUILayout.LabelField(bundlePacket.GetType().Name + ":", EditorStyles.boldLabel);

                            GUI.color = Color.red;

                            var deleteButton = GUILayout.Button("x", GUILayout.Height(EditorGUIUtility.singleLineHeight), GUILayout.Width(20));
                            if (deleteButton)
                            {
                                removePacket = bundlePacket;
                            }

                            GUI.color = defaultColor;

                            EditorGUILayout.EndHorizontal();

                            EditorGUILayout.BeginVertical("box");
                            EditablePacket(bundlePacket);
                            EditorGUILayout.EndVertical();

                            GUILayout.Space(10);
                        }

                        if (removePacket != null)
                        {
                            bundle.Packets.Remove(removePacket);
                        }
                    }
                    else
                    {
                        EditorGUILayout.BeginVertical("box");
                        EditorGUILayout.LabelField(_bundleEmptyContent, OSCEditorStyles.CenterLabel);
                        EditorGUILayout.EndVertical();
                    }

                    // ADD PACKET
                    EditorGUILayout.BeginHorizontal("box");
                    GUI.color = Color.green;

                    if (GUILayout.Button(_addBundleContent))
                    {
                        bundle.AddPacket(new OSCBundle());
                    }

                    if (GUILayout.Button(_addMessageContent))
                    {
                        bundle.AddPacket(new OSCMessage("/address"));
                    }

                    GUI.color = defaultColor;
                    EditorGUILayout.EndHorizontal();
                }
            }
            else
            {
                var message = packet as OSCMessage;
                if (message != null)
                {
                    EditorGUILayout.LabelField(_addressContent, EditorStyles.boldLabel);

                    EditorGUILayout.BeginVertical("box");
                    message.Address = EditorGUILayout.TextField(message.Address, GUILayout.MaxHeight(EditorGUIUtility.singleLineHeight));
                    EditorGUILayout.EndVertical();

                    OSCValue includeValue = null;

                    if (message.Values.Count > 0)
                    {
                        OSCValue removeValue = null;

                        var firstColumn = 40f;
                        var secondColumn = 60f;

                        EditorGUILayout.LabelField(string.Format("Values ({0}):", message.GetTags()), EditorStyles.boldLabel);

                        EditorGUILayout.BeginVertical();

                        foreach (var value in message.Values)
                        {
                            // FIRST COLUMN
                            EditorGUILayout.BeginHorizontal();
                            EditorGUILayout.BeginVertical("box");

                            GUILayout.Label(string.Format("Tag: {0}", value.GetTag()), OSCEditorStyles.CenterLabel, GUILayout.Width(firstColumn));

                            EditorGUILayout.EndVertical();

                            EditorGUILayout.BeginHorizontal();

                            if (value.Type == OSCValueType.Blob ||
                                value.Type == OSCValueType.Impulse ||
                                value.Type == OSCValueType.Null)
                            {
                                EditorGUILayout.BeginHorizontal("box");
                                EditorGUILayout.LabelField(value.Type.ToString(), OSCEditorStyles.CenterLabel);
                                EditorGUILayout.EndHorizontal();
                            }
                            else
                            {
                                EditorGUILayout.BeginHorizontal("box");
                                EditorGUILayout.LabelField(value.Type + ":", GUILayout.Width(secondColumn));
                                EditorGUILayout.EndHorizontal();


                                EditorGUILayout.BeginHorizontal("box");

                                switch (value.Type)
                                {
                                    case OSCValueType.Color:
                                        value.ColorValue = EditorGUILayout.ColorField(value.ColorValue);
                                        break;
                                    case OSCValueType.True:
                                    case OSCValueType.False:
                                        value.BoolValue = EditorGUILayout.Toggle(value.BoolValue);
                                        break;
                                    case OSCValueType.Char:
                                        var rawString = EditorGUILayout.TextField(value.CharValue.ToString());
                                        value.CharValue = rawString.Length > 0 ? rawString[0] : ' ';
                                        break;
                                    case OSCValueType.Int:
                                        value.IntValue = EditorGUILayout.IntField(value.IntValue);
                                        break;
                                    case OSCValueType.Double:
                                        value.DoubleValue = EditorGUILayout.DoubleField(value.DoubleValue);
                                        break;
                                    case OSCValueType.Long:
                                        value.LongValue = EditorGUILayout.LongField(value.LongValue);
                                        break;
                                    case OSCValueType.Float:
                                        value.FloatValue = EditorGUILayout.FloatField(value.FloatValue);
                                        break;
                                    case OSCValueType.String:
                                        value.StringValue = EditorGUILayout.TextField(value.StringValue);
                                        break;
                                    default:
                                        EditorGUILayout.SelectableLabel(value.Value.ToString(), GUILayout.Height(EditorGUIUtility.singleLineHeight));
                                        break;
                                }

                                EditorGUILayout.EndHorizontal();
                            }

                            EditorGUILayout.EndHorizontal();

                            EditorGUILayout.BeginVertical("box");

                            GUI.color = Color.red;

                            var deleteButton = GUILayout.Button("x", GUILayout.Height(EditorGUIUtility.singleLineHeight), GUILayout.Width(20));
                            if (deleteButton)
                            {
                                removeValue = value;
                            }

                            GUI.color = defaultColor;

                            EditorGUILayout.EndVertical();

                            EditorGUILayout.EndHorizontal();
                        }

                        EditorGUILayout.EndVertical();

                        if (removeValue != null)
                        {
                            message.Values.Remove(removeValue);
                        }
                    }

                    // ADD VALUE
                    EditorGUILayout.BeginHorizontal("box");

                    if (!_valueTypeTemp.ContainsKey(message))
                    {
                        _valueTypeTemp.Add(message, OSCValueType.Unknown);
                    }

                    _valueTypeTemp[message] = (OSCValueType)EditorGUILayout.EnumPopup(_valueTypeTemp[message]);

                    GUI.color = Color.green;

                    var addButton = GUILayout.Button(_addValueContent, GUILayout.Height(EditorGUIUtility.singleLineHeight));
                    if (addButton)
                    {
                        var value = OSCEditorUtils.CreateOSCValue(_valueTypeTemp[message]);
                        if (value != null)
                        {
                            includeValue = value;
                        }
                        else
                        {
                            EditorUtility.DisplayDialog("extOSC", "You can't add this value type!", "Ok");
                        }
                    }

                    GUI.color = defaultColor;
                    EditorGUILayout.EndHorizontal();

                    if (includeValue != null)
                    {
                        message.AddValue(includeValue);
                    }
                }
            }
        }

        public static void DrawProperties(SerializedObject serializedObject, params string[] exceptionsNames)
        {
            var serializedProperty = serializedObject.GetIterator();
            var isEmpty = true;

            while (serializedProperty.NextVisible(true))
            {
                if (serializedProperty.name == "m_Script" ||
                    exceptionsNames.Contains(serializedProperty.name))
                    continue;

                EditorGUILayout.PropertyField(serializedProperty);

                isEmpty = false;
            }

            if (isEmpty)
                EditorGUILayout.LabelField("- Empty -", OSCEditorStyles.CenterLabel);
        }

        public static void TransmitterSettings(SerializedProperty transmitterProperty, SerializedProperty addressProperty)
        {
            GUILayout.BeginVertical("box");
            EditorGUILayout.PropertyField(addressProperty, EditorGUIUtility.currentViewWidth > 410 ?
                                          _transmitterAddressContent : _transmitterAddressContentSmall);

            TransmittersPopup(transmitterProperty, _transmitterContent);

            EditorGUILayout.EndVertical();
        }

        public static void ReceiverSettings(SerializedProperty transmitterProperty, SerializedProperty addressProperty)
        {
            GUILayout.BeginVertical("box");
            EditorGUILayout.PropertyField(addressProperty, EditorGUIUtility.currentViewWidth > 380 ?
                                          _receiverAddressContent : _receiverAddressContentSmall);

            ReceiversPopup(transmitterProperty, _receiverContent);

            EditorGUILayout.EndVertical();
        }

        #endregion

        #region Static Private Methods

        private static T OSCPopup<T>(Dictionary<string, T> dictionary, T osc, GUIContent content) where T : OSCBase
        {
            T[] objects = null;
            string[] names = null;

            FillOSCArrays(dictionary, out names, out objects);

            var currentIndex = 0;
            var currentReceiver = osc;

            for (var index = 0; index < objects.Length; index++)
            {
                if (objects[index] == currentReceiver)
                {
                    currentIndex = index;
                    break;
                }
            }

            if (content != null)
            {
                var contentNames = new GUIContent[names.Length];

                for (var index = 0; index < names.Length; index++)
                {
                    contentNames[index] = new GUIContent(names[index]);
                }

                currentIndex = EditorGUILayout.Popup(content, currentIndex, contentNames);
            }
            else
            {
                currentIndex = EditorGUILayout.Popup(currentIndex, names);
            }

            return objects[currentIndex];
        }

        private static void FillOSCArrays<T>(Dictionary<string, T> dictionary, out string[] names, out T[] objects) where T : OSCBase
        {
            var namesList = new List<string>();
            namesList.Add("- None -");
            namesList.AddRange(dictionary.Keys);

            var objectsList = new List<T>();
            objectsList.Add(null);
            objectsList.AddRange(dictionary.Values);

			names = namesList.ToArray();
			objects = objectsList.ToArray();
		}

		private static void FillEnumsArray(out string[] names, out Enum[] types)
		{
			var namesList = new List<string>();
			namesList.Add("- None -");

			var objectsList = new List<Enum>();
			objectsList.Add(null);

			foreach (OSCValueType enumValue in Enum.GetValues(typeof(OSCValueType)))
			{
				namesList.Add(Enum.GetName(typeof(OSCValueType), enumValue));
				objectsList.Add(enumValue);
			}

			names = namesList.ToArray();
			types = objectsList.ToArray();
		}

		#endregion
	}
}