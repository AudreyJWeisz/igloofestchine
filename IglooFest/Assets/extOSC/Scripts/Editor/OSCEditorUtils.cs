/* Copyright (c) 2017 ExT (V.Sigalkin) */

using UnityEngine;
using UnityEditor;

using System;
using System.IO;
using System.Xml;
using System.Collections.Generic;

using extOSC.UI;
using extOSC.Core;
using extOSC.Core.Console;

namespace extOSC.Editor
{
    public static class OSCEditorUtils
    {
        #region Static Public Vars

        public static string DebugFolder
        {
            get
            {
                if (!Directory.Exists(_debugFolder))
                    Directory.CreateDirectory(_debugFolder);

                return _debugFolder;
            }
        }

        public static string BackupFolder
        {
            get
            {
                if (!Directory.Exists(_backupFoder))
                    Directory.CreateDirectory(_backupFoder);

                return _backupFoder;
            }
        }

        public static string LogsFilePath
        {
            get
            {
                if (!Directory.Exists(Path.GetDirectoryName(_logsFilePath)))
                    Directory.CreateDirectory(Path.GetDirectoryName(_logsFilePath));

                return _logsFilePath; ;
            }
        }

        #endregion

        #region Static Private Vars

        private static string _debugFolder = "./extOSC/Debug/";

        private static string _backupFoder = "./extOSC/";

        private static string _logsFilePath = "./extOSC/logs.xml";

        private static OSCControls.Resources _defaultResources;

        #endregion

        #region Static Public Methods

        public static OSCReceiver FindReceiver(int localPort)
        {
            var receivers = GameObject.FindObjectsOfType<OSCReceiver>();

            foreach (var receiver in receivers)
            {
                if (receiver.LocalPort == localPort)
                    return receiver;
            }

            return null;
        }

        public static OSCTransmitter FindTransmitter(string remoteHost, int remotePort)
        {
            var transmitters = GameObject.FindObjectsOfType<OSCTransmitter>();

            foreach (var transmitter in transmitters)
            {
                if (transmitter.RemoteHost == remoteHost &&
                    transmitter.RemotePort == remotePort)
                    return transmitter;
            }

            return null;
        }

        public static Dictionary<string, OSCReceiver> GetReceivers()
        {
            return GetOSC<OSCReceiver>((receiver) =>
            {
                return string.Format("Receiver: {0}", receiver.LocalPort);
            });
        }

        public static Dictionary<string, OSCTransmitter> GetTransmitters()
        {
            return GetOSC<OSCTransmitter>((transmitter) =>
            {
                return string.Format("Transmitter: {0}:{1}", transmitter.RemoteHost, transmitter.RemotePort);
            });
        }

        public static List<OSCConsoleMessage> LoadConsoleMessages(string filePath)
        {
            var list = new List<OSCConsoleMessage>();

            if (!File.Exists(filePath))
            {
                SaveConsoleMessages(filePath, list);

                return list;
            }

            var document = new XmlDocument();
            try
            {
                document.Load(filePath);

                var rootElement = document.FirstChild;

                foreach (XmlNode messageElement in rootElement.ChildNodes)
                {
                    var consoleMessage = new OSCConsoleMessage();

                    var instanceAttribute = messageElement.Attributes["info"];
                    consoleMessage.Info = instanceAttribute.InnerText;

                    var typeAttribute = messageElement.Attributes["type"];
                    consoleMessage.Type = (ConsoleMessageType)Enum.Parse(typeof(ConsoleMessageType), typeAttribute.InnerText);

                    var packetElement = messageElement["packet"];
                    consoleMessage.Packet = OSCPacket.FromBase64String(packetElement.InnerText);

                    list.Add(consoleMessage);
                }
            }
            catch (Exception e)
            {
                Debug.LogFormat("[OSCConsole] Error: {0}", e);
                list.Clear();
            }

            return list;
        }

        public static void SaveConsoleMessages(string filePath, List<OSCConsoleMessage> list)
        {
            var document = new XmlDocument();
            var rootElement = (XmlElement)document.AppendChild(document.CreateElement("root"));

            foreach (var consoleMessage in list)
            {
                var messageElement = rootElement.AppendChild(document.CreateElement("message"));

                var instanceAttribute = document.CreateAttribute("info");
                instanceAttribute.InnerText = consoleMessage.Info;

                var typeAttribute = document.CreateAttribute("type");
                typeAttribute.InnerText = consoleMessage.Type.ToString();

                messageElement.Attributes.Append(instanceAttribute);
                messageElement.Attributes.Append(typeAttribute);

                var packetElement = document.CreateElement("packet");
                packetElement.InnerText = OSCPacket.ToBase64String(consoleMessage.Packet);

                messageElement.AppendChild(packetElement);
            }

            document.Save(filePath);
        }

        public static OSCDebugPacket LoadDebugPacket(string filePath)
        {
            if (!File.Exists(filePath))
            {
                return null;
            }

            OSCDebugPacket debugPacket;

            var document = new XmlDocument();
            try
            {
                document.Load(filePath);

                var rootElement = document.FirstChild;

                debugPacket = new OSCDebugPacket();
                debugPacket.Name = rootElement.Attributes["name"].InnerText;
                debugPacket.Packet = OSCPacket.FromBase64String(rootElement.InnerText);
                debugPacket.FilePath = filePath;
            }
            catch (Exception e)
            {
                Debug.LogFormat("[OSCDebugPacket] Error: {0}", e);
                debugPacket = null;
            }

            return debugPacket;
        }

        public static void SaveDebugPacket(string filePath, OSCDebugPacket debugPacket)
        {
            var document = new XmlDocument();
            var rootElement = (XmlElement)document.AppendChild(document.CreateElement("debugPacket"));
            var nameAttribute = document.CreateAttribute("name");
            nameAttribute.InnerXml = debugPacket.Name;

            rootElement.Attributes.Append(nameAttribute);
            rootElement.InnerText = OSCPacket.ToBase64String(debugPacket.Packet);

            document.Save(filePath);
        }

        public static OSCControls.Resources GetStandardResources()
        {
            if (_defaultResources.PanelFilled == null)
            {
                _defaultResources.PanelFilled = OSCEditorSprites.PanelFilled;
                _defaultResources.PanelBorder = OSCEditorSprites.PanelBorder;
                _defaultResources.RotaryFilled = OSCEditorSprites.RotaryFilled;
                _defaultResources.RotaryFilledMask = OSCEditorSprites.RotaryFilledMask;
                _defaultResources.RotaryBorder = OSCEditorSprites.RotaryBorder;
            }

            return _defaultResources;
        }

        public static void PingObject(UnityEngine.Object @object, bool selectObject = true)
        {
            EditorGUIUtility.PingObject(@object);

            if (selectObject)
                Selection.activeObject = @object;
        }

        public static OSCValue CreateOSCValue(OSCValueType valueType)
        {
            switch (valueType)
            {
                case OSCValueType.Unknown:
                    return null;
                case OSCValueType.Int:
                    return OSCValue.Int(0);
                case OSCValueType.Long:
                    return OSCValue.Long(0);
                case OSCValueType.True:
                    return OSCValue.Bool(true);
                case OSCValueType.False:
                    return OSCValue.Bool(false);
                case OSCValueType.Float:
                    return OSCValue.Float(0);
                case OSCValueType.Double:
                    return OSCValue.Double(0);
                case OSCValueType.String:
                    return OSCValue.String("");
                case OSCValueType.Null:
                    return OSCValue.Null();
                case OSCValueType.Impulse:
                    return OSCValue.Impulse();
                case OSCValueType.Blob:
                    return OSCValue.Blob(new byte[0]);
                case OSCValueType.Char:
                    return OSCValue.Char(' ');
                case OSCValueType.Color:
                    return OSCValue.Color(Color.white);
                case OSCValueType.TimeTag:
                    return OSCValue.TimeTag(DateTime.Now);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public static OSCPacket CopyPacket(OSCPacket packet)
        {
            return OSCConverter.Unpack(OSCConverter.Pack(packet));
        }

        #endregion

        #region Static Private Methods

        private static Dictionary<string, T> GetOSC<T>(Func<T, string> namingCallback) where T : OSCBase
		{
			var dictionary = new Dictionary<string, T>();
			var objects = GameObject.FindObjectsOfType<T>();

			foreach (var osc in objects)
			{
                var name = osc.gameObject.name;

				if (namingCallback != null)
					name = namingCallback(osc);

                if (!dictionary.ContainsKey(name))
				    dictionary.Add(name, osc);
			}

			return dictionary;
		}

		#endregion
	}
}