/* Copyright (c) 2017 ExT (V.Sigalkin) */

using UnityEngine;
using UnityEditor;

using System.Collections.Generic;

using extOSC.Core;
using extOSC.Editor.Windows;

namespace extOSC.Editor.Panels
{
    public class OSCPanelContollers : OSCPanel
    {
        #region Private Static Vars

        private static readonly GUIContent _sendActionContent = new GUIContent("Send");

        private static readonly GUIContent _receiveActionContent = new GUIContent("Receive");

        private static readonly GUIContent _selectActionContent = new GUIContent("Select");

        private static readonly GUIContent _transmittersContent = new GUIContent("Transmitters:");

        private static readonly GUIContent _receiversContent = new GUIContent("Receivers:");

        private static readonly GUIContent _actionsContent = new GUIContent("Actions:");

        #endregion

        #region Private Vars

        private Dictionary<string, OSCTransmitter> _transmitters = new Dictionary<string, OSCTransmitter>();

        private Dictionary<string, OSCReceiver> _receivers = new Dictionary<string, OSCReceiver>();

        private Vector2 _scrollPosition;

        #endregion

        #region Unity Methods

        protected override void DrawContent(Rect contentRect)
        {
            EditorGUILayout.BeginVertical();
            EditorGUILayout.BeginHorizontal(EditorStyles.toolbar);

            GUILayout.FlexibleSpace();

            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndVertical();

            _scrollPosition = GUILayout.BeginScrollView(_scrollPosition);

            EditorGUILayout.HelpBox("For component activation in Edit Mode you need to choose appropriate GameObject and have \"Work In Editor\" turned on.", MessageType.Info);

            var wide = contentRect.width > 350;
            if (wide) GUILayout.BeginHorizontal();

            GUILayout.BeginVertical();
            GUILayout.Label(_transmittersContent);

            if (_transmitters.Count > 0)
            {
                foreach (var transmitter in _transmitters)
                {
                    DrawElement(transmitter.Key, transmitter.Value);
                }
            }
            else
            {
                EditorGUILayout.HelpBox("Scene doesn't have OSCReceiver.", MessageType.Info);
            }

            GUILayout.EndVertical();

            GUILayout.BeginVertical();
            GUILayout.Label(_receiversContent);

            if (_receivers.Count > 0)
            {
                foreach (var receiver in _receivers)
                {
                    DrawElement(receiver.Key, receiver.Value);
                }
            }
            else
            {
                EditorGUILayout.HelpBox("Scene doesn't have OSCTransmitter.", MessageType.Info);
            }

            GUILayout.EndVertical();

            if (wide) GUILayout.EndHorizontal();

            GUILayout.EndScrollView();
        }

        #endregion

        #region Public Methods

        public OSCPanelContollers(OSCWindow parent, string uniqueName) : base(parent, uniqueName) { }

        public void Refresh()
        {
            _transmitters.Clear();
            _transmitters = OSCEditorUtils.GetTransmitters();

            _receivers.Clear();
            _receivers = OSCEditorUtils.GetReceivers();
        }

        #endregion

        #region Private Methods

        private void DrawElement(string name, OSCBase osc)
        {
            var defaultColor = GUI.color;
            var elementColor = osc.IsAvaible ? Color.green : Color.red;

            GUI.color = elementColor;

            GUILayout.BeginVertical(EditorStyles.helpBox);

            GUILayout.Label(name);

            GUILayout.BeginVertical("box");
            GUILayout.Label("Active: " + osc.IsAvaible);
            GUILayout.EndVertical();

            GUILayout.Label(_actionsContent);
            GUILayout.BeginHorizontal("box");
            DrawActions(osc, elementColor);
            GUILayout.EndHorizontal();

            GUILayout.EndVertical();

            GUI.color = defaultColor;
        }

        private void DrawActions(OSCBase osc, Color elementColor)
        {
            GUI.color = Color.yellow;
            GUI.enabled = osc.IsAvaible;

            if (osc is OSCTransmitter) DrawTransmitterActions((OSCTransmitter)osc);
            else if (osc is OSCReceiver) DrawReceiverActions((OSCReceiver)osc);

            GUI.enabled = true;
            GUI.color = Color.white;

            var selectButton = GUILayout.Button(_selectActionContent, GUILayout.MaxWidth(60));
            if (selectButton) OSCEditorUtils.PingObject(osc);

            GUI.color = elementColor;

        }

        private void DrawTransmitterActions(OSCTransmitter transmitter)
        {
            var actionButton = GUILayout.Button(_sendActionContent);
            if (actionButton)
            {
                var debugPacket = OSCWindowDebug.CurrentDebugPacket;
                if (debugPacket != null)
                {
                    transmitter.Send(OSCEditorUtils.CopyPacket(debugPacket.Packet));
                }
            }
        }

        private void DrawReceiverActions(OSCReceiver receiver)
        {
            var actionButton = GUILayout.Button(_receiveActionContent);
            if (actionButton)
            {
                var debugPacket = OSCWindowDebug.CurrentDebugPacket;
                if (debugPacket != null)
                {
                    receiver.FakeReceive(OSCEditorUtils.CopyPacket(debugPacket.Packet));
                }
            }
        }

        #endregion
    }
}