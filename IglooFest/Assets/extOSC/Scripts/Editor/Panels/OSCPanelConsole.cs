/* Copyright (c) 2017 ExT (V.Sigalkin) */

using UnityEngine;
using UnityEditor;

using extOSC.Core;
using extOSC.Core.Console;
using extOSC.Editor.Windows;

namespace extOSC.Editor.Panels
{
    public class OSCPanelConsole : OSCPanel
    {
        #region Static Private Vars

        private static readonly GUIContent _clearContent = new GUIContent("Clear");

        private static readonly GUIContent _transmittedContent = new GUIContent("Transmitted");

        private static readonly GUIContent _recevedContent = new GUIContent("Received");

        private static readonly GUIContent _trackLastContent = new GUIContent("Track Last");

        private static readonly GUIContent _collapseContent = new GUIContent("Collapse (Alpha)");

        #endregion

        #region Static Private Methods

        private static void DrawItem(Rect itemRect, int index, OSCConsoleContainer consoleContainer, bool selected, bool collapse)
        {
            var dataRect = new Rect(0, 0, itemRect.width, itemRect.height);
            var backStyle = index % 2 != 0 ? OSCEditorStyles.ConsoleItemBackEven : OSCEditorStyles.ConsoleItemBackOdd;

            if (Event.current.type == EventType.Repaint)
            {
                backStyle.Draw(itemRect, false, false, selected, false);
            }

            GUI.BeginGroup(itemRect);

            DrawIcons(consoleContainer.Message, ref dataRect);

            if (collapse)
            {
                var collapseRect = new Rect(dataRect.x + 2, dataRect.y + 2, dataRect.height * 1.5f - 4, dataRect.height - 4);

                GUI.Box(collapseRect, "", EditorStyles.helpBox);
                GUI.Label(collapseRect, consoleContainer.Count.ToString(), OSCEditorStyles.CenterLabel);

                dataRect.x += collapseRect.width + 4;
                dataRect.width -= collapseRect.height + 4;
            }

            GUI.Label(dataRect, ItemToString(consoleContainer.Message), OSCEditorStyles.ConsoleLabel);

            GUI.EndGroup();
        }

        private static Rect PaddingRect(Rect sourceRect, float padding)
        {
            return new Rect(sourceRect.x + padding, sourceRect.y + padding, sourceRect.height - padding * 2f, sourceRect.height - padding * 2f);
        }

        private static void DrawIcons(OSCConsoleMessage consoleMessage, ref Rect dataRect)
        {
            Texture2D consoleTexture = null;

            if (consoleMessage.Type == ConsoleMessageType.Received)
            {
                consoleTexture = OSCEditorTextures.Receiver;
            }
            else if (consoleMessage.Type == ConsoleMessageType.Transmitted)
            {
                consoleTexture = OSCEditorTextures.Transmitter;
            }

            GUI.DrawTexture(PaddingRect(dataRect, 4), consoleTexture);

            dataRect.x += dataRect.height;
            dataRect.width -= dataRect.height;

            if (consoleMessage.Packet != null)
            {
                var packetTypeTextire = consoleMessage.Packet.IsBundle() ? OSCEditorTextures.Bundle : OSCEditorTextures.Message;

                GUI.DrawTexture(PaddingRect(dataRect, 4), packetTypeTextire);
            }

            dataRect.x += dataRect.height;
            dataRect.width -= dataRect.height;
        }

        private static string ItemToString(OSCConsoleMessage consoleMessage)
        {
            if (consoleMessage == null)
                return string.Empty;

            var stringMessage = PacketToString(consoleMessage.Packet);
            stringMessage += "\n" + consoleMessage.Info;

            return stringMessage;
        }

        private static string PacketToString(OSCPacket packet)
        {
            if (!packet.IsBundle())
            {
                return string.Format("<color=orange>Message:</color> {0}", packet.Address);
            }

            var bundle = packet as OSCBundle;
            if (bundle != null)
            {
                return string.Format("<color=yellow>Bundle:</color> (Packets: {0})", bundle.Packets.Count);
            }

            return string.Empty;
        }

        #endregion

        #region Public Vars

        public bool ShowTransmitted
        {
            get { return _showTransmitted; }
            set { _showTransmitted = value; }
        }

        public bool ShowReceived
        {
            get { return _showReceived; }
            set { _showReceived = value; }
        }

        public bool CollapseConsole
        {
            get { return _collapseConsole; }
            set { _collapseConsole = value; }
        }

        public bool TrackLast
        {
            get { return _trackLast; }
            set { _trackLast = value; }
        }

        public OSCConsoleMessage SelectedMessage
        {
            get { return _selectedMessage; }
            set { _selectedMessage = value; }
        }

        public int SelectedMessageId
        {
            get { return GetItemIndex(_selectedMessage); }
            set { if (_consoleBuffer != null && _consoleBuffer.Length > 0) _selectedMessage = _consoleBuffer[Mathf.Clamp(value, 0, _consoleBuffer.Length - 1)].Message; }
        }

        #endregion

        #region Private Vars

        private Rect _contentRect;

        private bool _showReceived;

        private bool _showTransmitted;

        private bool _collapseConsole;

        protected bool _trackLast;

        private Rect _lastContentRect;

        private Vector2 _scrollPosition;

        private float _lineHeight = 30f;

        private OSCConsoleMessage _selectedMessage;

        private OSCConsoleContainer[] _consoleBuffer;

        #endregion

        #region Public Methods

        public OSCPanelConsole(OSCWindow parent, string uniqueName) : base(parent, uniqueName) { }

        public void SelectFirstItem()
        {
            if (_consoleBuffer.Length <= 0) return;

            _selectedMessage = _consoleBuffer[0].Message;
            ScrollToItem(_selectedMessage);
        }

        public void SelectNextItem()
        {
            if (_consoleBuffer.Length <= 0) return;

            var currentIndex = GetItemIndex(_selectedMessage);

            _selectedMessage = _consoleBuffer[Mathf.Clamp(currentIndex + 1, 0, _consoleBuffer.Length - 1)].Message;
            ScrollToItem(_selectedMessage);
        }

        public void SelectPreviousItem()
        {
            if (_consoleBuffer.Length <= 0) return;

            var currentIndex = GetItemIndex(_selectedMessage);

            _selectedMessage = _consoleBuffer[Mathf.Clamp(currentIndex - 1, 0, _consoleBuffer.Length - 1)].Message;
            ScrollToItem(_selectedMessage);
        }

        public void SelectLastItem()
        {
            if (_consoleBuffer.Length <= 0) return;

            _selectedMessage = _consoleBuffer[_consoleBuffer.Length - 1].Message;
            ScrollToItem(_selectedMessage);
        }

        public void ScrollToItem(OSCConsoleMessage consoleMessage)
        {
            if (consoleMessage == null) return;

            var messageItem = GetItemIndex(consoleMessage);

            if (messageItem < 0) return;

            var itemY = _lineHeight * messageItem;

            if (itemY < _scrollPosition.y)
            {
                _scrollPosition.y = itemY;
            }
            else if (itemY > _scrollPosition.y + _lastContentRect.height - _lineHeight)
            {
                _scrollPosition.y = itemY - _lastContentRect.height + _lineHeight;
            }
        }

        #endregion

        #region Protected Methods

        protected override void PreDrawContent()
        {
            
        }

        protected override void DrawContent(Rect contentRect)
        {
            GUILayout.BeginArea(new Rect(0, 0, contentRect.width, 18));

            EditorGUILayout.BeginVertical();
            EditorGUILayout.BeginHorizontal(EditorStyles.toolbar);

            var clearButton = GUILayout.Button(_clearContent, EditorStyles.toolbarButton, GUILayout.Height(45f));

            GUILayout.Space(5f);

            _showReceived = GUILayout.Toggle(_showReceived, _recevedContent, EditorStyles.toolbarButton);
            _showTransmitted = GUILayout.Toggle(_showTransmitted, _transmittedContent, EditorStyles.toolbarButton);

            GUILayout.Space(5f);

            _trackLast = GUILayout.Toggle(_trackLast, _trackLastContent, EditorStyles.toolbarButton);

            GUILayout.FlexibleSpace();
            GUILayout.Space(5f);

            _collapseConsole = GUILayout.Toggle(_collapseConsole, _collapseContent, EditorStyles.toolbarButton);

            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndVertical();

            GUILayout.EndArea();

            contentRect.y += 18;
            contentRect.height -= 18;

            _lastContentRect = new Rect(contentRect);
            _consoleBuffer = OSCWindowConsole.GetConsoleBuffer(_showTransmitted, _showReceived, _collapseConsole);

            if (_trackLast)
            {
                if (_consoleBuffer.Length > 0)
                {
                    _selectedMessage = _consoleBuffer[0].Message;
                }
                else
                {
                    _selectedMessage = null;
                }
            }

            var viewRect = new Rect(contentRect);
            viewRect.height = _consoleBuffer.Length * _lineHeight;

            if (viewRect.height > contentRect.height)
                viewRect.width -= 16f;

            var itemRect = new Rect(0, viewRect.y, viewRect.width, _lineHeight);
            _scrollPosition = GUI.BeginScrollView(contentRect, _scrollPosition, viewRect);

            var drawed = false;

            for (var index = 0; index < _consoleBuffer.Length; index++)
            {
                var drawItem = !((itemRect.y + itemRect.height < _scrollPosition.y) ||
                                 (itemRect.y > _scrollPosition.y + contentRect.height + itemRect.height));

                if (drawItem)
                {
                    drawed = true;

                    var consoleMessage = _consoleBuffer[index];
                    var selected = _selectedMessage == consoleMessage.Message;
                    var color = GUI.color;

                    if (drawItem) DrawItem(itemRect, index, consoleMessage, selected, _collapseConsole);

                    GUI.color = color;

                    if (Event.current.type == EventType.MouseDown && itemRect.Contains(Event.current.mousePosition))
                    {
                        if (Event.current.button == 0)
                        {
                            _trackLast = false;

                            if (_selectedMessage != consoleMessage.Message)
                            {
                                _selectedMessage = consoleMessage.Message;
                                _parent.Repaint();
                            }
                        }
                        else if (Event.current.button == 1)
                        {
                            //TODO: ???
                        }
                    }
                }
                else if (drawed)
                {
                    break;
                }

                itemRect.y += itemRect.height;
            }

            GUI.EndScrollView(true);

            if (clearButton)
            {
                OSCWindowConsole.Clear();

                _selectedMessage = null;
            }
        }

        protected override void PostDrawContent()
        {
            var current = Event.current;

            if (!current.isKey || current.type != EventType.KeyDown) return;

            if (current.keyCode == KeyCode.DownArrow)
            {
                SelectNextItem();
            }
            else if (current.keyCode == KeyCode.UpArrow)
            {
                SelectPreviousItem();
            }
            else if (current.keyCode == KeyCode.End)
            {
                SelectLastItem();
            }
            else if (current.keyCode == KeyCode.Home)
            {
                SelectFirstItem();
            }

            current.Use();

            _parent.Repaint();
        }

        #endregion

        #region Private Methods

        private int GetItemIndex(OSCConsoleMessage consoleMessage)
        {
            for (var index = 0; index < _consoleBuffer.Length; index++)
            {
                if (_consoleBuffer[index].Message == consoleMessage) return index;
            }

            return -1;
        }

        #endregion
    }
}