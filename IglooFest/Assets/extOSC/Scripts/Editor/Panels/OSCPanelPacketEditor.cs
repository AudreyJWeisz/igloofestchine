/* Copyright (c) 2017 ExT (V.Sigalkin) */

using UnityEngine;
using UnityEditor;

using System.IO;

using extOSC.Editor.Windows;

namespace extOSC.Editor.Panels
{
    public class OSCPanelPacketEditor : OSCPanel
    {
        #region Static Public Vars

        private static readonly GUIContent _createContent = new GUIContent("Create");

        private static readonly GUIContent _openContent = new GUIContent("Open Packet");

        private static readonly GUIContent _saveContent = new GUIContent("Save Packet");

        private static readonly GUIContent _infoContent = new GUIContent("Create or load debug packet!");

        #endregion

        #region Public Vars

        public OSCDebugPacket CurrentDebugPacket
        {
            get { return _currentDebugPacket; }
            set { _currentDebugPacket = value; }
        }

        #endregion

        #region Private Vars

        private Vector2 _scrollPosition;

        private GUIContent[] _createPoputItems = new GUIContent[] {
            new GUIContent("Message"),
            new GUIContent("Bundle")
        };

        private OSCDebugPacket _currentDebugPacket;

        #endregion

        #region Unity Methods

        protected override void DrawContent(Rect contentRect)
        {
            EditorGUILayout.BeginVertical();
            EditorGUILayout.BeginHorizontal(EditorStyles.toolbar);

            if (GUILayout.Button(_createContent, EditorStyles.toolbarDropDown))
            {
                var customMenuRect = new Rect(Event.current.mousePosition.x, Event.current.mousePosition.y, 0, 0);

                EditorUtility.DisplayCustomMenu(customMenuRect, _createPoputItems, -1, CreatePacket, null);
            }

            GUILayout.Space(5);

            var openButton = GUILayout.Button(_openContent, EditorStyles.toolbarButton);
            var saveButton = GUILayout.Button(_saveContent, EditorStyles.toolbarButton);

            GUILayout.FlexibleSpace();

            if (_currentDebugPacket != null)
                GUILayout.Label(string.Format("Name: {0}", _currentDebugPacket.Name));

            EditorGUILayout.EndHorizontal();

            if (_currentDebugPacket != null && _currentDebugPacket.Packet != null)
            {
                _scrollPosition = EditorGUILayout.BeginScrollView(_scrollPosition);

                OSCEditorLayout.EditablePacket(_currentDebugPacket.Packet);

                EditorGUILayout.EndScrollView();
            }
            else
            {
                EditorGUILayout.LabelField(_infoContent, OSCEditorStyles.CenterLabel, GUILayout.Height(contentRect.height));
            }

            EditorGUILayout.EndVertical();

            if (openButton) OpenCurrentDebugPacket();
            if (saveButton) SaveCurrentDebugPacket();
        }

        #endregion

        #region Public Methods

        public OSCPanelPacketEditor(OSCWindow parent, string uniqueName) : base(parent, uniqueName)
        { }

        #endregion

        #region Private Methods

        private void SaveCurrentDebugPacket()
        {
            if (_currentDebugPacket == null) return;

            var filePath = EditorUtility.SaveFilePanel("Save Packet", OSCEditorUtils.DebugFolder, "New Debug Packet", "eod");
            if (!string.IsNullOrEmpty(filePath))
            {
                _currentDebugPacket.Name = Path.GetFileNameWithoutExtension(filePath);
                _currentDebugPacket.FilePath = filePath;

                OSCEditorUtils.SaveDebugPacket(filePath, _currentDebugPacket);
            }
        }

        private void OpenCurrentDebugPacket()
        {
            var filePath = EditorUtility.OpenFilePanel("Open Packet", OSCEditorUtils.DebugFolder, "eod");
            if (!string.IsNullOrEmpty(filePath))
            {
                _currentDebugPacket = OSCEditorUtils.LoadDebugPacket(filePath);
            }
        }

        private void CreatePacket(object userData, string[] options, int selected)
        {
            var debugPacket = new OSCDebugPacket();

            if (selected == 0)
                debugPacket.Packet = new OSCMessage("/address");
            else
                debugPacket.Packet = new OSCBundle();

            _currentDebugPacket = debugPacket;
        }

        #endregion
    }
}