/* Copyright (c) 2017 ExT (V.Sigalkin) */

using UnityEngine;
using UnityEditor;

using System.Linq;
using System.Collections.Generic;

using extOSC.Core.Console;
using extOSC.Editor.Panels;

namespace extOSC.Editor.Windows
{
    public class OSCWindowConsole : OSCWindow<OSCWindowConsole>
    {
        #region Static Public Vars

        public static List<OSCConsoleMessage> ConsoleBuffer;

        #endregion

        #region Static Private Vars

        private static OSCConsoleContainer[] _tempBuffer = new OSCConsoleContainer[0];

        private static bool _previousTransmitted;

        private static bool _previousReceived;

        private static bool _previousCollapsed;

        private static OSCConsoleMessage _lastTransmitted;

        private static OSCConsoleMessage _lastReceived;

        private static int _maxBufferCapacity = 256;

        private static OSCConsoleContainer[] _emptyContainers = new OSCConsoleContainer[0];

        #endregion

        #region Public Static Methods

        [MenuItem("Window/extOSC/Console Window", false, 1)]
        public static void ShowWindow()
        {
            Instance.titleContent = new GUIContent("OSC Console", OSCEditorTextures.IronWall);
            Instance.minSize = new Vector2(610, 200);
            Instance.Show();
        }

        public static OSCConsoleContainer[] GetConsoleBuffer(bool transmitted, bool received, bool collapsed)
        {
            if (ConsoleBuffer == null)
                return _emptyContainers;

            var requireRebuild = false;

            if (_previousTransmitted != transmitted ||
                _previousReceived != received ||
                _previousCollapsed != collapsed)
            {
                _previousTransmitted = transmitted;
                _previousReceived = received;
                _previousCollapsed = collapsed;

                requireRebuild = true;
            }
            else if (ConsoleBuffer != null && ConsoleBuffer.Count > 0)
            {
                requireRebuild = (ConsoleBuffer[0] != _lastReceived && ConsoleBuffer[0] != _lastTransmitted);
            }
            else
            {
                requireRebuild = true;
            }

            if (!requireRebuild)
                return _tempBuffer;

            var consoleList = new List<OSCConsoleContainer>();
            var consoleDictionary = new Dictionary<string, OSCConsoleContainer>();
            var lastTransmitted = false;
            var lastReceived = false;

            foreach (var item in ConsoleBuffer)
            {
                if (!lastTransmitted && item.Type == ConsoleMessageType.Transmitted)
                {
                    _lastTransmitted = item;
                    lastTransmitted = true;
                }

                if (!lastReceived && item.Type == ConsoleMessageType.Received)
                {
                    _lastReceived = item;
                    lastReceived = true;
                }

                if ((transmitted && item.Type == ConsoleMessageType.Transmitted) ||
                    (received && item.Type == ConsoleMessageType.Received))
                {
                    if (collapsed)
                    {
                        var address = item.Packet.Address + "-" + item.Type;

                        if (consoleDictionary.ContainsKey(address))
                        {
                            consoleDictionary[address].Count++;
                        }
                        else
                        {
                            consoleDictionary.Add(address, new OSCConsoleContainer()
                            {
                                Count = 1,
                                Message = item
                            });
                        }
                    }
                    else
                    {
                        consoleList.Add(new OSCConsoleContainer()
                        {
                            Count = 0,
                            Message = item
                        });
                    }
                }
            }

            _tempBuffer = collapsed ? consoleDictionary.Values.ToArray() : consoleList.ToArray();

            return _tempBuffer;
        }

        public static void Clear()
        {
            if (ConsoleBuffer != null)
            {
                ConsoleBuffer.Clear();
                OSCEditorUtils.SaveConsoleMessages(OSCEditorUtils.LogsFilePath, ConsoleBuffer);
            }
        }

        #endregion

        #region Protected Vars

        private OSCPanelConsole logPanel;

        private OSCPanelPacket packetPanel;

        #endregion

        #region Private Vars

        private OSCSplitPanel _mainSplitPanel;

        private readonly string _showReceivedSettings = OSCEditorSettings.Console + "showreceived";

        private readonly string _showTransmittedSettings = OSCEditorSettings.Console + "showtransmitted";

        private readonly string _collapseLogSettings = OSCEditorSettings.Console + "collapselog";

        private readonly string _trackLastSettings = OSCEditorSettings.Console + "tracklast";

        #endregion

        #region Unity Methods

        protected override void OnEnable()
        {
            _mainSplitPanel = new OSCSplitPanel(this, "consoleMainPanel");

            logPanel = new OSCPanelConsole(this, "oscLogPanel");
            packetPanel = new OSCPanelPacket(this, "oscPacketPanel");

            _mainSplitPanel.AddPanel(logPanel, 310, 0.6f);
            _mainSplitPanel.AddPanel(packetPanel, 300, 0.4f);

            SetRoot(_mainSplitPanel);

            base.OnEnable();
        }

        protected override void Update()
        {
            if (ConsoleBuffer == null)
            {
                ConsoleBuffer = OSCEditorUtils.LoadConsoleMessages(OSCEditorUtils.LogsFilePath);

                Repaint();
            }

            if (OSCConsole.ConsoleBuffer.Count > 0)
            {
                foreach (var message in OSCConsole.ConsoleBuffer)
                {
                    if (ConsoleBuffer.Count >= _maxBufferCapacity)
                        ConsoleBuffer.RemoveAt(ConsoleBuffer.Count - 1);

                    ConsoleBuffer.Insert(0, message);
                }

                OSCEditorUtils.SaveConsoleMessages(OSCEditorUtils.LogsFilePath, ConsoleBuffer);
                OSCConsole.ConsoleBuffer.Clear();

                Repaint();
            }

            if (packetPanel.SelecedMessage != logPanel.SelectedMessage)
            {
                packetPanel.SelecedMessage = logPanel.SelectedMessage;
                Repaint();
            }
        }

        #endregion

        #region Protected Methods

        protected override void LoadWindowSettings()
        {
            base.LoadWindowSettings();

            if (logPanel == null) return;

            logPanel.ShowReceived = OSCEditorSettings.GetBool(_showReceivedSettings, true);
            logPanel.ShowTransmitted = OSCEditorSettings.GetBool(_showTransmittedSettings, true);
            logPanel.CollapseConsole = OSCEditorSettings.GetBool(_collapseLogSettings, false);
            logPanel.TrackLast = OSCEditorSettings.GetBool(_trackLastSettings, false);
        }

        protected override void SaveWindowSettings()
        {
            base.SaveWindowSettings();

            if (logPanel == null) return;

            OSCEditorSettings.SetBool(_showReceivedSettings, logPanel.ShowReceived);
            OSCEditorSettings.SetBool(_showTransmittedSettings, logPanel.ShowTransmitted);
            OSCEditorSettings.SetBool(_collapseLogSettings, logPanel.CollapseConsole);
            OSCEditorSettings.SetBool(_trackLastSettings, logPanel.TrackLast);
        }

        #endregion
	}
}