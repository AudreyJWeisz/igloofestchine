/* Copyright (c) 2017 ExT (V.Sigalkin) */

using UnityEngine;
using UnityEditor;

using extOSC.Core;
using extOSC.Editor.Panels;

namespace extOSC.Editor.Windows
{
    public class OSCWindowDebug : OSCWindow<OSCWindowDebug>
    {
        #region Static Public Vars

        public static OSCDebugPacket CurrentDebugPacket
        {
            get
            {
                if (Instance != null && Instance.packetEditorPanel != null && Instance.packetEditorPanel.CurrentDebugPacket != null)
                    return Instance.packetEditorPanel.CurrentDebugPacket;

                return null;
            }
        }

        #endregion

        #region Static Public Methods

        [MenuItem("Window/extOSC/Debug Window", false, 2)]
        public static void ShowWindow()
        {
            Instance.titleContent = new GUIContent("OSC Debug", OSCEditorTextures.IronWall);
            Instance.minSize = new Vector2(550, 200);
            Instance.Show();
        }

        public static void OpenPacket(OSCPacket packet)
        {
            ShowWindow();

            var debugPacket = new OSCDebugPacket();
            debugPacket.Packet = OSCEditorUtils.CopyPacket(packet);

            Instance.packetEditorPanel.CurrentDebugPacket = debugPacket;
            Instance.Focus();
        }

        #endregion

        #region Protected Vars

        protected OSCPanelPacketEditor packetEditorPanel;

        protected OSCPanelContollers controllersPanel;

        #endregion

        #region Private Vars

        private OSCSplitPanel _mainSplitPanel;

        private readonly string _lastFileSettings = OSCEditorSettings.Debug + "lastfile";

        #endregion

        #region Unity Methods

        protected override void OnEnable()
        {
            _mainSplitPanel = new OSCSplitPanel(this, "debugMainPanel");

            packetEditorPanel = new OSCPanelPacketEditor(this, "debugPacketEditor");
            controllersPanel = new OSCPanelContollers(this, "debugOSCControllers");

            _mainSplitPanel.AddPanel(packetEditorPanel, 300);
            _mainSplitPanel.AddPanel(controllersPanel, 250);

            SetRoot(_mainSplitPanel);

            base.OnEnable();
        }

        protected void OnInspectorUpdate()
        {
            if (controllersPanel != null)
                controllersPanel.Refresh();

            Repaint();
        }

        #endregion

        #region Protected Methods

        protected override void SaveWindowSettings()
        {
            if (packetEditorPanel == null) return;

            var debugPacket = packetEditorPanel.CurrentDebugPacket;
            if (debugPacket != null)
            {
                if (string.IsNullOrEmpty(debugPacket.FilePath))
                {
                    debugPacket.FilePath = OSCEditorUtils.BackupFolder + "debug_unsaved.eod";
                }

                OSCEditorUtils.SaveDebugPacket(debugPacket.FilePath, debugPacket);
                OSCEditorSettings.SetString(_lastFileSettings, debugPacket.FilePath);

                return;
            }

            OSCEditorSettings.SetString(_lastFileSettings, "");
        }

        protected override void LoadWindowSettings()
        {
            if (packetEditorPanel == null) return;

            var lastOpenedFile = OSCEditorSettings.GetString(_lastFileSettings, "");

            if (!string.IsNullOrEmpty(lastOpenedFile))
            {
                var debugPacket = OSCEditorUtils.LoadDebugPacket(lastOpenedFile);
                if (debugPacket != null)
                {
                    packetEditorPanel.CurrentDebugPacket = debugPacket;
                }
            }
        }

        #endregion
    }
}