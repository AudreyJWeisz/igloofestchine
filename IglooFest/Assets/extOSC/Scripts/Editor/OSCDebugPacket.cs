/* Copyright (c) 2017 ExT (V.Sigalkin) */

using extOSC.Core;

namespace extOSC.Editor
{
    public class OSCDebugPacket
    {
        #region Public Vars

        public string Name = "Unnamed";

        public OSCPacket Packet;

        public string FilePath;

        #endregion
    }
}