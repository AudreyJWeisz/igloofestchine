/* Copyright (c) 2017 ExT (V.Sigalkin) */

using extOSC.Core.Console;

namespace extOSC.Editor.Panels
{
    public class OSCConsoleContainer
    {
        #region Public Vars

        public int Count;

        public OSCConsoleMessage Message;

        #endregion
    }
}