/* Copyright (c) 2017 ExT (V.Sigalkin) */

using UnityEngine;
using UnityEditor;
using UnityEditor.UI;

using extOSC.UI;

namespace extOSC.Editor.Components.UI
{
    [CanEditMultipleObjects]
    [CustomEditor(typeof(OSCPad), true)]
    public class OSCPadEditor : SelectableEditor
    {
        #region Private Vars

        private static readonly GUIContent _maxContent = new GUIContent("Max");

        private static readonly GUIContent _minContent = new GUIContent("Min");

        private SerializedProperty _handleRectProperty;

        private SerializedProperty _minValueProperty;

        private SerializedProperty _maxValueProperty;

        private SerializedProperty _wholeNumbersProperty;

        private SerializedProperty _valueProperty;

        private SerializedProperty _onValueChangedProperty;

        private SerializedProperty _handleAlignmentProperty;

        private SerializedProperty _xAxisRectProperty;

        private SerializedProperty _yAxisRectProperty;

        private SerializedProperty _resetValueProperty;

        private SerializedProperty _resetValueTimeProperty;

        private SerializedProperty _callbackOnResetProperty;

        #endregion

        #region Unity Methods

        protected override void OnEnable()
        {
            base.OnEnable();

            _handleRectProperty = serializedObject.FindProperty("_handleRect");
            _minValueProperty = serializedObject.FindProperty("_minValue");
            _maxValueProperty = serializedObject.FindProperty("_maxValue");
            _wholeNumbersProperty = serializedObject.FindProperty("_wholeNumbers");
            _valueProperty = serializedObject.FindProperty("_value");
            _onValueChangedProperty = serializedObject.FindProperty("_onValueChanged");
            _handleAlignmentProperty = serializedObject.FindProperty("_handleAlignment");
            _xAxisRectProperty = serializedObject.FindProperty("_xAxisRect");
            _yAxisRectProperty = serializedObject.FindProperty("_yAxisRect");
            _resetValueProperty = serializedObject.FindProperty("_resetValue");
            _resetValueTimeProperty = serializedObject.FindProperty("_resetValueTime");
            _callbackOnResetProperty = serializedObject.FindProperty("_callbackOnReset");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            GUILayout.Space(10);
            OSCEditorLayout.DrawLogo();
            GUILayout.Space(5);

            GUILayout.Label(string.Format("Value: {0}", _valueProperty.vector2Value), EditorStyles.boldLabel);
            GUILayout.BeginVertical("box");

            GUILayout.Label("Settings:", EditorStyles.boldLabel);
            GUILayout.BeginVertical("box");
            base.OnInspectorGUI();
            GUILayout.EndVertical();

            GUILayout.Label("Pad Settings:", EditorStyles.boldLabel);
            GUILayout.BeginVertical("box");
            EditorGUILayout.PropertyField(_handleRectProperty);

            if (_handleRectProperty.objectReferenceValue != null)
            {
                EditorGUILayout.PropertyField(_handleAlignmentProperty);
                GUILayout.EndVertical();

                var minX = _minValueProperty.FindPropertyRelative("x");
                var maxX = _maxValueProperty.FindPropertyRelative("x");
                var minY = _minValueProperty.FindPropertyRelative("y");
                var maxY = _maxValueProperty.FindPropertyRelative("y");

                GUILayout.Label("Value Settings:", EditorStyles.boldLabel);
                GUILayout.BeginVertical("box");
                EditorGUILayout.PropertyField(_xAxisRectProperty);
                EditorGUILayout.PropertyField(_yAxisRectProperty);

                EditorGUILayout.Space();

                EditorGUILayout.LabelField("X Axis");
                EditorGUI.indentLevel++;
                EditorGUILayout.PropertyField(minX, _minContent);
                EditorGUILayout.PropertyField(maxX, _maxContent);
                EditorGUI.indentLevel--;

                EditorGUILayout.Space();

                EditorGUILayout.LabelField("Y Axis");
                EditorGUI.indentLevel++;
                EditorGUILayout.PropertyField(minY, _minContent);
                EditorGUILayout.PropertyField(maxY, _maxContent);
                EditorGUI.indentLevel--;

                EditorGUILayout.Space();

                EditorGUILayout.PropertyField(_wholeNumbersProperty);

                EditorGUILayout.Space();

                EditorGUILayout.LabelField("Value");
                EditorGUI.indentLevel++;
                EditorGUILayout.Slider(_valueProperty.FindPropertyRelative("x"), minX.floatValue, maxX.floatValue);
                EditorGUILayout.Slider(_valueProperty.FindPropertyRelative("y"), minY.floatValue, maxY.floatValue);
				EditorGUI.indentLevel--;

				GUILayout.EndVertical();

				GUILayout.Label("Reset Value Settings:", EditorStyles.boldLabel);
				GUILayout.BeginVertical("box");
				EditorGUILayout.PropertyField(_resetValueProperty);
				EditorGUILayout.PropertyField(_resetValueTimeProperty);
				EditorGUILayout.PropertyField(_callbackOnResetProperty);
				GUILayout.EndVertical();

				EditorGUILayout.Space();

				EditorGUILayout.PropertyField(_onValueChangedProperty);
			}
			else
			{
				GUILayout.EndVertical();
				EditorGUILayout.HelpBox("You need to set \"RectTransform Group\" for correct work of the component.", MessageType.Info);
			}

			GUILayout.EndVertical();
			serializedObject.ApplyModifiedProperties();
		}

		#endregion
	}
}