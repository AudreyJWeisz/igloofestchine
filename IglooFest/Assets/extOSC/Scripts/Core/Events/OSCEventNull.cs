/* Copyright (c) 2017 ExT (V.Sigalkin) */

using UnityEngine.Events;

using System;

namespace extOSC.Core.Events
{
    [Serializable]
    public class OSCEventNull : OSCEvent
	{ }
}