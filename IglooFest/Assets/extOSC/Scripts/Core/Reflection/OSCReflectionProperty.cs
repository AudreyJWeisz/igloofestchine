/* Copyright (c) 2017 ExT (V.Sigalkin) */

using UnityEngine;

using System;
using System.Reflection;

namespace extOSC.Core.Internals
{
    public class OSCReflectionProperty
    {
        #region Static Public Methods

        public static OSCReflectionProperty Create(object target, MemberInfo memberInfo)
        {
            if (memberInfo is FieldInfo)
                return new OSCReflectionProperty(target, (FieldInfo)memberInfo);

            if (memberInfo is PropertyInfo)
                return new OSCReflectionProperty(target, (PropertyInfo)memberInfo);

            return null;
        }

        #endregion

        #region Public Vars

        public object Value
        {
            get { return GetValue(); }
            set { SetValue(value); }
        }

        public Type PropertyType
        {
            get { return _type; }
        }

        public MemberInfo MemberInfo
        {
            get { return GetMemberInfo(); }
        }

        #endregion

        #region Private Vars

        private readonly object _target;

        private FieldInfo _fieldInfo;

        private MethodInfo _propertyGetter;

        private MethodInfo _propertySetter;

        private PropertyInfo _propertyInfo;

        private Type _type;

        #endregion

        #region Public Methods

        public object GetValue()
        {
            object value = null;

            if (_propertyInfo == null)
                value = GetFieldValue();
            else
                value = GetPropertyValue();

            return value;
        }

        public void SetValue(object value)
        {
            if (_propertyInfo == null)
                SetFieldValue(value);
            else
                SetPropertyValue(value);
        }

        #endregion

        #region Private Methods

        private OSCReflectionProperty(object target, PropertyInfo propertyInfo)
        {
            _target = target;
            _propertyInfo = propertyInfo;
            _propertyGetter = propertyInfo.GetGetMethod();
            _propertySetter = propertyInfo.GetSetMethod();
            _type = propertyInfo.PropertyType;
        }

        private OSCReflectionProperty(object target, FieldInfo fieldInfo)
        {
            _target = target;
            _fieldInfo = fieldInfo;
            _type = fieldInfo.FieldType;
        }

        private OSCReflectionProperty() { }

        private MemberInfo GetMemberInfo()
        {
            if (_propertyInfo == null)
                return _fieldInfo;
            else
                return _propertyInfo;
        }

        private object GetFieldValue()
        {
            return _fieldInfo.GetValue(_target);
        }

        private void SetFieldValue(object value)
        {
            if (_fieldInfo.IsLiteral)
                return;

            if (value == null || PropertyType.IsAssignableFrom(value.GetType()))
            {
                _fieldInfo.SetValue(_target, value);
            }
            else
            {
                _fieldInfo.SetValue(_target, Convert.ChangeType(value, PropertyType));
            }
        }

        private object GetPropertyValue()
        {
            if (_propertyGetter == null)
                return null;

            return _propertyGetter.Invoke(_target, null);
        }

        private void SetPropertyValue(object value)
        {
            if (_propertySetter == null)
                return;

            var parameters = new object[1];

            if (value == null || PropertyType.IsAssignableFrom(value.GetType()))
            {
                parameters[0] = value;
            }
            else
            {
                parameters[0] = Convert.ChangeType(value, PropertyType);
            }

            _propertySetter.Invoke(_target, parameters);
        }

        #endregion
    }
}