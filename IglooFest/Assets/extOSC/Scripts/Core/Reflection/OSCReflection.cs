/* Copyright (c) 2017 ExT (V.Sigalkin) */

using System;
using System.Reflection;
using System.Collections.Generic;

namespace extOSC.Core.Internals
{
    public static class OSCReflection
    {
        #region Static Public Methods

        public static MethodInfo[] GetMethodsInfo(object target, Type[] parameterTypes, BindingFlags bindingAttr = BindingFlags.Public | BindingFlags.Instance)
        {
            if (target == null) return null;

            var tempMethods = new List<MethodInfo>();
            var targetMethods = target.GetType().GetMethods(bindingAttr);

            foreach (var methodInfo in targetMethods)
            {
                var methodParameters = methodInfo.GetParameters();
                if (methodParameters.Length != parameterTypes.Length) continue;

                var include = true;

                for (int i = 0; i < methodParameters.Length; i++)
                {
                    var parameter = methodParameters[i];
                    var type = parameterTypes[i];

                    if (!parameter.ParameterType.Equals(type))
                    {
                        include = false;
                        break;
                    }
                }

                if (include)
                    tempMethods.Add(methodInfo);
            }

            return tempMethods.ToArray();
        }

        public static MemberInfo[] GetProperties(object target, Type propertyType, BindingFlags bindingAttr = BindingFlags.Public | BindingFlags.Instance)
        {
            if (target == null) return null;

            var tempMembers = new List<MemberInfo>();
            var targetMembers = target.GetType().GetMembers(bindingAttr);

            foreach (var memberInfo in targetMembers)
            {
                var fieldInfo = memberInfo as FieldInfo;
                if (fieldInfo != null)
                {
                    if (fieldInfo.FieldType == propertyType)
                    {
                        tempMembers.Add(fieldInfo);
                        continue;
                    }
                }

                var propertyInfo = memberInfo as PropertyInfo;
                if (propertyInfo != null)
                {
                    if (propertyInfo.PropertyType == propertyType)
                    {
                        tempMembers.Add(propertyInfo);
                    }
                }
            }

            return tempMembers.ToArray();
        }

        #endregion
    }
}