/* Copyright (c) 2017 ExT (V.Sigalkin) */

namespace extOSC.Core.Console
{
    public enum ConsoleMessageType
    {
        Received,

        Transmitted
    }

    public class OSCConsoleMessage
    {
        #region Public Vars

        public ConsoleMessageType Type;

        public OSCPacket Packet;

        public string Info;

        #endregion
    }
}