/* Copyright (c) 2017 ExT (V.Sigalkin) */

namespace extOSC
{
    public enum OSCValueType
    {
        Unknown,

        // Convertable
        Int,            // Tag: i
        Long,           // Tag: h
        True,           // Tag: T
        False,          // Tag: F
        Float,          // Tag: f
        Double,         // Tag: d
        String,         // Tag: s
        Null,           // Tag: N
        Impulse,        // Tag: I
        Char,           // Tag: c (char 32bit)
        Color,          // Tag: r (RGBA 32bit)
        Blob,           // Tag: b (byte array - osc)
        TimeTag,        // Tag: t (osc-time tag)

        // TODO:
        //MIDI,			// Tag: m (MIDI O_o)
        //Array			// Tag: "[" and "]"
    }
}