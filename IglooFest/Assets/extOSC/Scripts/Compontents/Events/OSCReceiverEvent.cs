/* Copyright (c) 2017 ExT (V.Sigalkin) */

using UnityEngine;
using UnityEngine.Events;

namespace extOSC.Components.Events
{
    public abstract class OSCReceiverEvent : OSCReceiverComponent
    { }

    public abstract class OSCReceiverEvent<T> : OSCReceiverEvent where T : UnityEventBase
    {
        #region Public Vars

        public T OnReceive
        {
            get { return onReceive; }
            set { onReceive = value; }
        }

        #endregion

        #region Protected Vars

        [SerializeField]
        protected T onReceive;

        #endregion
    }
}