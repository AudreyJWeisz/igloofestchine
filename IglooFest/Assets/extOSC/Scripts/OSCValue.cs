/* Copyright (c) 2017 ExT (V.Sigalkin) */

using UnityEngine;

using System;

namespace extOSC
{
    public class OSCValue
    {
        #region Static Public Methods

        public static OSCValue Long(long value)
        {
            return new OSCValue(OSCValueType.Long, value);
        }

        public static OSCValue Char(char value)
        {
            return new OSCValue(OSCValueType.Char, value);
        }

        public static OSCValue Color(Color value)
        {
            return new OSCValue(OSCValueType.Color, value);
        }

        public static OSCValue Blob(byte[] value)
        {
            return new OSCValue(OSCValueType.Blob, value);
        }

        public static OSCValue Int(int value)
        {
            return new OSCValue(OSCValueType.Int, value);
        }

        public static OSCValue Bool(bool value)
        {
            return new OSCValue(value ? OSCValueType.True : OSCValueType.False, value);
        }

        public static OSCValue Float(float value)
        {
            return new OSCValue(OSCValueType.Float, value);
        }

        public static OSCValue Double(double value)
        {
            return new OSCValue(OSCValueType.Double, value);
        }

        public static OSCValue String(string value)
        {
            return new OSCValue(OSCValueType.String, value);
        }

        public static OSCValue Null()
        {
            return new OSCValue(OSCValueType.Null, null);
        }

        public static OSCValue Impulse()
        {
            return new OSCValue(OSCValueType.Impulse, null);
        }

        public static OSCValue TimeTag(DateTime value)
        {
            return new OSCValue(OSCValueType.TimeTag, value);
        }

        public static char GetTag(OSCValueType valueType)
        {
            switch (valueType)
            {
                case OSCValueType.Unknown:
                    return 'N';
                case OSCValueType.Int:
                    return 'i';
                case OSCValueType.Long:
                    return 'h';
                case OSCValueType.True:
                    return 'T';
                case OSCValueType.False:
                    return 'F';
                case OSCValueType.Float:
                    return 'f';
                case OSCValueType.Double:
                    return 'd';
                case OSCValueType.String:
                    return 's';
                case OSCValueType.Null:
                    return 'N';
                case OSCValueType.Impulse:
                    return 'I';
                case OSCValueType.Blob:
                    return 'b';
                case OSCValueType.Char:
                    return 'c';
                case OSCValueType.Color:
                    return 'r';
                case OSCValueType.TimeTag:
                    return 't';
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public static OSCValueType GetValueType(char valueTag)
        {
            switch (valueTag)
            {
                case 'i':
                    return OSCValueType.Int;
                case 'h':
                    return OSCValueType.Long;
                case 'T':
                    return OSCValueType.True;
                case 'F':
                    return OSCValueType.False;
                case 'f':
                    return OSCValueType.Float;
                case 'd':
                    return OSCValueType.Double;
                case 's':
                    return OSCValueType.String;
                case 'N':
                    return OSCValueType.Null;
                case 'I':
                    return OSCValueType.Impulse;
                case 'b':
                    return OSCValueType.Blob;
                case 'c':
                    return OSCValueType.Char;
                case 'r':
                    return OSCValueType.Color;
                case 't':
                    return OSCValueType.TimeTag;
                default:
                    return OSCValueType.Unknown;
            }
        }

        /*
        // Do you really need this?
        public static Type GetRealType(OSCValueType valueType)
        {
            switch (valueType)
            {
                case OSCValueType.Unknown:
                    return null;
                case OSCValueType.Int:
                    return typeof(int);
                case OSCValueType.Long:
                    return typeof(long);
                case OSCValueType.True:
                    return typeof(bool);
                case OSCValueType.False:
                    return typeof(bool);
                case OSCValueType.Float:
                    return typeof(float);
                case OSCValueType.Double:
                    return typeof(double);
                case OSCValueType.String:
                    return typeof(string);
                case OSCValueType.Null:
                    return null;
                case OSCValueType.Impulse:
                    return null;
                case OSCValueType.Blob:
                    return typeof(byte[]);
                case OSCValueType.Char:
                    return typeof(char);
                case OSCValueType.Color:
                    return typeof(Color);
                case OSCValueType.TimeTag:
                    return typeof(DateTime);
                default:
                    return null;
            }
        }
        */

        #endregion

        #region Public Vars

        public object Value
        {
            get { return value; }
        }

        public OSCValueType Type
        {
            get { return type; }
        }

        public char Tag
        {
            get { return GetTag(type); }
        }

        public long LongValue
        {
            get { return GetValue<long>(OSCValueType.Long); }
            set { this.value = value; }
        }

        public char CharValue
        {
            get { return GetValue<char>(OSCValueType.Char); }
            set { this.value = value; }
        }

        public Color ColorValue
        {
            get { return GetValue<Color>(OSCValueType.Color); }
            set { this.value = value; }
        }

        public byte[] BlobValue
        {
            get { return GetValue<byte[]>(OSCValueType.Blob); }
            set { this.value = value; }
        }

        public int IntValue
        {
            get { return GetValue<int>(OSCValueType.Int); }
            set { this.value = value; }
        }

        public bool BoolValue
        {
            get { return type == OSCValueType.True; }
            set
            {
                this.value = value;
                this.type = value ? OSCValueType.True : OSCValueType.False;
            }
        }

        public float FloatValue
        {
            get { return GetValue<float>(OSCValueType.Float); }
            set { this.value = value; }
        }

        public double DoubleValue
        {
            get { return GetValue<double>(OSCValueType.Double); }
            set { this.value = value; }
        }

        public string StringValue
        {
            get { return GetValue<string>(OSCValueType.String); }
            set { this.value = value; }
        }

        public bool IsNull
        {
            get { return type == OSCValueType.Null; }
        }

        public bool IsImpulse
        {
            get { return type == OSCValueType.Impulse; }
        }

        public DateTime TimeTagValue
        {
            get { return GetValue<DateTime>(OSCValueType.TimeTag); }
            set { this.value = value; }
        }

        #endregion

        #region Protected Vars

        protected object value;

        protected OSCValueType type;

        #endregion

        #region Public Methods

        public OSCValue(OSCValueType type, object value)
        {
            this.value = value;
            this.type = type;
        }

        public char GetTag()
        {
            return GetTag(type);
        }

        public override string ToString()
        {
            if (type == OSCValueType.True || type == OSCValueType.False || type == OSCValueType.Null || type == OSCValueType.Impulse)
            {
                return string.Format("<OSCValue {0}>", GetTag());
            }
            else
            {
                return string.Format("<OSCValue {0}: {1}>", GetTag(), value);
            }
        }

        #endregion

        #region Private Methods

        private OSCValue() { }

        private T GetValue<T>(OSCValueType requiredType)
        {
            if (requiredType == type)
            {
                return (T)value;
            }

            return default(T);
        }

        #endregion
    }
}