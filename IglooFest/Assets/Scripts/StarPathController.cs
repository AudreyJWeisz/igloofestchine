﻿using UnityEngine;
using System.Collections;

public class StarPathController : MonoBehaviour {

    public bool IsGoingToTheRight;
    public bool HasBeenSpawnedByGeisha;
    public iTween.EaseType EaseTypeForAnimationCurve = iTween.EaseType.linear;
    public AudioClip HoverSFX, StarHitSFX;   

    private float TimeToCompletePath = 30f;
    private string pathName;
    private int randomStringIndex;
    private float WaitTimeAtSummit = 1.0f;
    private AudioSource audioSource;

    private Vector3 initialScale;
    private float scaleFactor = 1.05f;

    void Start ()
    {
        initialScale = transform.localScale;
        transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);

        if (!IsGoingToTheRight)
            transform.eulerAngles = new Vector3(0, 180, 0) + transform.eulerAngles;

        randomStringIndex = Random.Range(0, 3);
        WaitTimeAtSummit = GameParameters.Instance.StarHoveringTime;
        TimeToCompletePath = Random.Range(GameParameters.Instance.StarFlightLenght.x, GameParameters.Instance.StarFlightLenght.y) ;

        audioSource = GetComponent<AudioSource>();

        if (IsGoingToTheRight && HasBeenSpawnedByGeisha)
        {
            if (randomStringIndex == 0)
                pathName = "StarPathGeisha1_2";
            else if (randomStringIndex == 1)
                pathName = "StarPathGeisha1_2";
            else
                pathName = "StarPathGeisha1_2";
        }
        else if (HasBeenSpawnedByGeisha)
        {
            if (randomStringIndex == 0)
                pathName = "StarPathGeisha1";
            else if (randomStringIndex == 1)
                pathName = "StarPathGeisha1";
            else
                pathName = "StarPathGeisha1";
        }
        else if (IsGoingToTheRight && !HasBeenSpawnedByGeisha)
        {
            if (randomStringIndex == 0)
                pathName = "StarPathArcher1_2";
            else if (randomStringIndex == 1)
                pathName = "StarPathArcher1_2";
            else
                pathName = "StarPathArcher1_2";
        }
        else if (!HasBeenSpawnedByGeisha)
        {
            if (randomStringIndex == 0)
                pathName = "StarPathArcher1";
            else if (randomStringIndex == 1)
                pathName = "StarPathArcher1";
            else
                pathName = "StarPathArcher1";
        }
        

        if (IsGoingToTheRight)
        {
            iTween.PutOnPath(gameObject, iTweenPath.GetPath(pathName), 1);
            iTween.MoveTo(gameObject, iTween.Hash("oncomplete", "OnCompleteFirstHalfPath", "oncompletetarget", gameObject, "path", iTweenPath.GetPathReversed(pathName), "movetopath", false, "time", (TimeToCompletePath / 2), "delay", 0.0f, "easetype", EaseTypeForAnimationCurve));
        }            
        else
        {
            iTween.PutOnPath(gameObject, iTweenPath.GetPath(pathName), 0);
            iTween.MoveTo(gameObject, iTween.Hash("oncomplete", "OnCompleteFirstHalfPath", "oncompletetarget", gameObject, "path", iTweenPath.GetPath(pathName), "movetopath", false, "time", (TimeToCompletePath / 2), "delay", 0.0f, "easetype", EaseTypeForAnimationCurve));
        }            
    }

    void FixedUpdate ()
    {
        if (GameManager.Instance.currentScreen == ScreenType.SCORE_SCREEN)
            OnCompleteSecondHalfPath();

        if (transform.localScale.x < initialScale.x)
            transform.localScale = new Vector3(transform.localScale.x * scaleFactor, transform.localScale.y * scaleFactor, transform.localScale.z * scaleFactor);

    }


    public void OnCompleteFirstHalfPath()
    {
        //Debug.Log("completed first half!");
        StartCoroutine(CompleteStarSecondPartOfPath());
        audioSource.clip = HoverSFX;
        audioSource.Play();
    }

    private IEnumerator CompleteStarSecondPartOfPath ()
    {
        yield return new WaitForSeconds(WaitTimeAtSummit);
 
        if (IsGoingToTheRight && HasBeenSpawnedByGeisha)
        {
            if (randomStringIndex == 0)
                pathName = "StarPathGeisha1";
            else if (randomStringIndex == 1)
                pathName = "StarPathGeisha1";
            else
                pathName = "StarPathGeisha1";
        }
        else if (HasBeenSpawnedByGeisha)
        {
            if (randomStringIndex == 0)
                pathName = "StarPathGeisha1_2";
            else if (randomStringIndex == 1)
                pathName = "StarPathGeisha1_2";
            else
                pathName = "StarPathGeisha1_2";
        }
        else if (IsGoingToTheRight && !HasBeenSpawnedByGeisha)
        {
            if (randomStringIndex == 0)
                pathName = "StarPathArcher1";
            else if (randomStringIndex == 1)
                pathName = "StarPathArcher1";
            else
                pathName = "StarPathArcher1";
        }
        else if (!HasBeenSpawnedByGeisha)
        {
            if (randomStringIndex == 0)
                pathName = "StarPathArcher1_2";
            else if (randomStringIndex == 1)
                pathName = "StarPathArcher1_2";
            else
                pathName = "StarPathArcher1_2";
        }

        if (IsGoingToTheRight)
            iTween.MoveTo(gameObject, iTween.Hash("oncomplete", "OnCompleteSecondHalfPath", "oncompletetarget", gameObject, "path", iTweenPath.GetPathReversed(pathName), "time", (TimeToCompletePath / 2), "delay", 0.0f, "easetype", iTween.EaseType.linear));
        else
            iTween.MoveTo(gameObject, iTween.Hash("oncomplete", "OnCompleteSecondHalfPath", "oncompletetarget", gameObject, "path", iTweenPath.GetPath(pathName), "time", (TimeToCompletePath/2), "delay", 0.0f, "easetype", iTween.EaseType.linear));
        
    }


    public void OnCompleteSecondHalfPath ()
    {
        GameManager.Instance.CurrentlyVisibleTargets.Remove(this.gameObject);
        GameManager.Instance.numberOfStarSpawned--;     
        Destroy(this.gameObject);
    }

    public void IsHit (Vector3 worldPos)
    {
		WorldShakeController.Instance.BigSkake ();
        GetComponent<SphereCollider>().enabled = false;
        // Play the hit SFX
        GetComponent<AudioSource>().clip = StarHitSFX;

        GetComponent<AudioSource>().volume = 1f;
        GetComponent<AudioSource>().Play();
        // physic behavior
        iTween.Stop(gameObject);
        GetComponent<Collider>().isTrigger = false;
        Rigidbody _rigidBody = gameObject.AddComponent<Rigidbody>();
        _rigidBody.mass = 0.25f;
        _rigidBody.angularDrag = 0.1f;
        _rigidBody.AddForceAtPosition(new Vector3(0, 0, 1) * GameParameters.Instance.ImpactForce * 10, worldPos);
        // destroy the can after the SFX
        Destroy(gameObject, 2.2f);        
    }
}
