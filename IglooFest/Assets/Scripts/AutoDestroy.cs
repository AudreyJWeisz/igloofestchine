﻿using UnityEngine;
using System.Collections;

public class AutoDestroy : MonoBehaviour {

    public float TimeBeforDestruction;

	// Use this for initialization
	IEnumerator Start () {
        yield return new WaitForSeconds(TimeBeforDestruction);
        Destroy(gameObject);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
