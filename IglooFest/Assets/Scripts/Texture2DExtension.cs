
using System;
using UnityEngine;
using System.IO;
public static class Texture2DExtension
{
    //Fill the texture with one solid color
    public static void FillWithColor(this Texture2D tex, Color color)
    {
        for (int w = 0; w < tex.width; w++)
        {
            for (int h = 0; h < tex.height; h++)
            {
                tex.SetPixel(w, h, color);		
            }
        }
        tex.Apply();
    }

    //Creates a new instance of the given Texture and copies pixels
    public static Texture2D Copy(this Texture2D tex)
    {
        Texture2D copy = new Texture2D(tex.width, tex.height);
        copy.SetPixels(tex.GetPixels());
        copy.Apply();
        return copy;
    }

    public static void SaveToDisc(this Texture2D tex, string fileName)
    {
        byte[] file = tex.EncodeToPNG();
        File.WriteAllBytes(Application.dataPath + "/" + fileName + ".png", file);
    }

    public static void DrawLine(this Texture2D tex, int x0, int y0, int x1, int y1, Color col)
    {
        int dy = (int)(y1 - y0);
        int dx = (int)(x1 - x0);
        int stepx, stepy;
        
        if (dy < 0)
        {
            dy = -dy;
            stepy = -1;
        } else
        {
            stepy = 1;
        }
        if (dx < 0)
        {
            dx = -dx;
            stepx = -1;
        } else
        {
            stepx = 1;
        }
        dy <<= 1;
        dx <<= 1;
        
        float fraction = 0;
        
        tex.SetPixel(x0, y0, col);
        if (dx > dy)
        {
            fraction = dy - (dx >> 1);
            while (Mathf.Abs(x0 - x1) > 1)
            {
                if (fraction >= 0)
                {
                    y0 += stepy;
                    fraction -= dx;
                }
                x0 += stepx;
                fraction += dy;
                tex.SetPixel(x0, y0, col);
            }
        } else
        {
            fraction = dx - (dy >> 1);
            while (Mathf.Abs(y0 - y1) > 1)
            {
                if (fraction >= 0)
                {
                    x0 += stepx;
                    fraction -= dy;
                }
                y0 += stepy;
                fraction += dx;
                tex.SetPixel(x0, y0, col);
            }
        }
    }

}

