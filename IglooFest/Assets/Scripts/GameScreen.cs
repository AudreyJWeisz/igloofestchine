﻿using UnityEngine;
using System.Collections;

public enum ScreenType
{
    START_SCREEN,
    COUNTDOWN_SCREEN,
    GAMEPLAY_SCREEN,
    SCORE_SCREEN,
    LEADERBOARD_SCREEN,
    FOREGROUND_CITY,
    NEXT_TEAM_FOREGROUND,
    CONNECTION_ERROR
}


[System.Serializable]
public class GameScreen : System.Object {
    public ScreenType ScreenType;
    public GameObject ScreenRootObject;
	
}
