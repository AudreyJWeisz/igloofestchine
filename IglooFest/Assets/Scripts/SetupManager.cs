﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;


public enum SetupStage
{
    NONE,
    SCREEN_DISTANCE,
    FAR,
    NEAR,
    RIGHT,
    LEFT,
    TOP,
    DOWN,
}

public class SetupManager : MonoBehaviour {
    public Text Instructions;
    public Text Step;
    public Text RightValue;
    public Text LeftValue;
    public Text TopValue;
    public Text BottomValue;
    public Text ScreenValue;
    public Text FarValue;
    public Text NearValue;
    public SetupStage CurrentStage;

    private const string ScreenInstructions = "Press the DOWN key until you see black pixels in projection canvas. Then press ENTER.";
    private const string RightInstructions = "Press the UP key to enlarge the detection to the right or DOWN to narrow it. Then press ENTER.";
    private const string LeftInstructions = "Press the UP key to enlarge the detection to the left or DOWN to narrow it. Then press ENTER.";
    private const string TopInstructions = "Press the UP key to enlarge the detection to the top or DOWN to narrow it. Then press ENTER.";
    private const string BottomInstructions = "Press the UP key to enlarge the detection to the bottom or DOWN to narrow it. Press ENTER to leave the setup.";
    private const string NearInstructions = "Press the UP key to enlarge the detection to the back or DOWN to narrow it. Then press ENTER.";
    private const string FarInstructions = "Press the DOWN key until you do not see the projection canvas anymore. The square should now be almost black. Then press ENTER.";

    private string ScreenStep = ((int)(SetupStage.SCREEN_DISTANCE)).ToString() + "/7 Projection canvas distance.";
    private string RightStep = ((int)(SetupStage.RIGHT)).ToString() + "/7 Right Detection limit.";
    private string LeftStep = ((int)(SetupStage.LEFT)).ToString() + "/7 Left Detection limit.";
    private string TopStep = ((int)(SetupStage.TOP)).ToString() + "/7 Top Detection limit.";
    private string BottomStep = ((int)(SetupStage.DOWN)).ToString() + "/7 Bottom Detection limit.";
    private string NearStep = ((int)(SetupStage.NEAR)).ToString() + "/7 Front Detection limit.";
    private string FarStep = ((int)(SetupStage.FAR)).ToString() + "/7 Back Detection limit.";

    // Use this for initialization
    void Start ()
    {
        if (Application.loadedLevel == 1)
        {
            CurrentStage = SetupStage.NONE;
            NextStage();
        }
    }

    public void NextStage()
    {
        if ((int)CurrentStage == Enum.GetNames(typeof(SetupStage)).Length - 1)
            Application.LoadLevel("MainGameplay");
        CurrentStage++;
        switch(CurrentStage)
        {
            case SetupStage.SCREEN_DISTANCE:
                Step.text = ScreenStep;
                Instructions.text = ScreenInstructions;
                break;
            case SetupStage.RIGHT:
                Step.text = RightStep;
                Instructions.text = RightInstructions;
                break;
            case SetupStage.LEFT:
                Step.text = LeftStep;
                Instructions.text = LeftInstructions;
                break;
            case SetupStage.TOP:
                Step.text = TopStep;
                Instructions.text = TopInstructions;
                break;
            case SetupStage.DOWN:
                Step.text = BottomStep;
                Instructions.text = BottomInstructions;
                break;
            case SetupStage.NEAR:
                Step.text = NearStep;
                Instructions.text = NearInstructions;
                break;
            case SetupStage.FAR:
                Step.text = FarStep;
                Instructions.text = FarInstructions;
                break;
        }
    }
}
