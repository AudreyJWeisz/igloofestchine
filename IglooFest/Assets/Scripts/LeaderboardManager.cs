﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using System.IO;
using System.Text;


public class LeaderboardManager : MonoBehaviour {    

    public static LeaderboardManager Instance;

    public Dictionary<string, float> scores;
    
    [HideInInspector]
    public string filePath = "";

    private string fileName = "SapporoHuntScores.csv";
    private string folder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments);


    private string[] validDateFormats = new string[] { "yyyy/MM/dd HH:mm:ss" };


	// Use this for initialization
	void Awake ()
	{
		Instance = this;
	}	


    // Use this for initialization
    void Start ()
    {
        Instance = this;
        filePath = System.IO.Path.Combine(folder, fileName);
        scores = new Dictionary<string, float>();
	}	


    public void FillLeaderboardWithFileEntries (DateTime startDate)
    {   
        try
        {
            DateTime dateEnteredInFile = new DateTime();
            DateTime startDateAfterNoon = new DateTime(startDate.Year, startDate.Month, startDate.Day, 13, 00, 00);
            string line;
            if (File.Exists(filePath))
            {
                string backUpFileName = System.IO.Path.Combine(folder, "SapporoHuntScoresBackUp_" + DateTime.Now.ToString("yyyy-MM-dd HH.mm.ss") + ".csv");
                File.Copy(filePath, backUpFileName, true);

                scores.Clear();
                StreamReader theReader = new StreamReader(filePath, Encoding.Default);
                using (theReader)
                {
                    int lineNumber = 0;
                    do
                    {
                        line = theReader.ReadLine();
                        
						if (!string.IsNullOrEmpty(line) && !line.Contains("Date,Team Name,Email,Score,LeaderName,LeaderGivenName,IsokWithTerms"))
                        {                            
                            string stringDateInFile = line.Split(',')[0];                            
                            if (!string.IsNullOrEmpty(stringDateInFile))
                            {
                                dateEnteredInFile = DateTime.ParseExact(stringDateInFile, validDateFormats, System.Globalization.CultureInfo.InstalledUICulture, System.Globalization.DateTimeStyles.None);
                                if (dateEnteredInFile >= startDateAfterNoon)
                                {
                                    AddScoreFromFileToLeaderboard(line.Split(',')[1], float.Parse(line.Split(',')[3]), lineNumber);
                                }                                
                            }
                            
                        }
                        lineNumber++;
                    }
                    while (line != null);
                    // Done reading, close the reader and return true to broadcast success    
                    theReader.Close();
                }                
            }
            HighScoreUi.Instance.SetHighScoreText();
        }
        catch (Exception e)
        {
            Debug.LogError(e.Message);
            GameManager.Instance.ConnectionErrorPanel.SetActive(true);
            GameManager.Instance.ConnectionErrorPanel.GetComponentInChildren<UnityEngine.UI.Text>().text = "Une erreur est survenue dans la lecture des scores dans le fichier. Message: " + e.Message + "\nVeuillez redémarrer le jeu et la side app.";
        }          
    }

    private void AddScoreFromFileToLeaderboard (string teamName, float score, int teamNumber)
    {
        string teamWithStamp = "";
        if (teamNumber < 10)
            teamWithStamp = teamName + "-_-" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.FFFF") + teamNumber;
        else if (teamNumber < 100)
            teamWithStamp = teamName + "-_-" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.FFF") + teamNumber;
        else if (teamNumber < 1000)
            teamWithStamp = teamName + "-_-" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.FF") + teamNumber;
        else
            teamWithStamp = teamName + "-_-" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.F") + teamNumber;
        scores.Add(teamWithStamp, score);
    }


    public void AddScoreToLeaderboard (string teamName, float score)
    {
        string teamWithStamp = teamName + "-_-" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.FFFFF");        
        scores.Add(teamWithStamp, score);        
    }

    public void RemoveTeamFromLeaderboard (int positionNumber)
    {
        DictionaryEntry leaderboardElement = new DictionaryEntry();
        int index = 1;
        bool foundEntry = false;
        foreach (var item in scores.OrderByDescending(i => i.Value))
        {
            //Debug.Log("found item : " + item.Key + " , " + item.Value + " at position " + index);
            if (index == positionNumber)
            {
                //Debug.Log("found element for position: " + positionNumber + " : " + item.Key + " , " + item.Value);                
                leaderboardElement.Key = item.Key;
                leaderboardElement.Value = item.Value;
                foundEntry = true;
                break;
            }
            index++;
        }
        if (foundEntry)
            scores.Remove((string)leaderboardElement.Key);
    }

    public DictionaryEntry GetLeaderboardElementAtPosition (int positionNumber)
    {        
        int index = 1;
        DictionaryEntry leaderboardElement = new DictionaryEntry();
        bool foundEntry = false;

        foreach (var item in scores.OrderByDescending(i => i.Value))
        {
           // Debug.Log("found item : " + item.Key + " , " + item.Value + " at position " + index);
            if (index == positionNumber)
            {
                //Debug.Log("found element for position: " + positionNumber + " : " + item.Key + " , " + item.Value);                
                leaderboardElement.Key = item.Key.Split(new string[] { "-_-" }, StringSplitOptions.None)[0];
                leaderboardElement.Value = item.Value;
                foundEntry = true;
                break;
            }
            index++;
        }

        if (!foundEntry)
        {
            leaderboardElement.Key = " Équipe Sapporo Team";
            leaderboardElement.Value = 0.0f;
        }
        
        return leaderboardElement;
    }

    public int GetPositionForTeam (string teamName)
    {
        DateTime teamTimePlayed = new DateTime(2000, 1, 1);

        int position = 1;
        int index = -1;

        foreach (var item in scores.OrderByDescending(i => i.Value))
        {            
            if (item.Key.Split(new string[] { "-_-" }, StringSplitOptions.None)[0].ToUpper().Equals(teamName.ToUpper()) && DateTime.Parse(item.Key.Split(new string[] { "-_-" }, StringSplitOptions.None)[1]) > teamTimePlayed)
            {
                teamTimePlayed = DateTime.Parse(item.Key.Split(new string[] { "-_-" }, StringSplitOptions.None)[1]);
                index = position;              
            }
            position++;
        }

        return index;            
    }

	// get today the hight score 
	public float GetHighScore()
    {
        if (scores == null)return 0;
        if (scores.Count == 0) return 0;
        // get the best score in the dictionary
        float score = (float)GetLeaderboardElementAtPosition(1).Value; // this also work but the other one is lighter
        Debug.Log("Current high score:" + score);
        return score;
       // return scores.OrderByDescending (i => i.Value).ElementAt(0).Value;

    }



    public void PrintLeaderboardEntries ()
    {
        foreach (var item in scores)
        {
            Debug.Log("Team: " + item.Key + ", Score: " + item.Value);
        }
    }
}
