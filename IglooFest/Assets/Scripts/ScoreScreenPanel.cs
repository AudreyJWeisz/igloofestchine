﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UTJ;

public class ScoreScreenPanel : MonoBehaviour {

	public Text teamName;
	public Text text1;
	public Text teamScore;
	public Text sapporoLiter;

	private float animScore;

	public static ScoreScreenPanel Instance;
	public GameObject audioPrefab;
	public AudioClip resultAudioClip;

	void Awake()
	{
		Instance = this;
	}

	void OnEnable()
	{
		StartCoroutine (AnimScreen ());
	}

	IEnumerator AnimScreen()
	{

		teamName.gameObject.SetActive (false);
		text1.gameObject.SetActive (false);
		teamScore.gameObject.SetActive (false);
		sapporoLiter.gameObject.SetActive (false);

		//m_imagePreview.texture = FindObjectOfType<IMovieRecorder> ().GetScratchBuffer ();

		yield return new WaitForSeconds (1f);
		PlayAudioPrefab ();
		teamName.gameObject.SetActive (true);
		teamName.transform.DOPunchScale (new Vector3 (1.2f, 1.2f, 1.2f), 0.3f);

		yield return new WaitForSeconds (0.5f);
		PlayAudioPrefab ();
		text1.gameObject.SetActive (true);

		yield return new WaitForSeconds (0.5f);
		PlayAudioPrefab ();
		teamScore.gameObject.SetActive (true);
		teamScore.text = "0";
		float targetvalue = float.Parse (GameManager.Instance.CurrentScore.text);
		DOTween.To (x=> animScore=x,0,targetvalue,1).OnUpdate(UpdateScore);
		//teamScore.transform.DOPunchScale (new Vector3 (1.3f, 1.3f, 1.3f), 0.3f);

		yield return new WaitForSeconds (0.1f);
		PlayAudioPrefab ();
		sapporoLiter.gameObject.SetActive (true);
	}

	void UpdateScore()
	{
		teamScore.text	= Mathf.Round(animScore).ToString();
	}

	public void PlayAudioPrefab()
	{
		GameObject go = Instantiate (audioPrefab, Vector3.zero, Quaternion.identity) as GameObject;
		go.GetComponent<AudioSource> ().PlayOneShot(resultAudioClip);
        go.GetComponent<AudioSource>().volume = 0.4f;

    }
}
