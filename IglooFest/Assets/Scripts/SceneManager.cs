﻿using UnityEngine;
using System.Collections;

public class SceneManager : MonoBehaviour {

	// Use this for initialization
	void Start ()
    {
        DontDestroyOnLoad(gameObject);
    }
	
	// Update is called once per frame
	void Update () {
        int levelToLoad = -1;
        if (Input.GetKey(KeyCode.Escape))
        {
            levelToLoad = 0;
        }

        if (Input.GetKey(KeyCode.LeftControl) && Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.D))
        {
            levelToLoad = 1;
        }

        if (Input.GetKey(KeyCode.LeftControl) && Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.H))
        {
            levelToLoad = 2;
        }

        if (levelToLoad != -1 && levelToLoad != Application.loadedLevel)
            Application.LoadLevel(levelToLoad);
    }
}
