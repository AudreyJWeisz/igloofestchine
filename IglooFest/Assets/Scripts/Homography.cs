﻿using UnityEngine;
using System.Collections;
using OpenCVForUnity;
using System.Collections.Generic;
using MathNet.Numerics.LinearAlgebra.Double;
using System.Xml.Serialization;
using System.IO;
using System;
using UnityEngine.UI;

public struct SerializedMat
{
    public double M00;
    public double M01;
    public double M02;

    public double M10;
    public double M11;
    public double M12;

    public double M20;
    public double M21;
    public double M22;
}

public class Homography : MonoBehaviour {
    
    public bool IsAuto = true;
    public Image Chessboard;

    public const string FileName = "/homography.xml";

    private Mat srcMat;
    private Texture2D tex;
    private Texture2D calibrationTex;
    MatOfPoint2f destCorners;
    MatOfPoint2f corners;
    private bool chessboardDone;
    private bool calibrationDone;
    Size cornerSize = new Size(5, 4);
    int cornerCount;
    Point[] desPoints;
    int calibPointCount;

    const int width = 1920;
    const int height = 1080;
    private DenseMatrix projMat;
    private Mat calibrationMat;
    private bool calibrationChessboardDone;
    private RenderTexture renderTex;
    private bool calibrationTexInitialized;
    SerializedMat homographyMat;

    public static void Save(SerializedMat homographyMat)
    {
        XmlSerializer ser = new XmlSerializer(typeof(SerializedMat));
        using (var stream = new StreamWriter(Application.streamingAssetsPath + FileName))
        {
            ser.Serialize(stream, homographyMat);
        }
    }

    // Use this for initialization
    void Start () {
        homographyMat = new SerializedMat();
        srcMat = new Mat(height, width, CvType.CV_8UC3);
        corners = new MatOfPoint2f();
        destCorners = new MatOfPoint2f();
        tex = new Texture2D(width, height, TextureFormat.RGBA32, false);
        GetComponent<Renderer>().material.SetTextureScale("_MainTex", new Vector2(1, 1));
        cornerCount = (int)cornerSize.width * (int)cornerSize.height;
        desPoints = new Point[cornerCount];

        calibrationTex = new Texture2D(Screen.width, Screen.height);

        calibrationMat = new Mat(Screen.height, Screen.width, CvType.CV_8UC3);

        renderTex = new RenderTexture(Screen.width, Screen.height, 0);
        RenderTexture.active = renderTex;

        StartCoroutine(ScreenShot());
    }

    IEnumerator ScreenShot()
    {
        yield return new WaitForSeconds(1);
        yield return new WaitForEndOfFrame();

        if (!chessboardDone)
        {
            chessboardDone = Calib3d.findChessboardCorners(srcMat, cornerSize, corners);
        }

        if (!calibrationTexInitialized)
        {
            calibrationTex.ReadPixels(new UnityEngine.Rect(0, 0, renderTex.width, renderTex.height), 0, 0);
            calibrationTex.Apply();
            calibrationTexInitialized = true;
            calibrationTex.SaveToDisc("calib");
        }
    }

    // Update is called once per frame
    void Update () {
        if (calibrationChessboardDone && !calibrationDone)
        {
            
            calibrationDone = true;
            if (IsAuto)
            {
                List<Point> destList = destCorners.toList();
                destCorners = new MatOfPoint2f(destList.ToArray());
            }else destCorners = new MatOfPoint2f(desPoints);
            Mat homography = Calib3d.findHomography(corners, destCorners);
            homographyMat.M00 = homography.get(0, 0)[0];
            homographyMat.M01 = homography.get(0, 1)[0];
            homographyMat.M02 = homography.get(0, 2)[0];

            homographyMat.M10 = homography.get(1, 0)[0];
            homographyMat.M11 = homography.get(1, 1)[0];
            homographyMat.M12 = homography.get(1, 2)[0];

            homographyMat.M20 = homography.get(2, 0)[0];
            homographyMat.M21 = homography.get(2, 1)[0];
            homographyMat.M22 = homography.get(2, 2)[0];

            projMat = new DenseMatrix(3, 3, new double[] { homographyMat.M00, homographyMat.M01, homographyMat.M02, homographyMat.M10, homographyMat.M11, homographyMat.M12, homographyMat.M20, homographyMat.M21, homographyMat.M22 });

            List<Point> cornerList = corners.toList();
            for (int i = 0; i < cornerList.Count; i++)
            {
                print(cornerList[i].x + " "+cornerList[i].y);
                DenseVector result = new DenseVector(new double[3] { cornerList[i].x, cornerList[i].y, 1 }) * projMat;

                Vector3 screenPoint = new Vector3((float)(result[0] / result[2]), (float)(result[1] / result[2]), 10);
                
                Vector3 worldPt = Camera.main.ScreenToWorldPoint(screenPoint);

                GameObject test = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                test.transform.position = worldPt;
                float colorValue = (float)i / (float)cornerList.Count;
                test.GetComponent<Renderer>().material.color = new Color(colorValue, colorValue, colorValue); 
            }

            Save(homographyMat);

        }

        if (Input.GetMouseButtonUp(0) && !calibrationDone)
        {
            desPoints[calibPointCount++] = new Point(Input.mousePosition.x, Input.mousePosition.y);
            calibrationChessboardDone = calibPointCount == cornerCount;
        }

        if (chessboardDone)
        {
            Calib3d.drawChessboardCorners(srcMat, cornerSize, corners, chessboardDone);

            if (calibrationTexInitialized && !calibrationChessboardDone && IsAuto)
            {
                Utils.texture2DToMat(calibrationTex, calibrationMat);
                calibrationChessboardDone = Calib3d.findChessboardCorners(calibrationMat, cornerSize, destCorners);
                Calib3d.drawChessboardCorners(calibrationMat, cornerSize, destCorners, calibrationChessboardDone);

                Utils.matToTexture2D(calibrationMat, calibrationTex);
                Chessboard.sprite = Sprite.Create(calibrationTex, new UnityEngine.Rect(0, 0, calibrationTex.width, calibrationTex.height), Vector2.one * 0.5f);
                Utils.matToTexture2D(srcMat, tex);
                GetComponent<Renderer>().material.mainTexture = tex;
            }
        }
    }
}
