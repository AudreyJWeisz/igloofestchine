﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MegaBeerCanMotion : MonoBehaviour {

    //Note: Its mass is set to 1 in the inspector.

    public List<AudioClip> HitSFXs;
    public float rotationSpeed = 1000;
    public GameObject starsEffectPrefab;
    public GameObject hitEffectPrefab;
    public GameObject destroyExplosionPrefab;

    private float DelayBeforeDestroyOnceInvisible = 2.0f;
    private float invisibilityCounter;
    private bool isCurrentlyVisible = false;
    private Rigidbody beerBody;
    private Vector2 speed;
    private bool hasBeenHit = false;
    private bool hasBeenHit2 = false;
    private Vector3 originalScale;
    private Vector3 hitPosition;
    private RainCameraController freezeEffect; 

    // Use this for initialization
    void Start () {

        GetComponent<AudioSource>().clip = HitSFXs[0];
        GetComponent<AudioSource>().pitch = Random.Range(0.85f, 1.15f);

        beerBody = GetComponent<Rigidbody>();

        invisibilityCounter = 0;

        speed = GameParameters.Instance.MegaBeerSpeed;

        beerBody.constraints = RigidbodyConstraints.FreezePositionZ;

        originalScale = this.gameObject.transform.localScale = new Vector3(0, 0, 0);
        transform.DORotate(new Vector3(0, 0, 360), 0.5f);
        transform.DOScale(new Vector3(500, 500, 500), 0.5f).SetEase(Ease.InCirc);

        Vector3 childCenter = this.gameObject.transform.Find("Center").localPosition;

        GameObject stars = Instantiate(starsEffectPrefab, childCenter, Quaternion.identity) as GameObject;
        stars.GetComponent<ParticleSystem>().Play();
        Destroy(stars, 5f);

        freezeEffect = GameObject.Find("FBackground").GetComponent<RainCameraController>();
        freezeEffect.Alpha = 0; 
    }

    // Update is called once per frame
    void Update () {

        float rotation = rotationSpeed * Time.deltaTime;

        if (hasBeenHit)
        {
            this.gameObject.transform.Rotate(Vector3.up, rotation);
        }

        rotationSpeed -= 10;
        if (rotationSpeed <= 0)
        {
            rotationSpeed = 1000;
            hasBeenHit = false; 
        }
        if (hasBeenHit2)
        {
            GameObject hit = Instantiate(hitEffectPrefab, hitPosition, Quaternion.identity) as GameObject;
            hit.GetComponent<ParticleSystem>().Play();
            Destroy(hit, 5f);
            hasBeenHit2 = false; 
        }
    }

    public void CanIsHit(RaycastHit hit, Vector3 worldPos)
    {
        hitPosition = hit.point; 
        // Play the hit SFX
        GetComponent<AudioSource>().Play();

        beerBody.constraints = RigidbodyConstraints.None;

        hasBeenHit = true;
        hasBeenHit2 = true; 

        this.gameObject.transform.DOShakePosition(1f, 10, 10, 90, false, true);

        freezeEffect.Alpha += 0.04f;
        freezeEffect.Play();
    }


    public void OnDestroy()
    {
        Vector3 center = this.gameObject.transform.Find("Center").transform.position;
        GameObject explosion = Instantiate(destroyExplosionPrefab, center, Quaternion.identity) as GameObject;
        explosion.GetComponent<ParticleSystem>().Play();
        Destroy(explosion, 4f);
    }
}
