﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Pathfinding.Serialization.JsonFx;
using System.Linq;
using UTJ;
using MomentsRecorder;
//using UnityEngine.Windows;

public class JsonPictureInfo{
	public media media;
}

public class mediaListResult {
    public media[] media;
    public int result;
}

public class media{
    public string type;
	public string url;
    public string name;
    public int date;

}
public class TakePictureController : MonoBehaviour {


	public string CameraIP ="192.168.1.31" ;
    public const string CameraIpPlayerPrefKey = "CameraIP";
	public GstUnityBridgeTexture videoPlayer;
	// capture infos 
	private string cameraVideoURl;
	private string finalVidéoPath;

	/// <summary>
	///  capture parameters 
	/// </summary>
	public float videoTimeSeconds =4;

	private bool DebugShoot = false;

	public static TakePictureController singleton;

	// link to all the url of photo burst
	private List<string> urlList;
    private bool photoRequestEnd;


	void Awake()
	{
		singleton = this;
	}
	
	// Use this for initialization
	void Start () 
	{
		videoPlayer.gameObject.SetActive (false); 
        LoadCameraIp();
		urlList = new List<string> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
       

	}

    //CAMERA IP SETTINGS 
    //sav ip to player pref
    public void SavCameraIP(string ip) {
        CameraIP = ip;
        PlayerPrefs.SetString(CameraIpPlayerPrefKey,ip);
    }

    // load camera ip from player prefs 
    private void LoadCameraIp()
    {
        if(PlayerPrefs.HasKey(CameraIpPlayerPrefKey))
            CameraIP = PlayerPrefs.GetString(CameraIpPlayerPrefKey);
    }


    public void RecordPlay()
    {
        Debug.Log("RecordPlay");
        if (CameraRecordSystem.recordType == 0)
            StartCoroutine(CameraRecordPlay());
        else
            StartCoroutine(CameraRecordPlaySlowMotion());
    }


    IEnumerator CameraRecordPlay()
    {
        photoRequestEnd = false;
        urlList.Clear ();
		SnapPicture (); 
		yield return new WaitForSeconds(3);
		SnapPicture (); 
    }

    IEnumerator CameraRecordPlaySlowMotion()
    {
        RecordVideo();
        yield return new WaitForSeconds(2);
        StopRecordVideo();
    }


    public WWW SnapPicture()
	{
		WWW www;
		Hashtable postHeader = new Hashtable();
		postHeader.Add("Content-Type", "application/json");

		// convert json string to byte
		var formData = System.Text.Encoding.UTF8.GetBytes("{\"command\":\"snapPicture\"}");

		www = new WWW("http://"+CameraIP+"/virb", formData);
		StartCoroutine(WaitForRequestSnapPicture(www));
		return www;
	}

	IEnumerator WaitForRequestSnapPicture(WWW data)
	{
		yield return data; // Wait until the download is done
		if (data.error != null)
		{
			Debug.Log("There was an error sending request: " + data.error);
		}
		else
		{
			//Debug.Log("WWW Request: " + data.text);
			JsonPictureInfo picInfo = JsonReader.Deserialize<JsonPictureInfo> (data.text);
            //JsonUtility.FromJson<JsonPictureInfo> (data.text);
            //Debug.Log(picInfo.media.name);
            GetUrlListOfPhotoBrust(picInfo.media.name);
		}
	}


	// PHOTO BURST LIST 

	public WWW GetUrlListOfPhotoBrust(string name)
	{
		WWW www;
		Hashtable postHeader = new Hashtable();
		postHeader.Add("Content-Type", "application/json");

		// convert json string to byte
		var formData = System.Text.Encoding.UTF8.GetBytes("{\"command\":\"mediaList\"}");

		www = new WWW("http://"+CameraIP+"/virb", formData);
		StartCoroutine(WaitForRequestPhotoBurst(www,name));
		return www;
	}


	IEnumerator WaitForRequestPhotoBurst(WWW data,string name)
	{
		yield return data; // Wait until the download is done
		if (data.error != null)
		{
			Debug.Log("There was an error sending request: " + data.error);
		}
		else
		{
			mediaListResult picInfo = JsonReader.Deserialize<mediaListResult> (data.text);
			var picrootName = name.Split('-');

			foreach(media m in picInfo.media)
			{
                if(!movieRecordIsRunning) // just in cas there is a movie recording but other picture are arriving
				    if (m.name.Split ('-')[0] == picrootName[0] ||m.name == picrootName[0]+".JPG" )
					    urlList.Add (m.url);
			}
            photoRequestEnd = true;

            Debug.Log("photo request end");
		}
	}

	// ======================================================================




	IEnumerator downloadImg (string url)
	{
		Debug.Log (url);
		Texture2D texture = new Texture2D(1,1);
		WWW www = new WWW(url);
		yield return www;
		www.LoadImageIntoTexture(texture);

		Sprite image = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
		//renderer.sprite = image;
	}


	// VIDEO 

	public WWW RecordVideo()
	{
		WWW www;
		Hashtable postHeader = new Hashtable();
		postHeader.Add("Content-Type", "application/json");

		// convert json string to byte
		var formData = System.Text.Encoding.UTF8.GetBytes("{\"command\":\"startRecording\"}");

		www = new WWW("http://"+CameraIP+"/virb", formData);
		//StartCoroutine(WaitForRequest(www));
		return www;
	}


	public WWW StopRecordVideo()
	{
		WWW www;
		Hashtable postHeader = new Hashtable();
		postHeader.Add("Content-Type", "application/json");

		// convert json string to byte
		var formData = System.Text.Encoding.UTF8.GetBytes("{\"command\":\"stopRecording\"}");

		www = new WWW("http://"+CameraIP+"/virb", formData);
		StartCoroutine(WaitForStopRecordResult(www));
		return www;
	}


	IEnumerator WaitForStopRecordResult(WWW data)
	{
		yield return data; // Wait until the download is done
		if (data.error != null)
		{
			Debug.Log("There was an error sending request: " + data.error);
		}
		else
		{
			//Debug.Log("WWW Request: " + data.text);
			JsonPictureInfo picInfo = JsonReader.Deserialize<JsonPictureInfo> (data.text);
			RetrievLastVideoRequest ();
		}
	}


	public WWW RetrievLastVideoRequest()
	{
		WWW www;
		Hashtable postHeader = new Hashtable();
		postHeader.Add("Content-Type", "application/json");
      // convert json string to byte
      var formData = System.Text.Encoding.UTF8.GetBytes("{\"command\":\"mediaList\"}");
        //var formData = System.Text.Encoding.UTF8.GetBytes("{\"command\":\"mediaList\",\"path\":\"//DCIM//100_VIRB\"}");

        www = new WWW("http://"+CameraIP+"/virb", formData);
		StartCoroutine(WaitForMediaList(www));
		return www;
	}



	IEnumerator WaitForMediaList(WWW data)
	{
		yield return data; // Wait until the download is done
		if (data.error != null)
		{
			Debug.Log("There was an error sending request: " + data.error);
		}
		else
		{
			Debug.Log("WWW Request: " + data.text);
            mediaListResult result = JsonReader.Deserialize<mediaListResult>(data.text);
			cameraVideoURl = result.media [result.media.Length - 1].url;

			//TexturePlayer.m_URI = cameraVideoURl;
		}
	}


	public void StartMovieFramedRecord(string DataPath)
	{
		if (CameraRecordSystem.recordType == 0)
			StartCoroutine (MovieFramedRecord(DataPath));
		else
			StartCoroutine (MovieFramedRecordSlowMotion(DataPath));
	}

	private List<Sprite> spriteFromCamera;

	IEnumerator GetAllSpriteFromCamera()
	{
        spriteFromCamera = new List<Sprite>();
        foreach (string s in urlList)
        {
			//Debug.Log (s);
            WWW www = new WWW(s);
            yield return www;
            spriteFromCamera.Add(Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0.5f, 0.5f)));
            //Debug.Log ("Get sprite from camera" + spriteFromCamera.Count);
            //www.Dispose();
            Debug.Log("GET PIC");
        }
    }

    bool movieRecordIsRunning = false;

// TODO move to camera record system 
	IEnumerator MovieFramedRecord(string DataPath)
	{
        movieRecordIsRunning = true;
        GifIsSaved = false;
        Debug.Log("MovieFramedRecord");

        float timer = 0;
        while (!photoRequestEnd)
        {
            yield return null;
            timer += Time.deltaTime;
            //Debug.Log(timer);
            if (timer>=5)
            { 
                yield break;
                Debug.Log("end because no photo request from camera after 5s");
            }
        }

        StopCoroutine("GetAllSpriteFromCamera");
		yield return StartCoroutine ("GetAllSpriteFromCamera");

        // stop everithing if no images 
        if(spriteFromCamera.Count == 0)
        {
            Debug.Log("no picture");
            yield break;
            movieRecordIsRunning = false;
        }

	
		//ScoreScreenPanel.Instance.playVideoPreview (spriteFromCamera);
		CameraRecordSystem.Instance.pictureMotionForVideo.PlayAnimation (spriteFromCamera);
		yield return null; 

		// gif system 
        Application.targetFrameRate = 30;

        //GifOffscreenRecorder Gifrecorder = FindObjectOfType<GifOffscreenRecorder>();
        //		Gifrecorder.m_outputDir = new DataPath(UTJ.DataPath.Root.CurrentDirectory, CameraRecordSystem.Instance.currentDataPath);
        //		Gifrecorder.BeginRecording();
        //		Gifrecorder.recording = true;
        //		yield return new WaitForSeconds(2);
        //		Gifrecorder.Flush(CameraRecordSystem.Instance.currentTeamInfos.teamName);
        //		Gifrecorder.EndRecording();

        Recorder m_Recorder = FindObjectOfType<Recorder>();
        m_Recorder.OnFileSaved = OnFileSaved;
        m_Recorder.SaveFolder = CameraRecordSystem.Instance.currentFullDataPath;
        //m_Recorder.OnFileSaveProgress = OnFileSaveProgress;

        
        m_Recorder.Record();
        yield return new WaitForSeconds(2f);
        m_Recorder.Save(CameraRecordSystem.Instance.currentTeamInfos.teamName);


        Application.targetFrameRate = 60;
		yield return null;


        // VIDEO RECORD 
        /*
        AVProMovieCaptureBase recorder = FindObjectOfType<AVProMovieCaptureBase>();
        recorder._outputFolderPath = CameraRecordSystem.Instance.currentFullDataPath;
        recorder._forceFilename = CameraRecordSystem.Instance.currentTeamInfos.teamName + ".mp4";
        recorder.StartCapture ();
		yield return new WaitForSeconds (2f);
		recorder.StopCapture ();

		yield return null; 
        */

		CameraRecordSystem.Instance.pictureMotionForVideo.StopAnimation ();
		//ScoreScreenPanel.Instance.pictureMotionUI.StopAnimation ();
		ClearSprite();
     

        //yield break;

        while (!GifIsSaved)
            yield return null;

        // send to imgur
        yield return StartCoroutine(GifUploadToImgur.Instance.SendToimgurCoroutine(CameraRecordSystem.Instance.currentFullDataPath + "/" + CameraRecordSystem.Instance.currentTeamInfos.teamName + ".gif"));

		while (!GifUploadToImgur.Instance.didfinished)
			yield return null;


        if (GifUploadToImgur.Instance.succed)
		{
			Debug.Log ("StartMailSend");
            yield return StartCoroutine(SendMail.Instance.SendCoroutine(CameraRecordSystem.Instance.currentDataPath, CameraRecordSystem.Instance.currentTeamInfos, GifUploadToImgur.Instance.linkFileImgur));

            //yield return StartCoroutine(SendMail.Instance.SendCoroutine (recorder.LastFilePath,CameraRecordSystem.Instance.currentTeamInfos, GifUploadToImgur.Instance.linkFileImgur));
        }

        Resources.UnloadUnusedAssets ();
        movieRecordIsRunning = false;
    }


	private void ClearSprite()
	{
		foreach(Sprite s in spriteFromCamera)
		{
			Destroy (s);
		}	
		spriteFromCamera.Clear ();

	}

	// ============================= video slow motion Frame record 

	private bool videoStartPLaying= false; // does video started player on the frame 


	public void VideoStartPlaying()
	{
		videoStartPLaying= true; 
	}

	IEnumerator MovieFramedRecordSlowMotion(string DataPath)
	{
		Debug.Log("MovieFramedRecordSlowMotion");
		videoPlayer.gameObject.SetActive (true); 
		movieRecordIsRunning = true;
		GifIsSaved = false;
		videoPlayer.m_URI = cameraVideoURl;

		while (!videoStartPLaying)
			yield return null;

		// gif system 
		Application.targetFrameRate = 30;


		Recorder m_Recorder = FindObjectOfType<Recorder>();
		m_Recorder.OnFileSaved = OnFileSaved;
		m_Recorder.SaveFolder = CameraRecordSystem.Instance.currentFullDataPath;
		//m_Recorder.OnFileSaveProgress = OnFileSaveProgress;


		m_Recorder.Record();
		yield return new WaitForSeconds(2f);
		m_Recorder.Save(CameraRecordSystem.Instance.currentTeamInfos.teamName);

		Application.targetFrameRate = 60;
		yield return null;

		while (!GifIsSaved)
			yield return null;

		// send to imgur
		yield return StartCoroutine(GifUploadToImgur.Instance.SendToimgurCoroutine(CameraRecordSystem.Instance.currentFullDataPath + "/" + CameraRecordSystem.Instance.currentTeamInfos.teamName + ".gif"));

		while (!GifUploadToImgur.Instance.didfinished)
			yield return null;


		if (GifUploadToImgur.Instance.succed)
		{
			Debug.Log ("StartMailSend");
			yield return StartCoroutine(SendMail.Instance.SendCoroutine(CameraRecordSystem.Instance.currentDataPath, CameraRecordSystem.Instance.currentTeamInfos, GifUploadToImgur.Instance.linkFileImgur));
		}

		Resources.UnloadUnusedAssets ();
		movieRecordIsRunning = false;
		videoPlayer.gameObject.SetActive (false); 
		videoStartPLaying = false;
	}


    private bool GifIsSaved;

    void OnFileSaved(int id, string filepath)
    {
        GifIsSaved = true;
    }

    void OnFileSaveProgress(int id, float percent)
    {
        // This callback is probably not thread safe so use it at your own risks.
        // Percent is in range [0;1] (0 being 0%, 1 being 100%).
        Debug.Log( percent * 100f);
    }


}
