﻿using UnityEngine;
using System.Collections;

public class GeishaController : MonoBehaviour {
    public float TimeBeforeDestruction = 7;
    public bool IsPlacedOnRightSide = false;
    public AudioClip FanSFX;
    private float destructionDelayCounter;      
    private bool hasThrownStar = false;

    private Animator geishaAnim;

	public GameObject audioPrefab;
    
    void Awake ()
    {
        //destructionDelayCounter = 0.0f;
        geishaAnim = GetComponent<Animator>();
        //geishaAnim.speed = 0;
        //StartCoroutine(WaitBeforeStartingAnim());
    }
    void Start()
    {
        if (IsPlacedOnRightSide)
            GetComponent<AudioSource>().panStereo = 0.5f;
        else
            GetComponent<AudioSource>().panStereo = -0.5f;


    }

    // called by anim event
    void DestroyGeisha ()
    {         
        GameManager.Instance.numberOfGeishaSpawned--;
        Destroy(gameObject);
	}

    void PlayFanSFX()
    {
		GameObject audiopref = GameObject.Instantiate (audioPrefab, this.transform.position, Quaternion.identity) as GameObject;
		audiopref.GetComponent<AudioSource>().PlayOneShot(FanSFX);
		audiopref.GetComponent<AutoDestroy> ().TimeBeforDestruction = 8;
		ThrowStar();
    }

    private void ThrowStar ()
    {
        hasThrownStar = true;
        GameObject star = Instantiate(GameManager.Instance.StarPrefab, new Vector3(transform.position.x, transform.position.y, 0), GameManager.Instance.StarPrefab.transform.rotation) as GameObject;
        GameManager.Instance.numberOfStarSpawned++;
        GameManager.Instance.CurrentlyVisibleTargets.Add(star);
        star.GetComponent<StarPathController>().IsGoingToTheRight = !IsPlacedOnRightSide;
        star.GetComponent<StarPathController>().HasBeenSpawnedByGeisha = true;
        star.transform.SetParent(GameManager.Instance.GetScreenWithType(ScreenType.GAMEPLAY_SCREEN).gameObject.transform);        
    }
}
