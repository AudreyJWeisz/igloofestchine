﻿using UnityEngine;
using System.Collections;
using System.Text.RegularExpressions;

// team information storage class (it is better for code cleaning and refactoring)
public class TeamInformationData  {

    public string teamName;
    public string teamEmail;
    public string teamLeaderName;
    public string teamLeaderGivenName;
    public bool isOkWithTerms;
    public string score;


	public TeamInformationData()
	{
	}
		
    public TeamInformationData(string TeamName,string TeamEmail, string TeamLeaderName, string TeamLeaderGivenName, bool IsOkWithTerms)
    {
		teamName = RemoveSpecialCharacters(TeamName);
        teamEmail = TeamEmail;
        teamLeaderName = TeamLeaderName;
        teamLeaderGivenName = TeamLeaderGivenName;
        isOkWithTerms = IsOkWithTerms;
    }

	public static string RemoveSpecialCharacters(string input)
	{
		return Regex.Replace(input, "[^\\w\\._]", "");
	}
}
