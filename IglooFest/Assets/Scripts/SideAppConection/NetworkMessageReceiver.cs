﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.IO;
using System;


public class NetworkMessageReceiver : MonoBehaviour {

    public static NetworkMessageReceiver Instance;

    public Text[] CurrentTeamNameTexts;
    public Text[] NextTeamNameTexts;

    private bool hasSetUpLeaderboardDate = false;
    private string previousTeamName = "";
    private string previousNextTeamName = "";
    private string previousEmail = "";
    private string currentTeamName = "";
    private string currentTeamEmail = "";

    private TeamInformationData currentTeamInfo;

    private string filePath = "";
    private bool isWrittingForFirstTime = true;
    private bool hasSetUpDeviceName = false;

    private StreamWriter streamWriter;    
    private string lastEmailWritten = "";

    private string[] validDateFormats = new string[] { "yyyy-MM-dd" };

    // Use this for initialization
    void Start ()
    {
        Instance = this;        
    }
	
	// Update is called once per frame
	void Update () {
        while (true)
        {
            MessageData msg = Server.PopMessage();
            if (msg == null)
                break;
                
            if (!hasSetUpDeviceName && !string.IsNullOrEmpty(msg.sideAppDeviceName))
            {
                hasSetUpDeviceName = true;
                Debug.Log("received device name: " + msg.sideAppDeviceName);
                Client.singleton.deviceName = msg.sideAppDeviceName;
            }

            if (!hasSetUpLeaderboardDate && !string.IsNullOrEmpty((msg.leaderboardDate)))
            {
                hasSetUpLeaderboardDate = true;
                Debug.Log("received leaderboard date: " + msg.leaderboardDate);
                DateTime startDate = DateTime.ParseExact(msg.leaderboardDate, validDateFormats, System.Globalization.CultureInfo.InstalledUICulture, System.Globalization.DateTimeStyles.None);
                LeaderboardManager.Instance.FillLeaderboardWithFileEntries(startDate);
            }

            if (!previousTeamName.Equals(msg.currentTeamName))
            {   
                Debug.Log("received team name: " + msg.currentTeamName);
                foreach (Text teamName in CurrentTeamNameTexts)
                {
                    teamName.text = msg.currentTeamName;
                    currentTeamName = msg.currentTeamName;
                }
            }

            if (!previousNextTeamName.Equals(msg.nextTeamName))
            {
                Debug.Log("received next team: " + msg.nextTeamName);
                foreach (Text nextTeamName in NextTeamNameTexts)
                {
                    nextTeamName.text = msg.nextTeamName;
                }
            }

            if (!previousEmail.Equals(msg.currentTeamEmail))
            {
                Debug.Log("received email: " + msg.currentTeamEmail + " for team " + currentTeamName + "name leader:"+msg.currentTeamLeaderName);                
                currentTeamEmail = msg.currentTeamEmail;
                // store team information (refactor not completed yet so current team email var is stil used)
                currentTeamInfo = new TeamInformationData(currentTeamName, msg.currentTeamEmail, msg.currentTeamLeaderName, msg.currentTeamLeaderGivenName, msg.isOkWithTerms);
            }

            if (msg.isTeamReady)
            {              
                GameManager.Instance.StartNextGame();
            }

            previousTeamName = msg.currentTeamName;
            previousNextTeamName = msg.nextTeamName;
            previousEmail = msg.currentTeamEmail;            
        }
    }

    public TeamInformationData GetCurrentTeamInfos()
    {
        if (currentTeamInfo != null)
            return currentTeamInfo;
        else // debug 
            return currentTeamInfo = new TeamInformationData("Debug", "eddie@playmind.com", "eddie", "playmind", true);
    }


    public void WriteTeamScoreToFile (string score)
    {
        if (isWrittingForFirstTime)
        {
            filePath = LeaderboardManager.Instance.filePath;
            try
            {
                if (File.Exists(filePath))
                {
                    streamWriter = new StreamWriter(filePath, true);
                }
                else
                {
                    streamWriter = new StreamWriter(filePath, false);
                    streamWriter.WriteLine("Date,Team Name,Email,Score,LeaderName,LeaderGivenName,IsokWithTerms\n");                    
                }
            }
            catch (DirectoryNotFoundException e)
            {
                Debug.LogError("writing to file error message : " + e.Message);
                GameManager.Instance.ConnectionErrorPanel.SetActive(true);
                GameManager.Instance.ConnectionErrorPanel.GetComponentInChildren<Text>().text = "Une erreur est survenue dans l'écriture des scores dans le fichier. Message: " + e.Message + "\nVeuillez redémarrer le jeu et la side app.";
            }
            isWrittingForFirstTime = false;
        }
        //Debug.Log("Should write to file the following data : " + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "," + currentTeamName + "," + currentTeamEmail+","+score+","+currentTeamInfo.teamLeaderName+","+currentTeamInfo.teamLeaderGivenName+","+currentTeamInfo.isOkWithTerms);
        try
        {
            streamWriter.AutoFlush = true;
            //first wright date 
            streamWriter.Write("" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")+",");

            // team name 
            if (string.IsNullOrEmpty(currentTeamName))
                streamWriter.Write("NOMAME,");
            else
                streamWriter.Write(currentTeamName + ",");

            // team leader mail
            if (string.IsNullOrEmpty(currentTeamEmail))
                streamWriter.Write("NO MAIL,");
            else
                streamWriter.Write(currentTeamEmail + ",");

            // update december 2016 we had to add some new data but we desided to not changed the order of data to not break previous work 

            // score 
            streamWriter.Write(score + ",");

            // team leader name
            if (string.IsNullOrEmpty(currentTeamInfo.teamLeaderName))
                streamWriter.Write("NO NAME,");
            else
                streamWriter.Write(currentTeamInfo.teamLeaderName + ",");

            // team leader given name
            if (string.IsNullOrEmpty(currentTeamInfo.teamLeaderGivenName))
                streamWriter.Write("NO GIVEN NAME,");
            else
                streamWriter.Write(currentTeamInfo.teamLeaderGivenName + ",");


            if(currentTeamInfo.isOkWithTerms)
                streamWriter.Write("ok with terms");
            else
                streamWriter.Write("NOT ok with terms");

            streamWriter.Write( "\n");
            currentTeamName = "";
            currentTeamEmail = "";
        }
        catch (System.Exception e)
        {            
           // Debug.LogError(e.Message);
            GameManager.Instance.ConnectionErrorPanel.SetActive(true);
            GameManager.Instance.ConnectionErrorPanel.GetComponentInChildren<Text>().text = "Une erreur est survenue dans l'écriture des scores dans le fichier. Message: " + e.Message + "\nVeuillez redémarrer le jeu et la side app.";
        }              
    }

    void OnApplicationQuit ()
    {
        if (streamWriter != null)
            streamWriter.Close();
    }

}
