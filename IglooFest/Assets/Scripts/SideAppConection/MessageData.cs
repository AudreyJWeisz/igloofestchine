﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

//V1

[System.Serializable]
public class MessageData
{
    public string sideAppDeviceName = "";
    public string leaderboardDate = "";
    public string currentTeamName = "";
    public string currentTeamEmail = "";
    public string currentTeamLeaderName="";
    public string currentTeamLeaderGivenName = "";
    public bool isOkWithTerms = false;
    public string nextTeamName = "";    
    public bool isTeamReady = false;
    public bool hasTeamStartedPlaying = false;
    public bool hasTeamFinishedPlaying = false;

    
    public SerializableV2[] messageVector2Array;

    public static MessageData FromByteArray (byte[] input)
    {
        // Create a memory stream, and serialize.
        MemoryStream stream = new MemoryStream(input);

        // Create a binary formatter.
        BinaryFormatter formatter = new BinaryFormatter();

        MessageData data = new MessageData();

        try
        {
            data.sideAppDeviceName = (string)formatter.Deserialize(stream);
            data.leaderboardDate = (string)formatter.Deserialize(stream);
            data.currentTeamName = (string)formatter.Deserialize(stream);
            data.currentTeamEmail = (string)formatter.Deserialize(stream);
            data.currentTeamLeaderName = (string)formatter.Deserialize(stream);
            data.currentTeamLeaderGivenName = (string)formatter.Deserialize(stream);
            data.isOkWithTerms = (bool)formatter.Deserialize(stream);
            data.isTeamReady = (bool)formatter.Deserialize(stream);
            data.nextTeamName = (string)formatter.Deserialize(stream);            
            data.messageVector2Array = (SerializableV2[])formatter.Deserialize(stream);
            data.hasTeamStartedPlaying = (bool)formatter.Deserialize(stream);
            data.hasTeamFinishedPlaying = (bool)formatter.Deserialize(stream);
        }
        catch (SerializationException e)
        {
            Debug.LogError("Failed to deserialize. Reason: " + e.Message);
            throw;
        }



        return data;
    }

    public static byte[] ToByteArray (MessageData msg)
    {
        // Create a memory stream, and serialize.
        MemoryStream stream = new MemoryStream();

        // Create a binary formatter.
        BinaryFormatter formatter = new BinaryFormatter();

        if (msg.messageVector2Array == null)
        {
            SerializableV2[] zeroVectors = new SerializableV2[] { new SerializableV2(Vector2.zero), new SerializableV2(Vector2.zero) };
            msg.messageVector2Array = zeroVectors;
        }

        // Serialize. 
        try
        {
            formatter.Serialize(stream, msg.sideAppDeviceName);
            formatter.Serialize(stream, msg.leaderboardDate);
            formatter.Serialize(stream, msg.currentTeamName);
            formatter.Serialize(stream, msg.currentTeamEmail);
            formatter.Serialize(stream, msg.currentTeamLeaderName);
            formatter.Serialize(stream, msg.currentTeamLeaderGivenName);
            formatter.Serialize(stream, msg.isOkWithTerms);
            formatter.Serialize(stream, msg.isTeamReady);
            formatter.Serialize(stream, msg.nextTeamName);            
            formatter.Serialize(stream, msg.messageVector2Array);
            formatter.Serialize(stream, msg.hasTeamStartedPlaying);
            formatter.Serialize(stream, msg.hasTeamFinishedPlaying);
        }
        catch (SerializationException e)
        {
            Debug.LogError("Failed to serialize. Reason: " + e.Message);
            throw;
        }


        // Now return the array.
        return stream.ToArray();
    }
}