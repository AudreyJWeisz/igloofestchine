﻿using UnityEngine;
using System.Collections;
using System;

public class CanHit : MonoBehaviour {
    private float anglePart;

	// Use this for initialization
	void Start () {
        anglePart = Mathf.PI * 2 / GameParameters.Instance.SubImpactsInRadius;
        GameInput.Instance.OnBallImpact.AddListener(OnBallHit);
    }

    private void OnBallHit(Vector3 position)
    {
        HitTest(position);
    }

    public void HitTest(Vector3 WorldPos)
    {
        Vector3 screenPoint = Camera.main.WorldToScreenPoint(WorldPos);

        for(int i = 0; i < GameParameters.Instance.SubImpactsInRadius; i++)
        {
            float angle = anglePart * i;
            float x = Mathf.Cos(angle);
            float y = Mathf.Sin(angle);
            Vector2 offset = new Vector2(x, y) * GameParameters.Instance.BallImpactRadius;
            Vector3 hitPoint = screenPoint + (Vector3)offset;
            Vector3 worldPos = Camera.main.ScreenToWorldPoint(hitPoint);

            Ray ray = Camera.main.ScreenPointToRay(hitPoint);
            RaycastHit hit;
            Physics.Raycast(ray, out hit);
            if (hit.collider != null)
            {
                print(hit.collider);
                hit.rigidbody.AddForceAtPosition(-hit.normal * GameParameters.Instance.ImpactForce, worldPos);
            }
        }
    }
}
