﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class LeaderboardScreen : MonoBehaviour {

	public GameObject[] leaderBoardField;
	public GameObject audioPrefab;
	public AudioClip resultAudioClip;

	private float pitch=1;

	// Use this for initialization
	void OnEnable () 
	{
		for(int i=0;i<leaderBoardField.Length;i++)
		{
			leaderBoardField [i].SetActive (false); 
		}

		StartCoroutine (Animation());
	}

	// Update is called once per frame
	IEnumerator Animation () {
		float revealTime = 0.25f;

		for(int i=leaderBoardField.Length-1;i>=0;i--)
		{
			leaderBoardField [i].SetActive (true); 
			PlayAudioPrefab ();
			pitch += 0.01f;
			//leaderBoardField [i].transform.DOPunchScale (new Vector3 (1.001f, 1.001f, 1.001f), 0.1f,1);
			yield return new WaitForSeconds(revealTime);
			revealTime -= 0.025f;
		}

	}
		
	public void PlayAudioPrefab()
	{
		GameObject go = Instantiate (audioPrefab, Vector3.zero, Quaternion.identity) as GameObject;
		go.GetComponent<AudioSource> ().PlayOneShot(resultAudioClip);
		go.GetComponent<AudioSource> ().pitch = pitch;
        go.GetComponent<AudioSource>().volume = 0.4f;
    }
}
