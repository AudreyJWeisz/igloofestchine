﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Collections.Specialized;
using System;
using DG.Tweening;

public class GameManager : MonoBehaviour
{

    public static GameManager Instance;

    public bool IsDebugMode = false;
    public bool IsWithoutSideAppMode = false;

    public GameScreen[] Screens;
    public Text CountdownText;
    public Text GameTimerSecondsText;
    public Text CurrentScore;
    public Text CongratsScoreText;
    public Text StartScreenTeamName;
    public Text StartScreenInstructions;
    public Text ScoreText;

    public Image TransitionLayer;

    public SpriteRenderer DrummerL;
    public Animator DrummerLAnimator;
    public SpriteRenderer DrummerR;
    public Animator DrummerRAnimator;

    public int GameDurationSeconds = 0;
    public float TimeBetweenSpecialCharactersInSecs = 5;
    public List<AudioClip> CountdownSFX;
    public AudioClip finalCountDownSFX;
    public AudioClip GameEndSFX;

    public Color CurrentTeamInLeaderboardColor;
    public Color OtherTeamsInLeaderboardColor;

    private int currentGameTimeLeftInSeconds;

    public GameObject MainCanvas;

    public GameObject ScorePopUp355;
    public GameObject AngryDragonPopUp;

    public GameObject StarPrefab;
    public GameObject SmallCanPrefab;
    public GameObject MediumCanPrefab;
    public GameObject LargeCanPrefab;
    public GameObject MegaCanPrefab;
    public bool spawOnceMegaCan = true;


    public GameObject explosionObjectPrefab; 

    public GameObject DragonPrefab;
    public GameObject CanPyramidPrefab;
    public GameObject GeishaPrefab;
    public GameObject ArcherPrefab;

    public GameObject HitWave;

    public GUIStyle FPSStyle;

    [HideInInspector]
    public GameObject startScreenPyramid;

    [HideInInspector]
    public string currentTeamNameInLeaderboard;

    public List<Transform> SpawnLocationsLeft, SpawnLocationsRight;
    public Transform LauncherSpawnLeftLocation;
    public Transform LauncherSpawnRightLocation;

    public List<GameObject> CurrentlyVisibleTargets;

    public List<Vector3> ScorePopUpPositions;

    public GameObject ConnectionErrorPanel;

    public int MaxNumberOfTargetsAtOnce = 10;
    public int FrequencyAtWhichDifficultyLevelIncreasesInSecs = 60;
    private List<float> MegaBeerSpawnTime, LargeBeerSpawnTime, MediumBeerSpawnTime, SmallBeerSpawnTime, GeishaSpawnTime, ArcherSpawnTime, DragonSpawnTime;

    private int numberOfBeersHit = 0;
    private int numberOfBallsRequiredToStart = 1;
    private AudioSource audioSource;

    [HideInInspector]
    public float difficultyLevel = 0;

    private Vector2 currentHitPosition;
    private float BallImpactRadius = 6;

    [HideInInspector]
    public int numberOfDragonsSpawned, numberOfGeishaSpawned, numberOfArcherSpawned, numberOfStarSpawned = 0;

    public bool IsTextInCaps = false;

    public ScreenType currentScreen;

    private float anglePart;
    private bool isShowingLeaderboard = false;
    private float gameStartTime = 0;
    private Vector3 previousMousePosition;
    private float lastMouseMove;
    private bool showFPS;

    private static string SpawnConfigFilePath;

    private const int XStep = 10;
    private const int YStep = 10;

    public delegate void ChangeMusic();
    public event ChangeMusic IntroStart, CountDownStart, GameStart;

    private Color gameTimerInitialColor;

    private bool hasStartedCountdown = false;
    private int gameTimerInitialTextSize;
    private bool hasStartedGrowingTimerText = false;

    public List<AudioClip> ScoringSFX;
    private float deltaTimeSum = 0f;
    private int numberOfRecords = 0;
    private float averagFps;

    private bool VideoTaken = false;


    #region Unitymethods

    void Awake()
    {
        Instance = this;
        //Application.LoadLevelAdditive("RecordVideoScene");
    }


    // Use this for initialization
    void Start()
    {
        SpawnConfigFilePath = Application.dataPath + "/StreamingAssets/SpawnerValues.txt";
        MegaBeerSpawnTime = new List<float>();
        LargeBeerSpawnTime = new List<float>();
        MediumBeerSpawnTime = new List<float>();
        SmallBeerSpawnTime = new List<float>();
        GeishaSpawnTime = new List<float>();
        ArcherSpawnTime = new List<float>();
        DragonSpawnTime = new List<float>();

        GameInput.Instance.OnBallImpact.AddListener(OnBallHitScreen);
        StartCoroutine(StartGame());
        anglePart = Mathf.PI * 2 / GameParameters.Instance.SubImpactsInRadius;

        ScorePopUpPositions = new List<Vector3>();

        if (IsWithoutSideAppMode)
            IsDebugMode = false;

        if (IsDebugMode)
            IsWithoutSideAppMode = false;

        gameTimerInitialColor = GameTimerSecondsText.color;
        gameTimerInitialTextSize = GameTimerSecondsText.fontSize;
    }

    void Update()
    {
        Vector3 mouseDelta = Input.mousePosition - previousMousePosition;

        if (mouseDelta.sqrMagnitude > 0)
            lastMouseMove = Time.time;

        Cursor.visible = Time.time - lastMouseMove <= 2;

        StartScreenTeamName.gameObject.SetActive(currentScreen == ScreenType.START_SCREEN);
        StartScreenInstructions.gameObject.SetActive(currentScreen == ScreenType.START_SCREEN);

        if (Input.GetKeyDown(KeyCode.F))
            showFPS = !showFPS;
        previousMousePosition = Input.mousePosition;

        //fps display
        deltaTimeSum += Time.deltaTime;
        numberOfRecords++;
        if (numberOfRecords == 30)
        {
            averagFps = numberOfRecords / deltaTimeSum;
            deltaTimeSum = 0f;
            numberOfRecords = 0;
        }
    }

    void OnGUI()
    {
        if (showFPS)
        {
            GUI.Box(new Rect(10, 10, 200, 100), Mathf.Ceil(averagFps).ToString(), FPSStyle);
        }
    }

    public IEnumerator StartGame()
    {
        while (SpawnerTextLoader.Instance == null)
            yield return null;

        SpawnerTextLoader.Instance.Load(SpawnConfigFilePath);
        HighScoreUi.Instance.SetHighScoreText();

        if (IntroStart != null) IntroStart(); //Audio event
        DisableAllScreens();
        numberOfBeersHit = 0;
        CurrentScore.text = "0";
        SetGameTimerUI();

        CurrentlyVisibleTargets = new List<GameObject>();

        EnableScreenWithType(ScreenType.GAMEPLAY_SCREEN);

        EnableScreenWithType(ScreenType.START_SCREEN);
        DrummerL.enabled = true;
        DrummerR.enabled = true;
        StartScreenTeamName.text = "";
        BallImpactRadius = GameParameters.Instance.BallImpactRadius;

        while (LocalizationManager.Instance == null)
            yield return null;

        while (string.IsNullOrEmpty(StartScreenTeamName.text) && !IsWithoutSideAppMode)
        {
            if (IsDebugMode && NetworkMessageReceiver.Instance != null)
            {
                foreach (Text teamName in NetworkMessageReceiver.Instance.CurrentTeamNameTexts)
                {
                    LocalizationTextHandler textHandler = teamName.gameObject.AddComponent<LocalizationTextHandler>();
                    textHandler.LocalizationKey = LocalizationKey.DEBUG_TEAM_NAME;
                    teamName.text = LocalizationManager.Instance.GetTextWithKey(LocalizationKey.DEBUG_TEAM_NAME);
                }
                break;
            }
            else if (!IsDebugMode && LocalizationManager.Instance != null)
            {
                if (IsTextInCaps)
                    StartScreenInstructions.text = LocalizationManager.Instance.GetTextWithKey(LocalizationKey.WAITING_FOR_TEAM).ToUpper();
                else
                    StartScreenInstructions.text = LocalizationManager.Instance.GetTextWithKey(LocalizationKey.WAITING_FOR_TEAM);
            }

            yield return null;
        }

        if (IsWithoutSideAppMode)
        {
            RemoveTeamNames();
        }

        if (IsTextInCaps)
            StartScreenInstructions.text = LocalizationManager.Instance.GetTextWithKey(LocalizationKey.GAME_INSTRUCTIONS).ToUpper();
        else
            StartScreenInstructions.text = LocalizationManager.Instance.GetTextWithKey(LocalizationKey.GAME_INSTRUCTIONS);
    }


    public void StartNextGame()
    {
        if (currentScreen == ScreenType.LEADERBOARD_SCREEN)
        {
            StartCoroutine(FadeIn());
            HighScoreUi.Instance.SetHighScoreText();
            SpawnerTextLoader.Instance.Load(SpawnConfigFilePath);
            StopCoroutine(ShowScoreScreen());
            DisableAllScreens();
            numberOfBeersHit = 0;
            numberOfDragonsSpawned = 0;
            numberOfGeishaSpawned = 0;
            CurrentScore.text = "0";
            SetGameTimerUI();
            CurrentlyVisibleTargets = new List<GameObject>();

            EnableScreenWithType(ScreenType.GAMEPLAY_SCREEN);
            EnableScreenWithType(ScreenType.START_SCREEN);
            DrummerL.enabled = true;
            DrummerR.enabled = true;
        }
    }

    private IEnumerator FadeIn()
    {
        while (TransitionLayer.color.a < 200f / 255f)
        {
            Color newCol = TransitionLayer.color;
            newCol.a += 0.02f;
            TransitionLayer.color = newCol;
            yield return null;
        }
        Color finalCol = TransitionLayer.color;
        finalCol.a = 200f / 255f;
        TransitionLayer.color = finalCol;
        foreach (GameObject blinkingSign in GameObject.FindGameObjectsWithTag("Sign"))
        {
            blinkingSign.GetComponent<SpriteRenderer>().sortingOrder = 0;
        }
    }

    private void SetGameTimerUI()
    {
        int seconds = GameDurationSeconds;

        string secondsString = seconds.ToString();
        if (seconds < 10)
        {
            secondsString = "0" + seconds.ToString();
        }
        GameTimerSecondsText.text = secondsString;
    }

    public void InitializeGeishaTimers(string[] timeValues)
    {
        GeishaSpawnTime.Clear();
        for (int i = 0; i < timeValues.Length; i++)
        {
            GeishaSpawnTime.Add(float.Parse(timeValues[i]));
        }
    }

    public void InitializeArcherTimers(string[] timeValues)
    {
        ArcherSpawnTime.Clear();
        for (int i = 0; i < timeValues.Length; i++)
        {
            ArcherSpawnTime.Add(float.Parse(timeValues[i]));
        }
    }

    public void InitializeDragonTimers(string[] timeValues)
    {
        DragonSpawnTime.Clear();
        for (int i = 0; i < timeValues.Length; i++)
        {
            DragonSpawnTime.Add(float.Parse(timeValues[i]));
        }
    }

    public void InitializeSmallBeerTimers(string[] timeValues)
    {
        SmallBeerSpawnTime.Clear();
        for (int i = 0; i < timeValues.Length; i++)
        {
            SmallBeerSpawnTime.Add(float.Parse(timeValues[i]));
        }
    }

    public void InitializeMediumBeerTimers(string[] timeValues)
    {
        MediumBeerSpawnTime.Clear();
        for (int i = 0; i < timeValues.Length; i++)
        {
            MediumBeerSpawnTime.Add(float.Parse(timeValues[i]));
        }
    }

    public void InitializeLargeBeerTimers(string[] timeValues)
    {
        LargeBeerSpawnTime.Clear();
        for (int i = 0; i < timeValues.Length; i++)
        {
            LargeBeerSpawnTime.Add(float.Parse(timeValues[i]));
        }
    }

    public void InitializeMegaBeerTimers(string[] timeValues)
    {
        MegaBeerSpawnTime.Clear();
        for (int i = 0; i < timeValues.Length; i++)
        {
            MegaBeerSpawnTime.Add(float.Parse(timeValues[i]));
        }
    }

    #endregion

    #region Spawners

    // triggers specific spawners based on the timer and current score
    private IEnumerator ManageSpawningOfObjects()
    {
        int pickLeftOrRight, randomIndex;
        while (currentScreen == ScreenType.GAMEPLAY_SCREEN && spawOnceMegaCan)
        {
            if (LargeBeerSpawnTime.Count != 0 && gameStartTime + LargeBeerSpawnTime[0] <= Time.timeSinceLevelLoad)
            {
                pickLeftOrRight = Mathf.RoundToInt(UnityEngine.Random.value);
                randomIndex = UnityEngine.Random.Range(0, SpawnLocationsLeft.Count);
                Vector3 spawnPosition = pickLeftOrRight == 0 ? SpawnLocationsLeft[randomIndex].position : SpawnLocationsRight[randomIndex].position;
                SpawnLargeBeerCan(spawnPosition, pickLeftOrRight == 0);
                LargeBeerSpawnTime.Remove(LargeBeerSpawnTime[0]);

            }
            else if (MediumBeerSpawnTime.Count != 0 && gameStartTime + MediumBeerSpawnTime[0] <= Time.timeSinceLevelLoad)
            {
                pickLeftOrRight = Mathf.RoundToInt(UnityEngine.Random.value);
                randomIndex = UnityEngine.Random.Range(0, SpawnLocationsLeft.Count);
                Vector3 spawnPosition = pickLeftOrRight == 0 ? SpawnLocationsLeft[randomIndex].position : SpawnLocationsRight[randomIndex].position;
                SpawnMediumBeerCan(spawnPosition, pickLeftOrRight == 0);
                MediumBeerSpawnTime.Remove(MediumBeerSpawnTime[0]);
            }
            else if (SmallBeerSpawnTime.Count != 0 && gameStartTime + SmallBeerSpawnTime[0] <= Time.timeSinceLevelLoad)
            {
                pickLeftOrRight = Mathf.RoundToInt(UnityEngine.Random.value);
                randomIndex = UnityEngine.Random.Range(0, SpawnLocationsLeft.Count);
                Vector3 spawnPosition = pickLeftOrRight == 0 ? SpawnLocationsLeft[randomIndex].position : SpawnLocationsRight[randomIndex].position;
                SpawnSmallBeerCan(spawnPosition, pickLeftOrRight == 0);
                SmallBeerSpawnTime.Remove(SmallBeerSpawnTime[0]);
            }
            else if (ArcherSpawnTime.Count != 0 && gameStartTime + ArcherSpawnTime[0] <= Time.timeSinceLevelLoad)
            {
                SpawnArcher();
                ArcherSpawnTime.Remove(ArcherSpawnTime[0]);
            }
            else if (GeishaSpawnTime.Count != 0 && gameStartTime + GeishaSpawnTime[0] <= Time.timeSinceLevelLoad)
            {
                SpawnGeisha();
                GeishaSpawnTime.Remove(GeishaSpawnTime[0]);

            }
            else if (DragonSpawnTime.Count != 0 && gameStartTime + DragonSpawnTime[0] <= Time.timeSinceLevelLoad)
            {
                if (!CheckThereArePyramidsLeft())
                {
                    pickLeftOrRight = Mathf.RoundToInt(UnityEngine.Random.value);
                    SpawnDragon(pickLeftOrRight == 0);
                }
                DragonSpawnTime.Remove(DragonSpawnTime[0]);

            }
            else if (Time.timeSinceLevelLoad % 1 == 0 && CurrentlyVisibleTargets.Count < 5)
            {
                if (numberOfStarSpawned == 0)
                {
                    if (gameStartTime + (GeishaSpawnTime.Count > 0 ? GeishaSpawnTime[0] : 0) < gameStartTime + (ArcherSpawnTime.Count > 0 ? ArcherSpawnTime[0] : 0))
                    {
                        SpawnGeisha();
                        if (GeishaSpawnTime.Count > 0)
                            GeishaSpawnTime.Remove(GeishaSpawnTime[0]);
                    }
                    else
                    {
                        SpawnArcher();
                        if (ArcherSpawnTime.Count > 0)
                            ArcherSpawnTime.Remove(ArcherSpawnTime[0]);
                    }
                }
                else if (numberOfDragonsSpawned == 0)
                {
                    if (!CheckThereArePyramidsLeft())
                    {
                        pickLeftOrRight = Mathf.RoundToInt(UnityEngine.Random.value);
                        SpawnDragon(pickLeftOrRight == 0);
                    }
                    if (DragonSpawnTime.Count > 0)
                        DragonSpawnTime.Remove(DragonSpawnTime[0]);
                }
                else
                {
                    pickLeftOrRight = Mathf.RoundToInt(UnityEngine.Random.value);
                    randomIndex = UnityEngine.Random.Range(0, SpawnLocationsLeft.Count);
                    Vector3 spawnPosition = pickLeftOrRight == 0 ? SpawnLocationsLeft[randomIndex].position : SpawnLocationsRight[randomIndex].position;

                    if (UnityEngine.Random.Range(0, 3) == 0)
                    {
                        SpawnSmallBeerCan(spawnPosition, pickLeftOrRight == 0);
                        if (SmallBeerSpawnTime.Count > 0)
                            SmallBeerSpawnTime.Remove(SmallBeerSpawnTime[0]);
                    }
                    else if (UnityEngine.Random.Range(0, 2) == 0)
                    {
                        SpawnMediumBeerCan(spawnPosition, pickLeftOrRight == 0);
                        if (MediumBeerSpawnTime.Count > 0)
                            MediumBeerSpawnTime.Remove(MediumBeerSpawnTime[0]);
                    }
                    else
                    {
                        SpawnLargeBeerCan(spawnPosition, pickLeftOrRight == 0);
                        if (LargeBeerSpawnTime.Count > 0)
                            LargeBeerSpawnTime.Remove(LargeBeerSpawnTime[0]);
                    }

                }

            }

            yield return new WaitForEndOfFrame();
        }
    }

    private bool CheckThereArePyramidsLeft()
    {
        int numberOfBeersInPyramid = 0;

        foreach (GameObject target in CurrentlyVisibleTargets)
        {
            if (target.name.Contains("Small"))
            {
                if (target.transform.parent.name.Contains("Pyramid"))
                {
                    numberOfBeersInPyramid = target.transform.parent.GetComponentsInChildren<MeshRenderer>().Length;

                    if (numberOfBeersInPyramid > 1)
                        return true;
                }
            }
        }
        return false;
    }


    GameObject SpawnDragon(bool isGoingToTheRight)
    {
        Vector3 spawnPosition = isGoingToTheRight ? new Vector3(LauncherSpawnLeftLocation.position.x, LauncherSpawnLeftLocation.position.y, 0) : new Vector3(LauncherSpawnRightLocation.position.x, LauncherSpawnRightLocation.position.y, 0);
        GameObject dragon = Instantiate(DragonPrefab, spawnPosition, DragonPrefab.transform.rotation) as GameObject;
        dragon.transform.SetParent(transform);

        if (isGoingToTheRight)
        {
            dragon.transform.eulerAngles = new Vector3(0, 180, 0) + dragon.transform.eulerAngles;
            dragon.GetComponentInChildren<DragonPathManager>().IsGoingToTheRight = true;
        }
        else
            dragon.GetComponentInChildren<DragonPathManager>().IsGoingToTheRight = false;

        numberOfDragonsSpawned++;
        CurrentlyVisibleTargets.Add(dragon);
        return dragon;
    }


    GameObject SpawnSmallBeerCan(Vector3 spawnPosition, bool isGoingToTheRight)
    {
        GameObject spawnedTarget = null;
        spawnedTarget = Instantiate(SmallCanPrefab, spawnPosition, SmallCanPrefab.transform.rotation) as GameObject;

        if (isGoingToTheRight)
            spawnedTarget.GetComponent<BeerCanMotion>().IsGoingToTheRight = true;
        else
            spawnedTarget.GetComponent<BeerCanMotion>().IsGoingToTheRight = false;

        spawnedTarget.transform.SetParent(transform);
        CurrentlyVisibleTargets.Add(spawnedTarget);

        return spawnedTarget;
    }

    GameObject SpawnMediumBeerCan(Vector3 spawnPosition, bool isGoingToTheRight)
    {
        GameObject spawnedTarget = null;

        spawnedTarget = Instantiate(MediumCanPrefab, spawnPosition, MediumCanPrefab.transform.rotation) as GameObject;

        if (isGoingToTheRight)
            spawnedTarget.GetComponent<BeerCanMotion>().IsGoingToTheRight = true;
        else
            spawnedTarget.GetComponent<BeerCanMotion>().IsGoingToTheRight = false;

        spawnedTarget.transform.SetParent(transform);
        CurrentlyVisibleTargets.Add(spawnedTarget);

        return spawnedTarget;
    }

    GameObject SpawnLargeBeerCan(Vector3 spawnPosition, bool isGoingToTheRight)
    {
        GameObject spawnedTarget = null;

        spawnedTarget = Instantiate(LargeCanPrefab, spawnPosition, LargeCanPrefab.transform.rotation) as GameObject;

        if (isGoingToTheRight)
            spawnedTarget.GetComponent<LargeBeerCanMotion>().IsGoingToTheRight = true;
        else
            spawnedTarget.GetComponent<LargeBeerCanMotion>().IsGoingToTheRight = false;

        spawnedTarget.transform.SetParent(transform);
        CurrentlyVisibleTargets.Add(spawnedTarget);

        return spawnedTarget;
    }

    GameObject SpawnMegaBeerCan(Vector3 spawnPosition, bool isGoingToTheRight)
    {
        GameObject spawnedTarget = null;

        spawnedTarget = Instantiate(MegaCanPrefab, spawnPosition, MegaCanPrefab.transform.rotation) as GameObject;

        spawnedTarget.transform.SetParent(transform);
        CurrentlyVisibleTargets.Add(spawnedTarget);

        return spawnedTarget;
    }


    GameObject SpawnGeisha()
    {
        int randomSpawnLocation = UnityEngine.Random.Range(0, 2);
        Vector3 spawnPosition = randomSpawnLocation == 0 ? LauncherSpawnLeftLocation.position : LauncherSpawnRightLocation.position;

        GameObject geisha = Instantiate(GeishaPrefab, spawnPosition, GeishaPrefab.transform.rotation) as GameObject;

        if (randomSpawnLocation == 0)
            geisha.transform.eulerAngles = new Vector3(0, 180, 0);
        else
            geisha.GetComponent<GeishaController>().IsPlacedOnRightSide = true;

        geisha.transform.SetParent(transform);
        numberOfGeishaSpawned++;
        return geisha;
    }

    GameObject SpawnArcher()
    {
        int randomSpawnLocation = UnityEngine.Random.Range(0, 2);
        Vector3 spawnPosition = randomSpawnLocation == 0 ? LauncherSpawnLeftLocation.position : LauncherSpawnRightLocation.position;
        GameObject archer = Instantiate(ArcherPrefab, spawnPosition, ArcherPrefab.transform.rotation) as GameObject;
        if (randomSpawnLocation == 1)
        {
            archer.transform.eulerAngles = new Vector3(0, 180, 0);
            archer.GetComponent<ArcherController>().IsPlacedOnRightSide = true;
        }

        archer.transform.SetParent(transform);
        numberOfArcherSpawned++;
        return archer;
    }

    #endregion

    #region UImethods
    private void DisableAllScreens()
    {
        if (currentScreen != ScreenType.START_SCREEN && currentScreen != ScreenType.COUNTDOWN_SCREEN)
        {
            DestroyCans();
            if (currentScreen == ScreenType.GAMEPLAY_SCREEN)
            {
                GameObject.Find("FBackground").GetComponent<RainCameraController>().Stop();
            }
        }

        foreach (GameScreen screen in Screens)
        {
            screen.ScreenRootObject.SetActive(false);
        }
    }


    private void EnableScreenWithType(ScreenType type)
    {
        foreach (GameScreen screen in Screens)
        {
            if (screen.ScreenType == type)
            {
                screen.ScreenRootObject.SetActive(true);
                if (type == ScreenType.LEADERBOARD_SCREEN && !isShowingLeaderboard)
                    GetScreenWithType(ScreenType.FOREGROUND_CITY).SetActive(true);
                currentScreen = type;
                break;
            }
        }

        if (type == ScreenType.START_SCREEN)
        {
            startScreenPyramid = Instantiate(CanPyramidPrefab) as GameObject;
            startScreenPyramid.transform.SetParent(gameObject.transform);
            startScreenPyramid.transform.position = new Vector3(37, -113, 31);
            foreach (Rigidbody can in startScreenPyramid.GetComponentsInChildren<Rigidbody>())
            {
                CurrentlyVisibleTargets.Add(can.gameObject);
            }
        }
    }


    public GameObject GetScreenWithType(ScreenType type)
    {
        foreach (GameScreen screen in Screens)
        {
            if (screen.ScreenType == type)
            {
                return screen.ScreenRootObject;
            }
        }
        return null;
    }

    private IEnumerator ShowCountdown()
    {
        audioSource = GetComponent<AudioSource>();
        MessageData teamStartedMsg = new MessageData();
        teamStartedMsg.currentTeamEmail = "";
        teamStartedMsg.currentTeamName = "";
        teamStartedMsg.hasTeamStartedPlaying = true;
        teamStartedMsg.hasTeamFinishedPlaying = false;
        teamStartedMsg.isTeamReady = false;
        teamStartedMsg.nextTeamName = "";
        teamStartedMsg.sideAppDeviceName = "";
        Client.singleton.Send(teamStartedMsg);
        yield return new WaitForSeconds(0.7f);
        DisableAllScreens();
        EnableScreenWithType(ScreenType.GAMEPLAY_SCREEN);
        EnableScreenWithType(ScreenType.COUNTDOWN_SCREEN);
        yield return new WaitForSeconds(0.2f);
        CountdownText.text = "3";
        audioSource.PlayOneShot(CountdownSFX[0]);
        yield return new WaitForSeconds(1);
        CountdownText.text = "2";
        audioSource.PlayOneShot(CountdownSFX[1]);
        yield return new WaitForSeconds(1);
        CountdownText.text = "1";
        audioSource.PlayOneShot(CountdownSFX[2]);
        DrummerLAnimator.SetBool("Vanish", true);
        DrummerRAnimator.SetBool("Vanish", true);
        yield return new WaitForSeconds(1);
        audioSource.PlayOneShot(CountdownSFX[3]);
        StartCoroutine(ShowGameScreen());
    }

    private IEnumerator ShowGameScreen()
    {
        hasStartedCountdown = false;
        //VideoTaken = false;
        DisableAllScreens();

        GameObject target = null;
        for (int i = CurrentlyVisibleTargets.Count - 1; i >= 0; i--)
        {
            target = CurrentlyVisibleTargets[i];
            if (target != null)
            {
                CurrentlyVisibleTargets.RemoveAt(i);
                Destroy(target);
                target = null;
            }
            else
                CurrentlyVisibleTargets.RemoveAt(i);
        }

        // Remove remaining cans from countdown screen
        foreach (Transform beer in GetComponentsInChildren<Transform>())
        {
            if (beer.name.Contains("Pyramid") || beer.name.Contains("Small"))
            {
                Destroy(beer.gameObject);
            }
        }

        if (!IsWithoutSideAppMode)
            EnableScreenWithType(ScreenType.NEXT_TEAM_FOREGROUND);
        foreach (Text nextTeamText in GetScreenWithType(ScreenType.NEXT_TEAM_FOREGROUND).GetComponentsInChildren<Text>())
        {
            nextTeamText.color = new Color(nextTeamText.color.r, nextTeamText.color.g, nextTeamText.color.b, 190.0f / 255.0f);
        }
        EnableScreenWithType(ScreenType.GAMEPLAY_SCREEN);
        DrummerLAnimator.SetBool("Vanish", false);
        DrummerRAnimator.SetBool("Vanish", false);
        DrummerL.enabled = false;
        DrummerR.enabled = false;

        while (TransitionLayer.color.a > 0)
        {
            Color newCol = TransitionLayer.color;
            newCol.a -= 0.02f;
            TransitionLayer.color = newCol;
            yield return null;
        }

        foreach (GameObject blinkingSign in GameObject.FindGameObjectsWithTag("Sign"))
        {
            blinkingSign.GetComponent<SpriteRenderer>().sortingOrder = 1;
        }

        if (GameStart != null) GameStart(); // event for audio

        currentGameTimeLeftInSeconds = GameDurationSeconds;
        gameStartTime = Time.timeSinceLevelLoad;
        float previousTime = Time.timeSinceLevelLoad;

        if (spawOnceMegaCan)
            StartCoroutine(ManageSpawningOfObjects());

        while (currentGameTimeLeftInSeconds > 0)
        {
            if (Time.timeSinceLevelLoad - previousTime >= 1.0f)
            {
                previousTime = Time.timeSinceLevelLoad;
                currentGameTimeLeftInSeconds--;
                GameTimerSecondsText.text = currentGameTimeLeftInSeconds.ToString();
            }

            if (currentGameTimeLeftInSeconds <= 20)
            {
                if (currentGameTimeLeftInSeconds <= 10)
                {
                    GameTimerSecondsText.color = Color.red;
                    if (!hasStartedGrowingTimerText)
                    {
                        hasStartedGrowingTimerText = true;
                        StartCoroutine(GrowTimerText());
                        StartCoroutine(FinalCountdown());
                    }
                }
                else
                    GameTimerSecondsText.color = new Color(0.8f, 0.4f, 0f);

                FeverMode();

                // trigger video shoot 15 befor end of the game 
                /*if (!VideoTaken)
				{
					VideoTaken = true;
					CameraRecordSystem.Instance.RecordPlay(NetworkMessageReceiver.Instance.GetCurrentTeamInfos());
				}*/
            }
            yield return null;
        }

        spawOnceMegaCan = true;
        GameTimerSecondsText.text = "00";
        GameTimerSecondsText.color = gameTimerInitialColor;
        foreach (GameObject blinkingSign in GameObject.FindGameObjectsWithTag("Sign"))
        {
            blinkingSign.GetComponent<SpriteRenderer>().sortingOrder = 0;
        }
        DisableAllScreens();

        isShowingLeaderboard = true;
        StartCoroutine(ShowScoreScreen());
    }

    private IEnumerator FinalCountdown()
    {
        if (audioSource == null)
            audioSource = GetComponent<AudioSource>();

        audioSource.clip = finalCountDownSFX;
        audioSource.Play();

        GameTimerSecondsText.gameObject.GetComponent<Outline>().enabled = true;

        yield return new WaitForSeconds(10);


        GameTimerSecondsText.gameObject.GetComponent<Outline>().enabled = false;

        yield return null;
    }


    private IEnumerator GrowTimerText()
    {
        //DIRTY ANIMATION
        GameTimerSecondsText.transform.DOPunchScale(new Vector3(1.3f, 1.3f, 1.3f), 0.2f);
        yield return new WaitForSeconds(1f);
        GameTimerSecondsText.transform.DOPunchScale(new Vector3(1.3f, 1.3f, 1.3f), 0.2f);
        yield return new WaitForSeconds(1f);
        GameTimerSecondsText.transform.DOPunchScale(new Vector3(1.3f, 1.3f, 1.3f), 0.2f);
        yield return new WaitForSeconds(1f);
        GameTimerSecondsText.transform.DOPunchScale(new Vector3(1.3f, 1.3f, 1.3f), 0.2f);
        yield return new WaitForSeconds(1f);
        GameTimerSecondsText.transform.DOPunchScale(new Vector3(1.3f, 1.3f, 1.3f), 0.2f);
        yield return new WaitForSeconds(1f);
        GameTimerSecondsText.transform.DOPunchScale(new Vector3(1.3f, 1.3f, 1.3f), 0.2f);
        yield return new WaitForSeconds(0.5f);
        GameTimerSecondsText.transform.DOPunchScale(new Vector3(1.3f, 1.3f, 1.3f), 0.2f);
        yield return new WaitForSeconds(0.5f);
        GameTimerSecondsText.transform.DOPunchScale(new Vector3(1.3f, 1.3f, 1.3f), 0.2f);
        yield return new WaitForSeconds(0.5f);
        GameTimerSecondsText.transform.DOPunchScale(new Vector3(1.3f, 1.3f, 1.3f), 0.2f);
        yield return new WaitForSeconds(0.5f);
        GameTimerSecondsText.transform.DOPunchScale(new Vector3(1.3f, 1.3f, 1.3f), 0.2f);
        yield return new WaitForSeconds(0.5f);
        GameTimerSecondsText.transform.DOPunchScale(new Vector3(1.3f, 1.3f, 1.3f), 0.2f);
        yield return new WaitForSeconds(0.5f);
        GameTimerSecondsText.transform.DOPunchScale(new Vector3(1.3f, 1.3f, 1.3f), 0.1f);
        yield return new WaitForSeconds(0.2f);
        GameTimerSecondsText.transform.DOPunchScale(new Vector3(1.3f, 1.3f, 1.3f), 0.1f);
        yield return new WaitForSeconds(0.2f);
        GameTimerSecondsText.transform.DOPunchScale(new Vector3(1.3f, 1.3f, 1.3f), 0.1f);
        yield return new WaitForSeconds(0.2f);
        GameTimerSecondsText.transform.DOPunchScale(new Vector3(1.3f, 1.3f, 1.3f), 0.1f);
        yield return new WaitForSeconds(0.2f);
        GameTimerSecondsText.transform.DOPunchScale(new Vector3(1.3f, 1.3f, 1.3f), 0.1f);
        yield return new WaitForSeconds(0.2f);
        GameTimerSecondsText.transform.DOPunchScale(new Vector3(1.3f, 1.3f, 1.3f), 0.1f);
        yield return new WaitForSeconds(0.2f);
        GameTimerSecondsText.transform.DOPunchScale(new Vector3(1.3f, 1.3f, 1.3f), 0.1f);
        yield return new WaitForSeconds(0.2f);
        GameTimerSecondsText.transform.DOPunchScale(new Vector3(1.3f, 1.3f, 1.3f), 0.1f);
        yield return new WaitForSeconds(0.2f);
    }

    private void ReturnTimerTextToInitialSize()
    {
        GameTimerSecondsText.fontSize = gameTimerInitialTextSize;
        hasStartedGrowingTimerText = false;
    }


    private IEnumerator ShowScoreScreen()
    {
        ReturnTimerTextToInitialSize();

        // ------ CAMERA RECORD System and email automatic sending 
        //CameraRecordSystem.Instance.RecordAndSendFramedVideo(CurrentScore.text, NetworkMessageReceiver.Instance.GetCurrentTeamInfos());

        if (!IsDebugMode && !IsWithoutSideAppMode)
            NetworkMessageReceiver.Instance.WriteTeamScoreToFile(CurrentScore.text);

        DisableAllScreens();
        EnableScreenWithType(ScreenType.GAMEPLAY_SCREEN);
        EnableScreenWithType(ScreenType.SCORE_SCREEN);

        Camera.main.GetComponent<AudioSource>().PlayOneShot(GameEndSFX);

        ScoreText.text = CurrentScore.text;

        yield return new WaitForSeconds(5);

        DisableAllScreens();

        EnableScreenWithType(ScreenType.GAMEPLAY_SCREEN);
        if (!IsWithoutSideAppMode)
            EnableScreenWithType(ScreenType.NEXT_TEAM_FOREGROUND);

        foreach (Text nextTeamText in GetScreenWithType(ScreenType.NEXT_TEAM_FOREGROUND).GetComponentsInChildren<Text>())
        {
            nextTeamText.color = new Color(nextTeamText.color.r, nextTeamText.color.g, nextTeamText.color.b, 1.0f);
        }

        EnableScreenWithType(ScreenType.FOREGROUND_CITY);
        EnableScreenWithType(ScreenType.LEADERBOARD_SCREEN);

        currentTeamNameInLeaderboard = NetworkMessageReceiver.Instance.CurrentTeamNameTexts[2].text;
        currentTeamNameInLeaderboard = currentTeamNameInLeaderboard.Replace("-_-", "_"); // this string is a delimiter that was added between team name and timestamp in order to get unique team names, therefore it shouldn't appear in the team name.
        LeaderboardManager.Instance.AddScoreToLeaderboard(currentTeamNameInLeaderboard, float.Parse(CurrentScore.text));

        Text lastTeamName = null;
        Text lastTeamScore = null;
        Text lastTeamPosition = null;
        Text lastTeamPoints = null;

        int teamPosition = 1;
        bool isTeamInTopTen = false;
        bool isThisElementCurrentTeamPosition = false;

        foreach (Transform leaderboardElement in GetScreenWithType(ScreenType.LEADERBOARD_SCREEN).transform)
        {
            if (leaderboardElement.gameObject.name.Contains("Team"))
            {
                foreach (Transform leaderboardChildElement in leaderboardElement.Cast<Transform>().OrderBy(t => t.name))
                {
                    DictionaryEntry currentElement = new DictionaryEntry();

                    if (leaderboardChildElement.gameObject.name.Contains("NameOfTeam"))
                    {
                        currentElement = LeaderboardManager.Instance.GetLeaderboardElementAtPosition(teamPosition);
                        leaderboardChildElement.GetComponent<Text>().text = (string)currentElement.Key;

                        if (LeaderboardManager.Instance.GetPositionForTeam(currentTeamNameInLeaderboard) == teamPosition)
                        {
                            isTeamInTopTen = true;
                            isThisElementCurrentTeamPosition = true;
                            leaderboardChildElement.gameObject.GetComponent<Text>().color = CurrentTeamInLeaderboardColor;
                        }
                        else
                            leaderboardChildElement.gameObject.GetComponent<Text>().color = OtherTeamsInLeaderboardColor;

                        if (teamPosition == 10)
                            lastTeamName = leaderboardChildElement.GetComponent<Text>();
                    }
                    else if (leaderboardChildElement.gameObject.name.Contains("Score"))
                    {
                        if (string.IsNullOrEmpty((string)currentElement.Key))
                            currentElement = LeaderboardManager.Instance.GetLeaderboardElementAtPosition(teamPosition);
                        leaderboardChildElement.GetComponent<Text>().text = ((float)currentElement.Value).ToString();
                        if (teamPosition == 10)
                            lastTeamScore = leaderboardChildElement.GetComponent<Text>();
                        if (isThisElementCurrentTeamPosition)
                            leaderboardChildElement.gameObject.GetComponent<Text>().color = CurrentTeamInLeaderboardColor;
                        else
                            leaderboardChildElement.gameObject.GetComponent<Text>().color = OtherTeamsInLeaderboardColor;
                    }

                    else if (leaderboardChildElement.gameObject.name.Contains("Position"))
                    {
                        if (teamPosition == 10)
                            lastTeamPosition = leaderboardChildElement.GetComponent<Text>();
                        if (isThisElementCurrentTeamPosition)
                            leaderboardChildElement.gameObject.GetComponent<Text>().color = CurrentTeamInLeaderboardColor;
                        else
                            leaderboardChildElement.gameObject.GetComponent<Text>().color = OtherTeamsInLeaderboardColor;
                    }
                    else if (leaderboardChildElement.gameObject.name.Contains("Text"))
                    {
                        if (teamPosition == 10)
                            lastTeamPoints = leaderboardChildElement.GetComponent<Text>();
                        if (isThisElementCurrentTeamPosition)
                        {
                            isThisElementCurrentTeamPosition = false;
                            leaderboardChildElement.gameObject.GetComponent<Text>().color = CurrentTeamInLeaderboardColor;
                        }
                        else
                            leaderboardChildElement.gameObject.GetComponent<Text>().color = OtherTeamsInLeaderboardColor;

                        teamPosition++;
                    }
                }
            }
        }

        lastTeamPosition.color = OtherTeamsInLeaderboardColor;

        if (isTeamInTopTen)
        {
            lastTeamPosition.text = "10";
        }
        else
        {
            int position = LeaderboardManager.Instance.GetPositionForTeam(currentTeamNameInLeaderboard);
            lastTeamName.text = currentTeamNameInLeaderboard;
            lastTeamName.color = CurrentTeamInLeaderboardColor;
            lastTeamScore.text = CurrentScore.text;
            lastTeamScore.color = CurrentTeamInLeaderboardColor;
            lastTeamPosition.text = position.ToString();
            lastTeamPosition.color = CurrentTeamInLeaderboardColor;
            lastTeamPoints.color = CurrentTeamInLeaderboardColor;
        }

        yield return new WaitForSeconds(15f);

        //TODO REFACTOR THIS IS UGLY 
        // this send to the app that the curren team finished to play 
        MessageData teamFinishedMsg = new MessageData();
        teamFinishedMsg.currentTeamEmail = "";
        teamFinishedMsg.currentTeamName = "";
        teamFinishedMsg.hasTeamStartedPlaying = false;
        teamFinishedMsg.hasTeamFinishedPlaying = true;
        teamFinishedMsg.isTeamReady = false;
        teamFinishedMsg.nextTeamName = "";
        teamFinishedMsg.sideAppDeviceName = "";
        Client.singleton.Send(teamFinishedMsg);

        if (IsWithoutSideAppMode)
            LeaderboardManager.Instance.RemoveTeamFromLeaderboard(LeaderboardManager.Instance.GetPositionForTeam(currentTeamNameInLeaderboard));

        if (IsDebugMode || IsWithoutSideAppMode)
        {
            yield return new WaitForSeconds(30);
            StartNextGame();
        }
        isShowingLeaderboard = false;
    }


    public void RemoveTeamNames()
    {
        foreach (Text teamName in NetworkMessageReceiver.Instance.CurrentTeamNameTexts)
        {
            if (teamName.gameObject.transform.parent.name.Contains("Score"))
            {
                teamName.text = LocalizationManager.Instance.GetTextWithKey(LocalizationKey.NO_SIDE_APP_TEAM_NAME);

                if (teamName.gameObject.GetComponent<LocalizationTextHandler>() == null)
                    teamName.gameObject.AddComponent<LocalizationTextHandler>();

                LocalizationTextHandler textHandler = teamName.gameObject.GetComponent<LocalizationTextHandler>();
                textHandler.LocalizationKey = LocalizationKey.NO_SIDE_APP_TEAM_NAME;
            }
            else
            {
                teamName.text = "";
                if (teamName.gameObject.GetComponent<LocalizationTextHandler>() != null)
                    Destroy(teamName.gameObject.GetComponent<LocalizationTextHandler>());
            }
        }

        for (int i = 0; i < NetworkMessageReceiver.Instance.NextTeamNameTexts.Length; i++)
        {
            NetworkMessageReceiver.Instance.NextTeamNameTexts[i].text = "";
            if (NetworkMessageReceiver.Instance.NextTeamNameTexts[i].gameObject.GetComponent<LocalizationTextHandler>() != null)
                Destroy(NetworkMessageReceiver.Instance.NextTeamNameTexts[i].gameObject.GetComponent<LocalizationTextHandler>());
        }
    }

    private string ConvertScoreToLiters(int score)
    {
        float result = (float)score / 1000.0f;
        return result.ToString();
    }


    private void OnBallHitScreen(Vector3 hitPos)
    {
        float multiplier = GetComponent<GameInput>().SideMultiplier;
        hitPos.x *= multiplier;

        currentHitPosition = hitPos;

        if (currentScreen == ScreenType.START_SCREEN && (!string.IsNullOrEmpty(StartScreenTeamName.text) || IsWithoutSideAppMode))
        {
            StartCoroutine(ShowHitWave(hitPos));
            if (CheckHasHitMultipleTargets())
                numberOfBeersHit++;
        }

        if (currentScreen == ScreenType.START_SCREEN && numberOfBeersHit >= numberOfBallsRequiredToStart && !hasStartedCountdown)
        {
            hasStartedCountdown = true;
            StartCoroutine(ShowCountdown());
        }

        if (currentScreen == ScreenType.GAMEPLAY_SCREEN || currentScreen == ScreenType.COUNTDOWN_SCREEN)
        {
            StartCoroutine(ShowHitWave(hitPos));
            CheckHasHitMultipleTargets();
        }
    }



    private bool CheckHasHitMultipleTargets()
    {
        bool hasHit = false;

        Vector3 screenPos = Camera.main.WorldToScreenPoint(currentHitPosition);

        List<GameObject> objectsHit = new List<GameObject>();

        RaycastHit[] hits;

        // Loop through all rays being created from the circle's circumference
        for (int i = 0; i < GameParameters.Instance.SubImpactsInRadius; i++)
        {
            float angle = anglePart * i;
            float x = Mathf.Cos(angle);
            float y = Mathf.Sin(angle);
            Vector2 offset = new Vector2(x, y) * GameParameters.Instance.BallImpactRadius;
            if (currentScreen != ScreenType.GAMEPLAY_SCREEN)
                offset = new Vector2(x, y) * (GameParameters.Instance.BallImpactRadius / 3);
            Vector3 hitPoint = screenPos + (Vector3)offset;
            Vector3 worldPos = Camera.main.ScreenToWorldPoint(hitPoint);

            Ray ray = Camera.main.ScreenPointToRay(hitPoint);
            hits = Physics.RaycastAll(ray);

            // Loop through all objects that have been hit by this ray
            foreach (RaycastHit hit in hits)
            {
                if (hit.collider.gameObject.name.Contains("Can") && !hit.collider.gameObject.name.Contains("Pyramid") && !objectsHit.Contains(hit.collider.gameObject))
                {
                    if (!hit.collider.gameObject.name.Contains("Mega"))
                        objectsHit.Add(hit.collider.gameObject);

                    if (hit.collider.gameObject.name.Contains("Big"))
                    {
                        CreatePopUpScoreUponBallHit(hit.point, ScorePopUp355, 650);
                        CurrentlyVisibleTargets.Remove(hit.collider.gameObject);
                        hit.collider.gameObject.GetComponent<LargeBeerCanMotion>().CanIsHit(hit, worldPos);
                        hasHit = true;
                    }
                    else if (hit.collider.gameObject.name.Contains("Medium"))
                    {
                        CreatePopUpScoreUponBallHit(hit.point, ScorePopUp355, 500);
                        CurrentlyVisibleTargets.Remove(hit.collider.gameObject);
                        hit.collider.gameObject.GetComponent<BeerCanMotion>().CanIsHit(hit, worldPos);
                        hasHit = true;
                    }
                    else if (hit.collider.gameObject.name.Contains("Small"))
                    {
                        CreatePopUpScoreUponBallHit(hit.point, ScorePopUp355, 355);
                        CurrentlyVisibleTargets.Remove(hit.collider.gameObject);
                        if (currentScreen == ScreenType.GAMEPLAY_SCREEN || currentScreen == ScreenType.COUNTDOWN_SCREEN)
                            hit.collider.gameObject.GetComponent<BeerCanMotion>().CanIsHit(hit, worldPos);
                        else
                            hit.collider.gameObject.GetComponent<BeerCanMotion>().StartCanPyramidIsHit(hit, worldPos);
                        hasHit = true;
                    }
                }
                else if (hit.collider.gameObject.name.Contains("Star") && !objectsHit.Contains(hit.collider.gameObject))
                {
                    objectsHit.Add(hit.collider.gameObject);
                    CreatePopUpScoreUponBallHit(hit.point, ScorePopUp355, 1000);
                    CurrentlyVisibleTargets.Remove(hit.collider.gameObject);
                    hit.collider.gameObject.GetComponent<StarPathController>().IsHit(worldPos);
                    hasHit = true;
                }
                else if (hit.collider.gameObject.name.Contains("Dragon") && !objectsHit.Contains(hit.collider.gameObject))
                {
                    objectsHit.Add(hit.collider.gameObject);
                    MakeDragonReactToHit(hit.collider.gameObject);
                    hasHit = true;
                }
            }
        }


        // Also check ray originating from the circle's center        
        Vector3 hitPointCenter = screenPos;
        Vector3 worldPosCenter = Camera.main.ScreenToWorldPoint(hitPointCenter);
        Ray rayCenter = Camera.main.ScreenPointToRay(hitPointCenter);
        hits = Physics.RaycastAll(rayCenter);

        // Loop through all objects that have been hit by this ray
        foreach (RaycastHit hit in hits)
        {
            if (hit.collider.gameObject.name.Contains("Can") && !hit.collider.gameObject.name.Contains("Pyramid") && !objectsHit.Contains(hit.collider.gameObject))
            {
                if (!hit.collider.gameObject.name.Contains("Mega"))
                    objectsHit.Add(hit.collider.gameObject);

                if (hit.collider.gameObject.name.Contains("Mega"))
                {
                    CreatePopUpScoreUponBallHit(hit.point, ScorePopUp355, 355);
                    hit.collider.gameObject.GetComponent<MegaBeerCanMotion>().CanIsHit(hit, worldPosCenter);
                    hasHit = true;
                }
                else if (hit.collider.gameObject.name.Contains("Big"))
                {
                    CreatePopUpScoreUponBallHit(hit.point, ScorePopUp355, 650);
                    CurrentlyVisibleTargets.Remove(hit.collider.gameObject);
                    hit.collider.gameObject.GetComponent<LargeBeerCanMotion>().CanIsHit(hit, worldPosCenter);
                    hasHit = true;
                }
                else if (hit.collider.gameObject.name.Contains("Medium"))
                {
                    CreatePopUpScoreUponBallHit(hit.point, ScorePopUp355, 500);
                    CurrentlyVisibleTargets.Remove(hit.collider.gameObject);
                    hit.collider.gameObject.GetComponent<BeerCanMotion>().CanIsHit(hit, worldPosCenter);
                    hasHit = true;
                }
                else if (hit.collider.gameObject.name.Contains("Small"))
                {
                    CreatePopUpScoreUponBallHit(hit.point, ScorePopUp355, 355);
                    CurrentlyVisibleTargets.Remove(hit.collider.gameObject);
                    if (currentScreen == ScreenType.GAMEPLAY_SCREEN || currentScreen == ScreenType.COUNTDOWN_SCREEN)
                        hit.collider.gameObject.GetComponent<BeerCanMotion>().CanIsHit(hit, worldPosCenter);
                    else
                        hit.collider.gameObject.GetComponent<BeerCanMotion>().StartCanPyramidIsHit(hit, worldPosCenter);
                    hasHit = true;
                }
            }
            else if (hit.collider.gameObject.name.Contains("Star") && !objectsHit.Contains(hit.collider.gameObject))
            {
                objectsHit.Add(hit.collider.gameObject);
                CreatePopUpScoreUponBallHit(hit.point, ScorePopUp355, 1000);
                CurrentlyVisibleTargets.Remove(hit.collider.gameObject);
                hit.collider.gameObject.GetComponent<StarPathController>().IsHit(worldPosCenter);
                hasHit = true;
            }
            else if (hit.collider.gameObject.name.Contains("Dragon") && !objectsHit.Contains(hit.collider.gameObject))
            {
                objectsHit.Add(hit.collider.gameObject);
                MakeDragonReactToHit(hit.collider.gameObject);
                hasHit = true;
            }
        }

        return hasHit;
    }



    private IEnumerator ShowHitWave(Vector2 worldPos)
    {
        GameObject wave = Instantiate(HitWave, new Vector3(worldPos.x, worldPos.y, 0), HitWave.transform.rotation) as GameObject;
        wave.transform.localScale = new Vector3(BallImpactRadius / 3.5f, BallImpactRadius / 3.5f, BallImpactRadius / 3.5f);
        yield return null;
    }

    void PlayScoringSound()
    {
        //Camera.main.gameObject.GetComponent<AudioSource>().PlayOneShot(ScoringSFX[Random.Range(0, ScoringSFX.Count)],0.2f);
    }


    private void CreatePopUpScoreUponBallHit(Vector3 position, GameObject scoreToShow, int score)
    {
        float Xoffset;
        float Yoffset;
        float offset = 100.0f;
        float minDistanceBetweenScores = 100.0f;
        bool foundClosePosition;
        Vector3 positionFound = position;
        Debug.Assert(minDistanceBetweenScores >= offset, "Minimum distance condition should be bigger than offset created, otherwise will enter an infinite loop");
        do
        {
            foundClosePosition = false;
            foreach (Vector3 popUpPos in ScorePopUpPositions)
            {
                if (Vector3.Distance(popUpPos, position) < minDistanceBetweenScores)
                {
                    foundClosePosition = true;
                    positionFound = popUpPos;
                    break;
                }
            }
            if (foundClosePosition)
            {
                int randomXOffset = UnityEngine.Random.Range(0, 3);
                int randomYOffset = UnityEngine.Random.Range(0, 3);
                if (randomXOffset == 0)
                    Xoffset = offset;
                else if (randomXOffset == 1)
                    Xoffset = -offset;
                else
                    Xoffset = offset;

                if (randomYOffset == 0)
                    Yoffset = 0;
                else if (randomYOffset == 1)
                    Yoffset = -(offset * 0.7f);
                else
                    Yoffset = offset * 0.7f;

                position = new Vector3(positionFound.x + Xoffset, positionFound.y, -200);
                positionFound = position;
            }
        }
        while (foundClosePosition);


        GameObject pop = Instantiate(scoreToShow, new Vector3(position.x, position.y, -200), scoreToShow.transform.rotation) as GameObject;
        pop.GetComponent<ScorePopUpController>().SetText(score.ToString());
        ScorePopUpPositions.Add(position);
        pop.transform.SetParent(GetScreenWithType(ScreenType.FOREGROUND_CITY).transform.parent);
        pop.transform.localScale = Vector3.one;
        float scoreFloat = float.Parse(CurrentScore.text);
        PlayScoringSound();
        scoreFloat += score;
        CurrentScore.text = scoreFloat.ToString();
        HighScoreUi.Instance.CheckIfHighScoreBeaten(scoreFloat); // verify if the team as beaten the best score 
        ScorePopUpPositions.Remove(position);
    }

    private void MakeDragonReactToHit(GameObject dragon)
    {
        dragon.GetComponent<DragonPathManager>().IsFurious = true;
    }

    private void FeverMode()
    {
        if (spawOnceMegaCan)
        {
            int pickLeftOrRight = Mathf.RoundToInt(UnityEngine.Random.value);
            int randomIndex = UnityEngine.Random.Range(-100, 100);
            Vector3 spawnPosition = new Vector3(randomIndex, -300, 0);

            SpawnMegaBeerCan(spawnPosition, pickLeftOrRight == 0);
            if (MegaBeerSpawnTime.Count > 0)
                MegaBeerSpawnTime.Remove(MegaBeerSpawnTime[0]);

            DestroyCans("Mega");
            StopCoroutine(ManageSpawningOfObjects());         
            spawOnceMegaCan = false;
        }
    }

    //This method destroys all cans on the screen: 
    private void DestroyCans(string name = "")
    {
        GameObject target = null;
        for (int i = CurrentlyVisibleTargets.Count - 1; i >= 0; i--)
        {
            target = CurrentlyVisibleTargets[i];
            if (target != null)
            {
                if (!target.name.Contains(name.ToString()) || String.IsNullOrEmpty(name))
                {
                    CurrentlyVisibleTargets.RemoveAt(i);
                    Vector3 pos = target.transform.position;
                    Destroy(target);

                    if (target.name.Contains("Mega"))
                    { 
                        GameObject explode = Instantiate(explosionObjectPrefab, pos, Quaternion.identity) as GameObject;
                        Destroy(explode, 0.5f);
                    }

                    target = null;
                }
            }
            else 
                CurrentlyVisibleTargets.RemoveAt(i);
        }

        // Remove empty pyramid can parents at end of game
        foreach (Transform pyramidParent in GetComponentsInChildren<Transform>())
        {
            if (pyramidParent.name.Contains("Pyramid"))
            {
                Destroy(pyramidParent.gameObject);
            }
        }
    }
    #endregion
}
