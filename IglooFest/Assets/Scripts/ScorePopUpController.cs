﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;

public class ScorePopUpController : MonoBehaviour {

	public float TimeBeforDestruction;

	private Text pointsText;

	void Awake()
	{
		pointsText =this.GetComponent<Text> ();
	}

	public void SetText(string points)
	{
		pointsText.text = points;
	}

	// Use this for initialization
	IEnumerator Start () {
		this.transform.DOLocalMoveY (this.transform.position.y+50,0.2f).SetEase(Ease.InCubic);
		this.transform.DOScale(1.5f,0.3f).SetEase(Ease.InCubic);
		pointsText.DOFade (0, 0.3f).SetDelay(0.3f);
		yield return new WaitForSeconds(TimeBeforDestruction);
		Destroy(gameObject);
	}

}
