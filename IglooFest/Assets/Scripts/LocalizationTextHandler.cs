﻿using UnityEngine;
using System.Collections;

public class LocalizationTextHandler : MonoBehaviour {

    public LocalizationKey LocalizationKey;

	// Use this for initialization
	IEnumerator Start ()
    {
        while (LocalizationManager.Instance == null)
            yield return null;

        LocalizationManager.Instance.OnLanguageChange += ChangeTextAccordingToLanguage;
        ChangeTextAccordingToLanguage();
	}
	
	public void ChangeTextAccordingToLanguage()
    {
        if (GameManager.Instance.IsTextInCaps)
            GetComponent<UnityEngine.UI.Text>().text = LocalizationManager.Instance.GetTextWithKey(LocalizationKey).ToUpper();
        else
            GetComponent<UnityEngine.UI.Text>().text = LocalizationManager.Instance.GetTextWithKey(LocalizationKey).ToUpper();
    }
}
