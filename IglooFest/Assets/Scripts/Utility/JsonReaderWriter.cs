﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections;
using System.Collections.Generic;
using System.Text;
//using System.Reflection;
using System.IO;
using System;
using Pathfinding.Serialization.JsonFx;
using System.Linq;

public class JsonReaderWriter : MonoBehaviour {

	
	public T GetJsonFromFile<T>(string file)
	{
		TextAsset filedata = (TextAsset)Resources.Load(file, typeof(TextAsset));
		StringReader reader = new StringReader (filedata.text);
		string jsonText = reader.ReadToEnd();

		Debug.Log(jsonText);

		T paramDict =  JsonReader.Deserialize<T>(jsonText);
		//EntityData paramDict =  MiniJSON.Json.Deserialize (jsonText) as EntityData;

		reader.Close();
		return paramDict;
	}	



	public T[] GetJsonFromFileToList<T>(string file)
	{
		TextAsset filedata = (TextAsset)Resources.Load(file, typeof(TextAsset));
	

		StringReader reader = new StringReader (filedata.text);
		string jsonText = reader.ReadToEnd();

		T[] paramDict = JsonReader.Deserialize<T[]>(jsonText);
		reader.Close();

		return paramDict;
	}


	public List<T> GetJsonFromFileToList2<T>(string file)
	{
		TextAsset filedata = (TextAsset)Resources.Load(file, typeof(TextAsset));
		
		
		StringReader reader = new StringReader (filedata.text);
		string jsonText = reader.ReadToEnd();
		
		T[] paramDict = JsonReader.Deserialize<T[]>(jsonText);
		reader.Close();
		if (paramDict != null)
						return paramDict.ToList ();
				else
						return null;
	}


	private StringReader reader = null; 

	/// <summary>
	/// Gets the json from text. no return type for no use it as sample
	/// </summary>
	/// <param name="txtAsset">Text asset.</param>
	public void GetJsonFromText(TextAsset file)
	{


		reader = new StringReader(file.text);
		if ( reader == null )
		{
			Debug.Log("not found or not readable");
		}
		else
		{
			string txt;
			// Read each line from the file
			while ( (txt = reader.ReadLine()) != null )
				Debug.Log("-->" + txt);
		}
	}

	public T[] GetJsonFromTextToList<T>(string text)
	{
		return JsonReader.Deserialize<T[]>(text);
	}

	private const string FILE_PATH = @"/Assets/Resources/JSON/MyFileX.txt";

	public static string ReadContents(string Filepath)
	{
		string ret;
		
//		using(FileStream fs = new FileStream(Filepath, FileMode.Open))
//		{
//			BinaryReader fileReader = new BinaryReader(fs);
//			ret = fileReader.ReadString();
//			fs.Close();
//		}

		StreamReader reader = new StreamReader(Filepath);
		ret = reader.ReadToEnd();
		reader.Close ();
		return ret;
	}


	public static void SaveJsonToFile(string Filepath,string fileName,string data)
	{
		var streamWriter = new StreamWriter(Filepath + fileName + ".txt");
		streamWriter.Write(data);
		streamWriter.Close();
		#if UNITY_EDITOR
		//AssetDatabase.Refresh();
		#endif
		Debug.Log("End save");
	}

	//string  fileName = @"/MyFile.txt";
	public void SaveContents(string text)
	{
//		if (File.Exists(Environment.CurrentDirectory  +fileName))
//		{
//			Debug.Log(fileName+" already exists.");
//			return;
//		}
//		var sr = File.CreateText(Environment.CurrentDirectory+"/Assets/Resources/JSON" +fileName);
//		sr.WriteLine ("This is my file.");
//		sr.WriteLine ("I can write ints {0} or floats {1}, and so on.",
//		              1, 4.2);
//		sr.Close();
		using(FileStream fs = new FileStream(Environment.CurrentDirectory+FILE_PATH, FileMode.Create))
		{
			Debug.Log(text);

			BinaryWriter fileWriter = new BinaryWriter(fs);
			
			fileWriter.Write(text);
			
			fs.Close();

			Debug.Log("Sav end");

		}
	}  

	
}
