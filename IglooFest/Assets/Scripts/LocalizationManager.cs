﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum Language
{
    FRANCAIS,
    ENGLISH
}

public enum LocalizationKey
{
    GAME_INSTRUCTIONS,
    TEAM,
    NEXT_TEAM,
    CONGRATS,
    SCORE,
    LITERS,
    LEADERBOARD,
    DEBUG_TEAM_NAME,
    WAITING_FOR_TEAM,
    NO_SIDE_APP_TEAM_NAME
}

public class LocalizationManager : MonoBehaviour
{
    public static LocalizationManager Instance;
    
    public Language CurrentLanguage;

    public delegate void ChangeLanguage ();
    public event ChangeLanguage OnLanguageChange;

    public Dictionary<LocalizationKey, string> FrenchDict;
    public Dictionary<LocalizationKey, string> EnglishDict;

    // Use this for initialization
    void Start ()
    {
        Instance = this;

        FrenchDict = new Dictionary<LocalizationKey, string>();
        EnglishDict = new Dictionary<LocalizationKey, string>();

        FrenchDict.Add(LocalizationKey.GAME_INSTRUCTIONS, "Touchez une bière Sapporo pour démarrer la partie");
        EnglishDict.Add(LocalizationKey.GAME_INSTRUCTIONS, "Hit a Sapporo can to start the game");

        FrenchDict.Add(LocalizationKey.TEAM, "Équipe");
        EnglishDict.Add(LocalizationKey.TEAM, "Team");

        FrenchDict.Add(LocalizationKey.NEXT_TEAM, "Prochaine équipe : ");
        EnglishDict.Add(LocalizationKey.NEXT_TEAM, "Next team: ");

        FrenchDict.Add(LocalizationKey.CONGRATS, "Félicitations");
        EnglishDict.Add(LocalizationKey.CONGRATS, "Congratulations");

        FrenchDict.Add(LocalizationKey.SCORE, "Vous avez obtenu");
        EnglishDict.Add(LocalizationKey.SCORE, "You scored");

        FrenchDict.Add(LocalizationKey.LITERS, "points !");
        EnglishDict.Add(LocalizationKey.LITERS, "points!");

        FrenchDict.Add(LocalizationKey.LEADERBOARD, "CLASSEMENT");
        EnglishDict.Add(LocalizationKey.LEADERBOARD, "LEADERBOARD");

        FrenchDict.Add(LocalizationKey.DEBUG_TEAM_NAME, "Équipe Sapporo");
        EnglishDict.Add(LocalizationKey.DEBUG_TEAM_NAME, "Sapporo Team");

        FrenchDict.Add(LocalizationKey.NO_SIDE_APP_TEAM_NAME, "Votre équipe");
        EnglishDict.Add(LocalizationKey.NO_SIDE_APP_TEAM_NAME, "Your Team");

        FrenchDict.Add(LocalizationKey.WAITING_FOR_TEAM, "En attente d'une équipe pour jouer...");
        EnglishDict.Add(LocalizationKey.WAITING_FOR_TEAM, "Waiting for a team to join...");
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyUp(KeyCode.L))
            SwitchLanguage();
	}

    public void SwitchLanguage()
    {
        if (CurrentLanguage == Language.FRANCAIS)        
            CurrentLanguage = Language.ENGLISH;        
        else        
            CurrentLanguage = Language.FRANCAIS;        

        if (OnLanguageChange != null)
            OnLanguageChange();
    }

    public string GetTextWithKey(LocalizationKey localizationKey)
    {
        if (CurrentLanguage == Language.FRANCAIS)
            return FrenchDict[localizationKey];
        else
            return EnglishDict[localizationKey];
    }

}
