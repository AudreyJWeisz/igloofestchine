﻿using UnityEngine;
using System.Collections;
using System;

public class DebugPosition : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {
      GameInput.Instance.OnBallImpact.AddListener(OnBallHitScreen);
    }

    private void OnBallHitScreen(Vector3 position)
    {
        GameObject target = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        //Destroy(target, 2);
        DestroyImmediate(target.GetComponent<Collider>());
        
        target.transform.position = position;
        //target.transform.SetParent(transform, true);
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnDestroy()
    {
        GameInput.Instance.OnBallImpact.RemoveListener(OnBallHitScreen);
    }
}
