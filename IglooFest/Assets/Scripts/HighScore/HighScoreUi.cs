﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

public class HighScoreUi : MonoBehaviour {

	public Text HighScoreLabelText;
	public Text HighScorePointText;

	private float currentHighScore;

	public static HighScoreUi Instance;

	void Awake()
	{
		Instance = this;
	}

	public void SetHighScoreText()
	{
		currentHighScore= LeaderboardManager.Instance.GetHighScore();
        if (currentHighScore == 0)
            SetVisibility(false);
        else
        {
			HighScoreLabelText.text = "SCORE À BATTRE";
            HighScorePointText.text = currentHighScore.ToString();
            SetVisibility(true);
        }
    }


	public void SetVisibility(bool value)
	{
		HighScoreLabelText.gameObject.SetActive (value);
		HighScorePointText.gameObject.SetActive (value);
	}

    public void CheckIfHighScoreBeaten(float currentTeamScore)
    {
        if(currentHighScore<currentTeamScore)
             StartCoroutine(AnimBestScore());
    }

    IEnumerator AnimBestScore()
    {
        HighScoreLabelText.text = "MEILLEUR SCORE";
        yield return new WaitForSeconds(0.5f);
		currentHighScore = 999999999999999999999f; 
        HighScorePointText.gameObject.SetActive(false);
		this.transform.DOPunchScale (new Vector3 (1.05f, 1.05f, 1.05f), 0.3f,1,0.5f).OnComplete(CompletetPunchScale);
    }

	void CompletetPunchScale()
	{
		this.transform.localScale = new Vector3 (1, 1, 1);
	}
}
