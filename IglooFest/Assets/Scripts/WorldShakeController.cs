﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class WorldShakeController : MonoBehaviour {

	public Transform cityFront;
	public Transform cityBack;

	public static WorldShakeController Instance;

	void Awake()
	{
		Instance = this;
	}

	public void SmallSkake()
	{
		cityBack.DOShakePosition (0.3f, 1.4f).SetDelay(0.1f);
		cityFront.DOShakePosition (0.3f, 1.4f);
	}

	public void SmallSkake2()
	{
		cityBack.DOShakePosition (0.3f, 1.6f).SetDelay(0.1f);
		cityFront.DOShakePosition (0.3f, 1.6f);
	}

	public void BigSkake()
	{
		cityBack.DOShakePosition (0.8f, 2f).SetDelay(0.1f);
		cityFront.DOShakePosition (0.8f, 2f);
	}
}
