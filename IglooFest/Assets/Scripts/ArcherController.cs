﻿using UnityEngine;
using System.Collections;

public class ArcherController : MonoBehaviour {

    public float TimeBeforeDestruction = 8f;
    public bool IsPlacedOnRightSide = false;

    public AudioClip BowSFX;
    private float destructionDelayCounter;     
    private bool hasThrownStar = false;

    private Animator archerAnim;

	public GameObject audioPrefab;
    
    void Awake ()
    {
        archerAnim = GetComponent<Animator>();
    }

    void Start ()
    {
        if (IsPlacedOnRightSide)
            GetComponent<AudioSource>().panStereo = 0.5f;
        else
            GetComponent<AudioSource>().panStereo = -0.5f;
	
    }

    // called by anim event
    void DestroyArcher()
    {        
        GameManager.Instance.numberOfArcherSpawned--;
        Destroy(gameObject);
    }


    void PlayBowSFX()
    {
		GameObject audiopref = GameObject.Instantiate (audioPrefab, this.transform.position, Quaternion.identity) as GameObject;
		audiopref.GetComponent<AudioSource>().PlayOneShot(BowSFX);
		audiopref.GetComponent<AutoDestroy> ().TimeBeforDestruction = 7;
		Invoke ("ThrowStar", 0.2f);
    }

    private void ThrowStar ()
    {
        hasThrownStar = true;
        GameObject star = Instantiate(GameManager.Instance.StarPrefab, new Vector3(transform.position.x, transform.position.y, 0), GameManager.Instance.StarPrefab.transform.rotation) as GameObject;
        GameManager.Instance.numberOfStarSpawned++;
        GameManager.Instance.CurrentlyVisibleTargets.Add(star);
        star.GetComponent<StarPathController>().IsGoingToTheRight = !IsPlacedOnRightSide;
        star.GetComponent<StarPathController>().HasBeenSpawnedByGeisha = false;
        star.transform.SetParent(GameManager.Instance.GetScreenWithType(ScreenType.GAMEPLAY_SCREEN).gameObject.transform);        
    }
}
