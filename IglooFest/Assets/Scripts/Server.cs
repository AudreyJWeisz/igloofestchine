﻿using UnityEngine;
using System.Collections;
using System.Net.Sockets;
using System.Net;
using System.Text;
using System.Runtime.InteropServices;
using System;

public class Server : MonoBehaviour
{
	public static Server singleton;

    private Socket m_Socket;

    ArrayList m_Connections = new ArrayList();

    ArrayList m_Buffer = new ArrayList();
    ArrayList m_ByteBuffer = new ArrayList();
	public string Myiplocal;

    void Awake ()
    {
        m_Socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp); 
		IPEndPoint ipLocal = new IPEndPoint(GetLocalIP(), 5000);
		Debug.Log("this computer's ip" +GetLocalIP());
		Myiplocal = ipLocal.ToString ();
        m_Socket.Bind(ipLocal);

        //start listening...
        m_Socket.Listen(100);
        singleton = this;
    }

	public static IPAddress GetLocalIP() 
	{
		IPAddress ipv4Address = IPAddress.Any;

		foreach (IPAddress currentIPAddress in Dns.GetHostAddresses(Environment.MachineName))
		{
			if (currentIPAddress.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
			{
				ipv4Address = currentIPAddress;
				break;
			}
		}

		return ipv4Address;
	}

    void OnApplicationQuit ()
    {
        Cleanup();
    }

    void Cleanup ()
    {
        if (m_Socket != null)
            m_Socket.Close();
        m_Socket = null;

        foreach (Socket con in m_Connections)
            con.Close();
        m_Connections.Clear();
    }
    ~Server ()
    {
        Cleanup();
    }

    void Update ()
    {
        // Accept any incoming connections!
        ArrayList listenList = new ArrayList();
        listenList.Add(m_Socket);
        Socket.Select(listenList, null, null, 1000);
        for (int i = 0; i < listenList.Count; i++)
        {
            Socket newSocket = ((Socket)listenList[i]).Accept();
            m_Connections.Add(newSocket);
            m_ByteBuffer.Add(new ArrayList());
            Debug.Log("Did connect");
        }

        // Read data from the connections!
        if (m_Connections.Count != 0)
        {
            ArrayList connections = new ArrayList(m_Connections);
            Socket.Select(connections, null, null, 1000);
            // Go through all sockets that have data incoming!
            foreach (Socket socket in connections)
            {
                byte[] receivedbytes = new byte[512];

                ArrayList buffer = (ArrayList)m_ByteBuffer[m_Connections.IndexOf(socket)];
                int read = socket.Receive(receivedbytes);
                for (int i = 0; i < read; i++)
                    buffer.Add(receivedbytes[i]);

                while (true && buffer.Count > 3)
                {
                    int length = BitConverter.ToInt32((byte[])buffer.ToArray(typeof(byte)), 0);
                    if (length + 4 <= buffer.Count)
                    {
                        ArrayList thismsgBytes = new ArrayList(buffer);
                        thismsgBytes.RemoveRange(length + 4, thismsgBytes.Count - (length + 4));
                        thismsgBytes.RemoveRange(0, 4);
                        if (thismsgBytes.Count != length)
                            Debug.Log("Bug");

                        buffer.RemoveRange(0, length + 4);
                        byte[] readbytes = (byte[])thismsgBytes.ToArray(typeof(byte));

                        MessageData readMsg = MessageData.FromByteArray(readbytes);
                        m_Buffer.Add(readMsg);

                        //Debug.Log(System.String.Format("Message {0}: {1}, {2}", readMsg.stringData, readMsg.mousex, readMsg.mousey));

                        if (singleton != this)
                            Debug.Log("Bug");
                    }
                    else
                        break;
                }

                // string output = Encoding.UTF8.GetString(bytes);
            }
        }
    }

    static public MessageData PopMessage ()
    {
        if (singleton.m_Buffer.Count == 0)
        {
            return null;
        }
        else
        {
            MessageData readMsg = null;
            try
            {
                readMsg = (MessageData)singleton.m_Buffer[0];
                singleton.m_Buffer.RemoveAt(0);                
            }
            catch (SocketException e)
            {
                Debug.Log("caught another exception!! :( OH NOES!! " + e.Message);
            }

            return readMsg;
        }
    }
}