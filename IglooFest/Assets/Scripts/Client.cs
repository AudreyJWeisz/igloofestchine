﻿using UnityEngine;
using System.Collections;
using System.Net.Sockets;
using System.Net;
using System;

public class Client : MonoBehaviour
{
    public string deviceName = "";
    public const int kPort = 10253;
    public static Client singleton;

    private int connectionAttempts = 0;
    private Socket m_Socket;

    private bool needsToResendMessage = false;
    private MessageData messageToResend = null;


    void Awake ()
    {
        DontDestroyOnLoad(transform.gameObject);
        singleton = this;
    }

    IEnumerator Start ()
    {
        m_Socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

        while (string.IsNullOrEmpty(deviceName))
            yield return null;
        TryToConnectToServer();
    }


    public void Send (MessageData msgData)
    {
        if (singleton.m_Socket == null)
            return;

        messageToResend = msgData;

        try
        {
            byte[] sendData = MessageData.ToByteArray(msgData);
            byte[] prefix = BitConverter.GetBytes(sendData.Length);
            singleton.m_Socket.Send(prefix);
            singleton.m_Socket.Send(sendData);
        }
        catch (SocketException e)
        {
            needsToResendMessage = true;
            Debug.Log("An exception occurred!!! :(  Message is as follows: " + e.Message);
            if (!GameManager.Instance.IsDebugMode && !GameManager.Instance.IsWithoutSideAppMode)
                TryToConnectToServer();
        }
    }


    void TryToConnectToServer ()
    {
        connectionAttempts++;
        try
        {
            // System.Net.PHostEntry ipHostInfo = Dns.Resolve("host.contoso.com");
            // System.Net.IPAddress remoteIPAddress = ipHostInfo.AddressList[0];  
            System.Net.IPAddress remoteIPAddress = IPAddress.Parse(deviceName);
            System.Net.IPEndPoint remoteEndPoint = new System.Net.IPEndPoint(remoteIPAddress, kPort);
            m_Socket.Connect(remoteEndPoint);
            if (GameManager.Instance.ConnectionErrorPanel.activeSelf)
                GameManager.Instance.ConnectionErrorPanel.SetActive(false);

            if (needsToResendMessage && messageToResend != null)
            {
                needsToResendMessage = false;
                Send(messageToResend);
                messageToResend = null;
            }
            connectionAttempts = 0;
        }
        catch (Exception e)
        {
            if (connectionAttempts < 100)
            {
                GameManager.Instance.ConnectionErrorPanel.SetActive(true);
                GameManager.Instance.ConnectionErrorPanel.GetComponentInChildren<UnityEngine.UI.Text>().text = "Connexion Lost, trying to reconnect...";
                GameManager.Instance.StopAllCoroutines();
                m_Socket.Close();
                m_Socket = null;
                m_Socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                StartCoroutine(WaitForOneSecondAndTryAgain());
                Debug.Log(e.Message);
            }
            else
            {
                Debug.Log(e.Message);
            }                
        }        
    }


    void OnApplicationQuit ()
    {
        m_Socket.Close();
        m_Socket = null;
    }


    private IEnumerator WaitForOneSecondAndTryAgain ()
    {
        yield return new WaitForSeconds(1);
        TryToConnectToServer();
    }

}