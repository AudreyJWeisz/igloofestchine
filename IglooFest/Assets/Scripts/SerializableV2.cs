﻿using UnityEngine;
using System.Collections;
using System.Runtime.Serialization;
using System;

[Serializable]
public class SerializableV2 : ISerializable  {

    public Vector2 V;

    private const string XValueID = "XValue";
    private const string YValueID = "YValue";

   

    public SerializableV2(Vector2 v)
    {
        this.V = v;
    }

    public SerializableV2(SerializationInfo info, StreamingContext context)
    {
        V.x = (float)info.GetValue(XValueID, typeof(float));
        V.y = (float)info.GetValue(YValueID, typeof(float));
    }

    public void GetObjectData(SerializationInfo info, StreamingContext context)
    {
        info.AddValue(XValueID, V.x);
        info.AddValue(YValueID, V.y);
    }
}
