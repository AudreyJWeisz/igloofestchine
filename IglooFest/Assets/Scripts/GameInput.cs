﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Events;
using MathNet.Numerics.LinearAlgebra.Double;

[System.Serializable]
public class V3Event: UnityEvent<Vector3> { }

public class GameInput : MonoBehaviour {

    public static GameInput Instance;

    [SerializeField]
    public V3Event OnBallImpact;
    
    public bool UseBallDetection = true;
    public float DistanceToTargets = 900;
    public float SideMultiplier = 1f;

    private const string SIDE_MULTIPLIER_ID = "SideMultiplier";

	// Use this for initialization
	void Awake ()
    {
        Instance = this;
    }

    void Start()
    {
        if (PlayerPrefs.HasKey(SIDE_MULTIPLIER_ID))
            SideMultiplier = PlayerPrefs.GetFloat(SIDE_MULTIPLIER_ID);
        //if (UseBallDetection)
           // TrackerManager.Instance.OnBallImpact.AddListener(OnBallHit);
    }

    public void TestHit(Vector3 position)
    {

        OnBallImpact.Invoke(position);
    }

    private void OnBallHit(Vector3 position)
    {
        position.x = Mathf.Clamp(position.x, 100, Screen.width-100);
        position.y = Mathf.Clamp(position.y, 100, Screen.height-100);

        if (!float.IsNaN(position.x))
        {
            Vector3 ballWorldPos = Camera.main.ScreenToWorldPoint(new Vector3(position.x, position.y, DistanceToTargets));
            OnBallImpact.Invoke(ballWorldPos);
        }
    }

    // Update is called once per frame
    void Update ()
    {
        if (Input.GetMouseButtonUp(0))
        {
            Vector3 mousePosWorld = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, DistanceToTargets));
            OnBallImpact.Invoke(mousePosWorld);
        }

        if (Input.GetKeyUp(KeyCode.X))
        {
            ScreenType screen = GameManager.Instance.currentScreen;
            if (screen == ScreenType.START_SCREEN || screen == ScreenType.LEADERBOARD_SCREEN)
            { 
                GameManager.Instance.IsWithoutSideAppMode = true;
                GameManager.Instance.RemoveTeamNames();
                GameManager.Instance.IsDebugMode = false;
            }
        }

        if (Input.GetKeyUp(KeyCode.Z))
        {
            ScreenType screen = GameManager.Instance.currentScreen;
            if (screen == ScreenType.LEADERBOARD_SCREEN)
                GameManager.Instance.StartNextGame();
        }

        if (Input.GetKeyUp(KeyCode.Q))
        {
            Application.Quit();
        }

        if (Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.L))
        {
            SideMultiplier += 0.01f;
            PlayerPrefs.SetFloat(SIDE_MULTIPLIER_ID, SideMultiplier);
            Debug.Log("Side multiplier:"+SideMultiplier);
        }

        if (Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.K))
        {
            SideMultiplier -= 0.01f;
            PlayerPrefs.SetFloat(SIDE_MULTIPLIER_ID, SideMultiplier);
            Debug.Log("Side multiplier:" + SideMultiplier);
        }

        if (Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.J))
        {
            SideMultiplier = 1.0f;
            PlayerPrefs.SetFloat(SIDE_MULTIPLIER_ID, SideMultiplier);
            Debug.Log("Side multiplier:" + SideMultiplier);
        }



        if (Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.LeftControl)  && Input.GetKeyDown(KeyCode.D))
        {
            Application.LoadLevel("ballDetection");
        }
        if (Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.H))
        {
            Application.LoadLevel("HomographyCalibration");
        }

    }
}
