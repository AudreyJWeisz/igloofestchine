/* Copyright (c) 2017 ExT (V.Sigalkin) */

using UnityEngine;
using System.Collections.Generic;
using extOSC;


    public class LazerScanInputController : MonoBehaviour
    {

        public GameInput gameinput;

        #region Private Vars

        private OSCTransmitter _transmitter;

        private OSCReceiver _receiver;

        private const string _oscAddress = "/scanblob/testunity";               // Also, you cam use mask in address: /example/*/

        private List<hitPointTime> hitlist;
        const float maxDistBetweenHit = 120;
        public float DistanceToTargets = 900;
        #endregion

        #region Unity Methods

        void Awake()
        {
            hitlist = new List<hitPointTime>();
        }

        protected virtual void Start()
        {
            // Creating a transmitter.
            _transmitter = gameObject.AddComponent<OSCTransmitter>();

            // Set remote host address.
            _transmitter.RemoteHost = "127.0.0.1";

            // Set remote port;
            _transmitter.RemotePort = 7001;


            // Creating a receiver.
            _receiver = gameObject.AddComponent<OSCReceiver>();

            // Set local port.
            _receiver.LocalPort = 7001;

            // Bind "MessageReceived" method to special address.
            _receiver.Bind(_oscAddress, MessageReceived);
            InvokeRepeating("tryRemoveOldHit", 0.1f, 0.1f);
        }

        #endregion

        #region Protected Methods

        protected void MessageReceived(OSCMessage message)
        {
            Vector2 value;

            if (message.ToVector2(out value))
            {
                if (value == Vector2.zero) return;
                // Debug.Log(value);
                // look if value is in the hit list 
                bool nearesthitfound = false;
                Vector3 hitPosWorld = Camera.main.ScreenToWorldPoint(new Vector3((value.x * Screen.width) / 100, (value.y * Screen.height) / 100, Mathf.Abs(Camera.main.transform.position.z)));


                foreach (hitPointTime v in hitlist)
                {
                    if ((hitPosWorld - v.hitpoint).magnitude < maxDistBetweenHit)
                    {
                        nearesthitfound = true;
                        break;
                    }
                }

                // trigger hit only if the hit is not near and other one 
                if (!nearesthitfound)
                {
                    Debug.Log("hit" + hitPosWorld);
                    hitlist.Add(new hitPointTime(hitPosWorld, Time.time));
                    gameinput.TestHit(hitPosWorld);
                }

            }
        }

        void tryRemoveOldHit()
        {
            foreach (hitPointTime v in hitlist)
            {
                if (Time.time - v.time >= 0.5f)
                {
                    hitlist.Remove(v);
                    Debug.Log("remove");
                    return;
                }
            }

            #endregion
        }

        public struct hitPointTime
        {
            // variables
            public Vector3 hitpoint;
            public float time;

            public hitPointTime(Vector3 mHitpoint, float mTime)
            {
                hitpoint = mHitpoint;
                time = mTime;
            }
        }
    }

