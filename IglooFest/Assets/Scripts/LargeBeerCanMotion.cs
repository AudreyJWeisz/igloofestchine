﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LargeBeerCanMotion : MonoBehaviour {

    public bool IsGoingToTheRight;
    
    public List<AudioClip> HitSFXs;

    private float DelayBeforeDestroyOnceInvisible = 2.0f;

    private float invisibilityCounter;
    

    private float initialZpos;

    private bool isCurrentlyVisible = false;

    private Rigidbody beerBody;
    private Vector2 speed;

    private bool hasBeenHit = false;

    void Start ()
    {
        initialZpos = transform.position.z;
        float horizontalForce = 0;
        GetComponent<AudioSource>().clip = HitSFXs[Random.Range(0, HitSFXs.Count)];
        GetComponent<AudioSource>().pitch = Random.Range(0.85f, 1.15f);

        beerBody = GetComponent<Rigidbody>();
        
        invisibilityCounter = 0;                
        
        speed = GameParameters.Instance.LargeBeerSpeed;

        float verticalForce = Random.Range((speed.x / 2.5f), (speed.y / 2.5f));

        if (IsGoingToTheRight)
        {
            horizontalForce = Random.Range(speed.x, speed.y);
            float angle = Vector3.Angle(new Vector3(horizontalForce, 0, 0), new Vector3(horizontalForce, verticalForce, 0));
            gameObject.transform.eulerAngles = new Vector3(0, 0, -angle);            
        }
        else
        {
            horizontalForce = Random.Range(-speed.x, -speed.y);
            float angle = Vector3.Angle(new Vector3(Mathf.Abs(horizontalForce), 0, 0), new Vector3(Mathf.Abs(horizontalForce), verticalForce, 0));
            gameObject.transform.eulerAngles = new Vector3(0, 0, angle);            
        }        

        beerBody.AddForce(new Vector3(horizontalForce, verticalForce, 0), ForceMode.Impulse);

        beerBody.constraints = RigidbodyConstraints.FreezePositionZ;
    }

    private void FixedUpdate()
    {
        if (hasBeenHit)
        {
            beerBody.AddForce(new Vector3(0, -15, 0), ForceMode.VelocityChange);
        }
    }

    void Update ()
    {        
        if (this.gameObject.transform.GetComponent<MeshRenderer>().isVisible)
            isCurrentlyVisible = true;
        else
            isCurrentlyVisible = false;

        // Detect that the log is no longer seen by the camera, remove it from the list, and destroy it.
        if (!isCurrentlyVisible)
            invisibilityCounter += Time.deltaTime;
        else
            invisibilityCounter = 0;

        if (beerBody.velocity != new Vector3(0, 0, 0))
            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(beerBody.velocity, Vector3.down), Time.deltaTime * (speed.x / 3.0f));

        if (invisibilityCounter > DelayBeforeDestroyOnceInvisible)
        {
            GameManager.Instance.CurrentlyVisibleTargets.Remove(this.gameObject);
            Destroy(this.gameObject);
        }
    }

    public void CanIsHit(RaycastHit hit, Vector3 worldPos)
    {
		WorldShakeController.Instance.SmallSkake2 ();
        // Play the hit SFX
        GetComponent<AudioSource>().Play();
        // TODO enable physic behavior
        beerBody.constraints = RigidbodyConstraints.None;
        //gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(0, 0, 10000));
        beerBody.AddForceAtPosition(new Vector3(0, 0, 1) * GameParameters.Instance.ImpactForce * 10, worldPos);
        GetComponent<BoxCollider>().enabled = false;
        hasBeenHit = true;
        // destroy the can after the SFX
        Destroy(gameObject, GetComponent<AudioSource>().clip.length - 0.1f);
    }
}
