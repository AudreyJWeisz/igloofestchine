﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Text;
using System;

public class SpawnerTextLoader : MonoBehaviour {

    public static SpawnerTextLoader Instance;

	// Use this for initialization
	void Awake () {
        Instance = this;
	}

    public bool Load (string fileName)
    {
        // Handle any problems that might arise when reading the text
        try
        {
            string line;
            // Create a new StreamReader, tell it which file to read and what encoding the file
            // was saved as
            StreamReader theReader = new StreamReader(fileName, Encoding.Default);
            // Immediately clean up the reader after this block of code is done.
            // You generally use the "using" statement for potentially memory-intensive objects
            // instead of relying on garbage collection.
            // (Do not confuse this with the using directive for namespace at the 
            // beginning of a class!)
            using (theReader)
            {
                // While there's lines left in the text file, do this:
                do
                {
                    line = theReader.ReadLine();

                    if (!string.IsNullOrEmpty(line))
                    {
                        // Do whatever you need to do with the text line, it's a string now
                        // In this example, I split it into arguments based on comma
                        // deliniators, then send that array to DoStuff()
                        string spawnerType = line.Split(':')[0];
                        string[] timeValues = (line.Split(':')[1]).Split(',');

                        if (spawnerType.Contains("Geisha") && timeValues.Length > 0)
                            GameManager.Instance.InitializeGeishaTimers(timeValues);
                        else if (spawnerType.Contains("Archer") && timeValues.Length > 0)
                            GameManager.Instance.InitializeArcherTimers(timeValues);
                        else if (spawnerType.Contains("Dragon") && timeValues.Length > 0)
                            GameManager.Instance.InitializeDragonTimers(timeValues);
                        else if (spawnerType.Contains("Small") && timeValues.Length > 0)
                            GameManager.Instance.InitializeSmallBeerTimers(timeValues);
                        else if (spawnerType.Contains("Medium") && timeValues.Length > 0)
                            GameManager.Instance.InitializeMediumBeerTimers(timeValues);
                        else if ((spawnerType.Contains("Large") || spawnerType.Contains("Big")) && timeValues.Length > 0)
                            GameManager.Instance.InitializeLargeBeerTimers(timeValues);
                        else if (spawnerType.Contains("Medium") && timeValues.Length > 0)
                            GameManager.Instance.InitializeMegaBeerTimers(timeValues);
                    }
                }
                while (line != null);
                // Done reading, close the reader and return true to broadcast success    
                theReader.Close();
                return true;
            }
        }
        // If anything broke in the try block, we throw an exception with information
        // on what didn't work
        catch (Exception e)
        {
            Debug.Log("spawner text loader error : " + e.Message);
            //Console.WriteLine("{0}\n", e.Message);
            return false;
        }
    }

}
