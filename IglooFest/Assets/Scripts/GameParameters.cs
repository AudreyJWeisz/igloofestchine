﻿using UnityEngine;
using System.Collections;
using System.Xml.Serialization;
using System.IO;
using System;

[System.Serializable]
public struct SerializedData
{
    public float BallImpactRadius; // how large is the area of impact and the matching FX 
    public int SubImpactsInRadius; // How many point of impact are considered in the impact radius (each point will have the force below applied)
    public float ImpactForce;  // Force applied to the 3d objects hit
    public Vector2 MegaBeerSpeed; // min/max speed for the mega beer can
    public Vector2 LargeBeerSpeed; // min/max speed for the large beer can
    public Vector2 MediumBeerSpeed; // min/max speed for the medium beer can
    public Vector2 StarFlightLenght; // how long the star's trajectory will take (min/max)
    public float StarHoveringTime; // How long the star stops in the middle of it's trajectory
    public float  DragonFlightLenght; // flight time for the dragon
    public float DragonAngrySpeed; // multiplier of the flight time remaining when the dragon is angry (hit by a ball), 
}

public class GameParameters : MonoBehaviour
{
    public static GameParameters Instance;

    // variables from the xml
    public float BallImpactRadius = 30;
    public float ImpactForce = 500;
    public int SubImpactsInRadius = 8;
    public Vector2 MegaBeerSpeed = new Vector2(1, 1);
    public Vector2 LargeBeerSpeed = new Vector2(10, 15);
    public Vector2 MediumBeerSpeed = new Vector2(10, 15);
    public Vector2 StarFlightLenght = new Vector2(8, 12);
    public float StarHoveringTime = 1f;
    public float DragonFlightLenght = 30f;
    public float DragonAngrySpeed;

    // private var
    private const string FILE_NAME = "/gameParams.xml";

    // Use this for initialization
    void Awake()
    {
        Instance = this;
        if (!File.Exists(Application.streamingAssetsPath + FILE_NAME))
        {
            SaveData();
        }
        LoadData();
    }

    public void LoadData()
    {
        XmlSerializer serializer = new XmlSerializer(typeof(SerializedData));
        using (StreamReader wr = new StreamReader(Application.streamingAssetsPath + FILE_NAME))
        {
            SerializedData readData = (SerializedData)serializer.Deserialize(wr);
            BallImpactRadius = readData.BallImpactRadius;
            ImpactForce = readData.ImpactForce;
            SubImpactsInRadius = readData.SubImpactsInRadius;
            MegaBeerSpeed = readData.MegaBeerSpeed; 
            LargeBeerSpeed = readData.LargeBeerSpeed;
            MediumBeerSpeed = readData.MediumBeerSpeed;
            StarFlightLenght = readData.StarFlightLenght;
            StarHoveringTime = readData.StarHoveringTime;
            DragonFlightLenght = readData.DragonFlightLenght;
            DragonAngrySpeed = readData.DragonAngrySpeed;
        }
    }

    public void SaveData()
    {
        SerializedData data = new SerializedData()
        {
            BallImpactRadius = BallImpactRadius,
            ImpactForce = ImpactForce,
            SubImpactsInRadius = SubImpactsInRadius,
            MegaBeerSpeed = MegaBeerSpeed,
            LargeBeerSpeed = LargeBeerSpeed,
            MediumBeerSpeed = MediumBeerSpeed,
            StarHoveringTime = StarHoveringTime,
            StarFlightLenght = StarFlightLenght,
            DragonFlightLenght = DragonFlightLenght,
            DragonAngrySpeed = DragonAngrySpeed
        };

        XmlSerializer serializer = new XmlSerializer(typeof(SerializedData));
        using (StreamWriter wr = new StreamWriter(Application.streamingAssetsPath + FILE_NAME))
        {
            serializer.Serialize(wr, data);
        }
    }

    // weight the time left and points scored to determine how many points should be on screen
    public int PointsRequiredOnScreen(float timeLeftPercent, int currentScore)
    {
        return 1000;
    }
}
