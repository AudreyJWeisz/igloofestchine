﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AudioManager : MonoBehaviour {

    public AudioClip IntroLoop, Countdown, GameLoop;

    private AudioSource audioSource;
    private float volume;
    private Coroutine audioTransition;

	// Use this for initialization
	void Awake () {
        GameManager.Instance.IntroStart += TransitionToIntroLoop;
        GameManager.Instance.CountDownStart += TransitionToCountDown;
        GameManager.Instance.GameStart += TransitionToGameLoop;
        GameManager.Instance.GameStart += TransitionToGameLoop;
        audioSource = GetComponent<AudioSource>();
        volume = audioSource.volume;

    }
	
	// Update is called once per frame
	void FixedUpdate () {

	
	}

    private void  TransitionToIntroLoop ()
    {
        //Debug.Log("TransitionToIntroLoop ");
        if (audioTransition!= null) StopCoroutine(audioTransition);
        audioTransition = StartCoroutine(ChangeMusicLoop(0.1f, IntroLoop));
    }

    private void TransitionToCountDown()
    {
        //Debug.Log("TransitionToCountDown");
        if (audioTransition != null) StopCoroutine(audioTransition);
        audioTransition = StartCoroutine(ChangeMusicLoop(0.1f, Countdown));

    }

    private void TransitionToGameLoop ()
    {
        //Debug.Log("TransitionToGameLoop");
        if (audioTransition != null) StopCoroutine(audioTransition);
        audioTransition = StartCoroutine(ChangeMusicLoop(0.1f, GameLoop));
    }

    IEnumerator ChangeMusicLoop (float fadeStep, AudioClip newClip)
    {
        while (audioSource.volume > 0.05f)
        {
            audioSource.volume = Mathf.Lerp(audioSource.volume, 0.0f, fadeStep);
            yield return new WaitForFixedUpdate() ;
        }
        audioSource.clip = newClip;
        audioSource.volume = volume;
        audioSource.Play();
        yield return null;
    }
}
