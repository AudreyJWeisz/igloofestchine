﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DragonPathManager : MonoBehaviour {
    
    public bool IsGoingToTheRight = false;
    public bool IsFurious = false;
    public float TimeToCompletePath = 30.0f;
    
    public List<AudioClip> DragonHit;

    private iTween.EaseType EaseTypeForAnimationCurve = iTween.EaseType.linear;

    private bool hasDroppedCanPyramid = false;    
    private string pathName;
    private int randomStringIndex;
    
    void Awake()
    {
        iTween.Init(gameObject);
    }

    void Start ()
    {
        randomStringIndex = Random.Range(0, 3);        
        TimeToCompletePath = GameParameters.Instance.DragonFlightLenght;
        if (randomStringIndex == 0)
            pathName = "DragonPath1";
        else if (randomStringIndex == 1)
            pathName = "DragonPath2";
        else
            pathName = "DragonPath3";        

        if (IsGoingToTheRight)
        {
            iTween.PutOnPath(gameObject, iTweenPath.GetPath(pathName), 1);
            iTween.MoveTo(gameObject, iTween.Hash("oncomplete", "OnCompletePath", "oncompletetarget", gameObject, "path", iTweenPath.GetPathReversed(pathName), "time", TimeToCompletePath, "delay", 0.0f, "easetype", EaseTypeForAnimationCurve, "name", "DragonNotAngry"));
        }            
        else
        {
            iTween.PutOnPath(gameObject, iTweenPath.GetPath(pathName), 0);
            iTween.MoveTo(gameObject, iTween.Hash("oncomplete", "OnCompletePath", "oncompletetarget", gameObject, "path", iTweenPath.GetPath(pathName), "time", TimeToCompletePath, "delay", 0.0f, "easetype", EaseTypeForAnimationCurve, "name", "DragonNotAngry"));
        }
            
    }

    void Update()
    {   
        if (IsFurious)
        {
            if (!hasDroppedCanPyramid)
                DropCanPyramid();
            SpeedUpDragon();
        }           

        if (GameManager.Instance.currentScreen == ScreenType.SCORE_SCREEN)
            OnCompletePath();
    }


    private void SpeedUpDragon ()
    {
        iTween ITSP = (iTween)this.gameObject.GetComponent(typeof(iTween));
        if (ITSP == null)
        {
            if (IsGoingToTheRight)
                iTween.MoveTo(gameObject, iTween.Hash("oncomplete", "OnCompletePath", "oncompletetarget", gameObject, "position", new Vector3(900, transform.position.y, transform.position.z), "speed", GameParameters.Instance.DragonAngrySpeed, "easetype", "easeInSine", "name", "DragonAngry"));
            else
                iTween.MoveTo(gameObject, iTween.Hash("oncomplete", "OnCompletePath", "oncompletetarget", gameObject, "position", new Vector3(-900, transform.position.y, transform.position.z), "speed", GameParameters.Instance.DragonAngrySpeed, "easetype", "easeInSine", "name", "DragonAngry"));
        }
        else if (ITSP._name == "DragonNotAngry")
        {
            Destroy(ITSP);
            GetComponent<AudioSource>().PlayOneShot(DragonHit[Mathf.RoundToInt(Random.Range(0, DragonHit.Count))]);
            
            DragonBreath.SetActive(true);
        }

    }

    public GameObject DragonBreath;

    private void DropCanPyramid ()
    {
        if (transform.position.x > -700 && transform.position.x < 700)
        {
            hasDroppedCanPyramid = true;
            GameObject cans = Instantiate(GameManager.Instance.CanPyramidPrefab, new Vector3(transform.position.x, transform.position.y, 20), GameManager.Instance.CanPyramidPrefab.transform.rotation) as GameObject;
            cans.transform.SetParent(GameObject.FindGameObjectWithTag("GameManager").transform);
            foreach (Rigidbody can in cans.GetComponentsInChildren<Rigidbody>())
            {
                GameManager.Instance.CurrentlyVisibleTargets.Add(can.gameObject);
            }
        }              
    }


    public void OnCompletePath ()
    {
        GameManager.Instance.CurrentlyVisibleTargets.Remove(this.gameObject);
        GameManager.Instance.numberOfDragonsSpawned--;
        Destroy(this.gameObject);
    }
}
