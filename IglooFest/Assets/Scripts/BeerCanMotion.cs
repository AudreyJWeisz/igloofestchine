﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class BeerCanMotion : MonoBehaviour {    
    public bool IsGoingToTheRight;
    [HideInInspector]
    public bool ZaxisLocked = true;
    public List<AudioClip> HitSFXs;
    private float DelayBeforeDestroyOnceInvisible = 2.0f;

    private float invisibilityCounter;
    
    private float initialZpos;

    private bool isCurrentlyVisible = false;
    private float anglePart;

    private Rigidbody beerBody;

    private bool hasBeenHit = false;
    public bool initialScale = false;
    private Vector3 originalScale;



    void Start ()
    {
        anglePart = Mathf.PI * 2 / GameParameters.Instance.SubImpactsInRadius;
        initialZpos = transform.position.z;
        float horizontalForce = 0;
        GetComponent<AudioSource>().clip = HitSFXs[Random.Range(0, HitSFXs.Count)];
        GetComponent<AudioSource>().pitch = Random.Range(0.95f, 1.05f);
        invisibilityCounter = 0;        

        Vector2 speed = GameParameters.Instance.MediumBeerSpeed;

        float speedDelta = Mathf.Abs(speed.y - speed.x);        
        float positionRelativeToBottom = (transform.position.y + 379.0f) / (379.0f + 328.0f); // -379 is the lowest y global position of all the spawn locations and 328 is the highest        
        horizontalForce = speed.y - (speedDelta * positionRelativeToBottom);

        beerBody = GetComponent<Rigidbody>();

        if (initialScale)
        {
            originalScale = this.gameObject.transform.localScale = new Vector3(0, 0, 0);
            transform.DOScale(new Vector3(1, 1, 1), 0.5f);
        }

        if (!transform.parent.name.Contains("CanPyramid"))
        {
            if (IsGoingToTheRight)
            {
                //horizontalForce = Random.Range(speed.x, speed.y);
                gameObject.transform.eulerAngles = new Vector3(Random.Range(-2.0f, 2.0f), 0, Random.Range(-45.0f, 0.0f));
            }
            else
            {
                horizontalForce *= -1;
                gameObject.transform.eulerAngles = new Vector3(Random.Range(-2.0f, 2.0f), 0, Random.Range(0.0f, 45.0f));
            }

            float verticalForce = Mathf.Abs(horizontalForce)/2.5f;

            beerBody.AddForce(new Vector3(horizontalForce, verticalForce, 0), ForceMode.Impulse);
        }
        else
        {
            beerBody.useGravity = false;
        }

        beerBody.constraints = RigidbodyConstraints.FreezePositionZ;
        //Debug.Log("position y = " + transform.position.y + ", force horizontale: " + horizontalForce + ", a changé de signe? " + !IsGoingToTheRight);
    }

    private void FixedUpdate()
    {
        if (hasBeenHit)
        {
            beerBody.AddForce(new Vector3(0, -15, 0), ForceMode.VelocityChange);
        }
    }

    void Update ()
    {
        if (this.gameObject.transform.GetComponent<MeshRenderer>().isVisible)
            isCurrentlyVisible = true;
        else
            isCurrentlyVisible = false;

        // Detect that the log is no longer seen by the camera, remove it from the list, and destroy it.
        if (!isCurrentlyVisible)
            invisibilityCounter += Time.deltaTime;
        else
            invisibilityCounter = 0;

        if (invisibilityCounter > DelayBeforeDestroyOnceInvisible)
        {
            GameManager.Instance.CurrentlyVisibleTargets.Remove(this.gameObject);

            Destroy(this.gameObject);
        }
    }

    public void CanIsHit(RaycastHit hit, Vector3 worldPos)
    {
        // Play the hit SFX
        GetComponent<AudioSource>().Play();
		WorldShakeController.Instance.SmallSkake ();
        // TODO enable physic behavior
        beerBody.constraints = RigidbodyConstraints.None;
        //gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(0, 0, 500),ForceMode.Impulse);

        beerBody.AddForceAtPosition(new Vector3(0, 0, 1) * GameParameters.Instance.ImpactForce * 10, worldPos);
        beerBody.useGravity = true; // only for pyramid
        beerBody.mass *= 100;

        hasBeenHit = true;
        GetComponent<BoxCollider>().enabled = false;
        // destroy the can after the SFX
        Destroy(gameObject, GetComponent<AudioSource>().clip.length - 0.1f);
    }


    public void StartCanPyramidIsHit (RaycastHit hit, Vector3 worldPos)
    {      
		WorldShakeController.Instance.SmallSkake ();
        // Play the hit SFX
        GetComponent<AudioSource>().Play();
        // TODO enable physic behavior
        beerBody.constraints = RigidbodyConstraints.None;

        Rigidbody canBeerBody;
        
        foreach (Transform can in transform.parent)
        {
            canBeerBody = can.gameObject.GetComponent<Rigidbody>();            
            canBeerBody.AddExplosionForce(100, worldPos, 1000, 0, ForceMode.Impulse);
            can.GetComponent<BoxCollider>().enabled = false;
        }

        beerBody.useGravity = true; // only for pyramid
        // destroy the can after the SFX
        Destroy(gameObject, GetComponent<AudioSource>().clip.length - 0.1f);
    }
}
