﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.IO;
using System;
using Pathfinding.Serialization.JsonFx;

public class ResendFailedMailController : MonoBehaviour {

	public ResendFailedMailUI resendFailedMailUi; // cleaner way is to use MVC with delegate (but we are in rush)

    private int failedVideoCount;
	private int resentVideoCount;

	void Update()
	{
		if (Input.GetKey (KeyCode.LeftShift) && Input.GetKeyDown (KeyCode.R))
			SearchForFailedSend ();
	}


	void SearchForFailedSend()
	{
		resendFailedMailUi.SetVisibilyty (true);
		StartCoroutine (SendFailedMail ());
	}

	IEnumerator SendFailedMail()
	{
		resendFailedMailUi.button.SetActive (false); // prevent user to close the UI wile process is running
		Debug.Log ("SendFailedMail");
		failedVideoCount = 0;
		resentVideoCount = 0;
		resendFailedMailUi.textResult.text = "Search for failed vidéo";
			
		string projectFolder = System.IO.Path.GetFullPath(System.IO.Path.Combine(Application.dataPath, ".."));
		string videoPath = System.IO.Path.Combine(projectFolder, CameraRecordSystem.Instance.videoDirectoryName);
		// first look at all folder and search for json file 
		string[] directories = Directory.GetDirectories(videoPath);

		string[] fileinDirectory;
		foreach(string directory in directories)
		{
			fileinDirectory = Directory.GetFiles (directory);
			foreach(string file in fileinDirectory)
			{
				string[] splited= file.Split('\\');
               // if txt meaning that there is a failed video there 
               if(splited[splited.Length-1] == "infoSav.txt")
	            {
	                failedVideoCount++;
					//get team information in the file
					TeamInformationData teamInfos = JsonReader.Deserialize<TeamInformationData>(JsonReaderWriter.ReadContents(file));
					// change value to be able to send proper file
					CameraRecordSystem.Instance.currentTeamInfos = teamInfos;
					CameraRecordSystem.Instance.currentFullDataPath = directory;

					File.Delete (file);// delete json data because we are trying to resend it
					// send process 
					yield return StartCoroutine(GifUploadToImgur.Instance.SendToimgurCoroutine(CameraRecordSystem.Instance.currentFullDataPath + "/" + CameraRecordSystem.Instance.currentTeamInfos.teamName + ".gif"));

					while (!GifUploadToImgur.Instance.didfinished)
						yield return null;
					
					if (GifUploadToImgur.Instance.succed)
					{
						yield return StartCoroutine(SendMail.Instance.SendCoroutine(directory+"/"+teamInfos.teamName+".mp4",CameraRecordSystem.Instance.currentTeamInfos, GifUploadToImgur.Instance.linkFileImgur));
			

						while (!SendMail.Instance.didFinished)
							yield return null;

						if(SendMail.Instance.success)
							resentVideoCount++;
					}
	            }
            }
		}

		if(failedVideoCount>0)
			resendFailedMailUi.textResult.text =  ("Retrieved " + failedVideoCount + " failed video, successfully resent "+resentVideoCount+" video");
		else 
			resendFailedMailUi.textResult.text = "noFailed video";
		resendFailedMailUi.button.SetActive (true);
	}




}
