﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ResendFailedMailUI : MonoBehaviour {


	public Text textResult;
	public GameObject button;

	public void Hide()
	{
		SetVisibilyty (false);
	}


	public void SetVisibilyty(bool value)
	{
		this.gameObject.SetActive (value);
	}

}
