﻿using UnityEngine;
using System.Collections;
using System;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

public class SendMail : MonoBehaviour {


    public static SendMail Instance;
    [HideInInspector]
    public bool success;
	public bool didFinished;
    void Awake() { Instance = this; }


    void Update()
    {
        // test shortcut
        if (Input.GetKeyUp(KeyCode.A))
        {
            //Send ("","","","", "http://i.imgur.com/9Q6hKdg.gifv");
        }
    }


    public void Send(string attachementPath, TeamInformationData teamInfos, string Imgurvideolink)
    {
        StartCoroutine(SendCoroutine(attachementPath, teamInfos, Imgurvideolink));
    }


    public IEnumerator SendCoroutine(string attachementPath, TeamInformationData teamInfos, string Imgurvideolink)
    {
        success = false;
		didFinished = false;
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            FailSend();
            yield break;
        }

        MailMessage mail = new MailMessage();

        mail.From = new MailAddress("chasselacanette@gmail.com");
        mail.To.Add(teamInfos.teamEmail);
        mail.Bcc.Add("chasselacanette@gmail.com"); // bcc own mail to get some sav data and live update
        mail.Subject = "Sapporo hunt resultat";
        mail.IsBodyHtml = true;

        string shareText = "Je viens tout juste de participer au jeu Chasse la canette de Sapporo à Igloofest! #sapporo #igloofestMTL";
        string facebookText = "http://www.facebook.com/sharer/sharer.php?u=" + Imgurvideolink + "&caption=sapporo&text=" + WWW.EscapeURL(shareText);
        string twitterText  = "https://twitter.com/intent/tweet?url=" + Imgurvideolink + "&text=" + WWW.EscapeURL(shareText);


		mail.Body = "<html><body><table width=\"850\" ><tr><td width=\"250\" ><IMG src=\"http://playmind.com/wp-content/uploads/2017/01/bigbox_sapporo300x600_dragon-fr__1_.jpg\"></IMG></td><td width=\"550\">" +
            "<table>"+
            "<tr><td><b><font size=\"5\" > Voici un petit souvenir de votre passage à Igloofest!</font></b><br><br>Utilisez les liens ci-dessous afin de partager votre gif officiel du jeu Chasse la canette de Sapporo:<br><br></td></tr>" +
            "<tr><td><table><tr><td><a href=\" " + facebookText + " \"><IMG SRC=\"http://playmind.com/wp-content/uploads/2017/01/LogoFb.gif\" alt=\"Logo\"></IMG></a></td><td><a href=\" " + facebookText + " \">Partage sur facebook</a></td></tr></table></td></tr>" +
            "<tr><td><table><tr><td><a href=\" " + twitterText + " \"><IMG SRC=\"http://playmind.com/wp-content/uploads/2017/01/LogoTwitter.gif\" alt=\"Logo\"></IMG></a></td><td><a href=\" " + twitterText + " \">Partage sur Twitter<br></td></tr></table></td></tr>" +
            "<tr><td><table><tr><td><a href=\" " + Imgurvideolink + " \"><IMG SRC=\"http://playmind.com/wp-content/uploads/2017/01/LogoImgur.gif\" alt =\"Logo\"></IMG></a></td><td><a href=\" " + Imgurvideolink + " \">Lien vers la video</a></td></tr></table></td></tr>" +
            "<tr><td><br>Merci d'avoir participé et bonne chance!</td></tr></table>" +
            "</td></tr></table></body></html>";
      
        string attachmentPath = attachementPath;
        //@"C:\Test.mp4"
        //System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(attachmentPath);
        //mail.Attachments.Add(attachment);
        // share image attachement
        //System.Net.Mail.Attachment attachmentlogo = new System.Net.Mail.Attachment(System.IO.Path.GetFullPath(System.IO.Path.Combine(Application.dataPath, ".."))+ "/LogoFB.gif");
        //mail.Attachments.Add(attachmentlogo);

        SmtpClient smtpServer = new SmtpClient("smtp.gmail.com");
        smtpServer.Port = 587;
        smtpServer.Credentials = new System.Net.NetworkCredential("chasselacanette@gmail.com", "vt6ch99lRtb") as ICredentialsByHost;
        smtpServer.EnableSsl = true;
        ServicePointManager.ServerCertificateValidationCallback =
        delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        { return true; };

        try
        {
            object userstate = mail;
            smtpServer.SendAsync(mail, userstate);
            smtpServer.SendCompleted += new SendCompletedEventHandler(SendCompleteCallBack);
            Debug.Log("mail send end");
        }
        catch
        {
            Debug.Log("Error No internet connection");
            FailSend();
        }

        // cancel send if take more than 5s
        yield return new WaitForSeconds(5f);
        if (!success) {
            smtpServer.SendAsyncCancel();
            FailSend();
        }
    }


    private void SendCompleteCallBack(object sender,System.ComponentModel.AsyncCompletedEventArgs arg)
    {
        success = true;
		didFinished = true;
		Debug.Log("mail success");
    } 

    private void FailSend()
    {
		Debug.Log ("Fail send mail");
		didFinished = true;
        success = false;
        CameraRecordSystem.Instance.SaveTeamInfoToJsonFile();
    }

 
}