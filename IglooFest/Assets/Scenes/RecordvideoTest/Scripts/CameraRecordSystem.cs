﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.IO;
using System;
using Pathfinding.Serialization.JsonFx;
using System.Text.RegularExpressions;
using UTJ;

public class CameraRecordSystem : MonoBehaviour {

    public static CameraRecordSystem Instance;

    public Text scoreText;
    public Text teamText;
    [HideInInspector]
    public TeamInformationData currentTeamInfos; // information of the current team video record 
	[HideInInspector]
	public string currentFullDataPath;
	[HideInInspector]
	public string currentDataPath; // data path from current project folder
	[HideInInspector]
	public string videoDirectoryName = "TeamVideos";

	public PictureMotion pictureMotionForVideo;
	const string prefRecordType = "RecordType";
	public static int recordType;

    void Awake()
	{
		Instance = this;
	}

	void Start()
	{
		if (PlayerPrefs.HasKey (prefRecordType))
			recordType = PlayerPrefs.GetInt (prefRecordType);
	}

    public void Update()
    {
        /*
        if (Input.GetKeyUp(KeyCode.Z)) {
            RecordAndSendFramedVideo("123", new TeamInformationData("Debug", "eddie@playmind.com", "eddie", "playmind", true));
        }
        */

		if (Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.N))
		{
			SavRecordSystemType(0);
		}
		if (Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.M))
		{
			SavRecordSystemType(1);
		}
    }


	private void SavRecordSystemType(int type)
	{
		recordType = type;
		PlayerPrefs.SetInt (prefRecordType,type);
		PlayerPrefs.Save ();
	}


	public void RecordPlay(TeamInformationData teamInfos)
	{
		// stop here if player are not ok for video recoding
		if (!IsCurrentTeamokToRecordVideo (teamInfos))
			return;
		
		TakePictureController.singleton.RecordPlay ();
	}


	public void RecordAndSendFramedVideo(string score, TeamInformationData teamInfos)
	{
		// stop here if player are not ok for video recoding
		if (!IsCurrentTeamokToRecordVideo (teamInfos))
			return;
		
		scoreText.text = score;
		teamText.text = teamInfos.teamName;
        currentTeamInfos = teamInfos;

        // create directory for the video
        string projectFolder = System.IO.Path.GetFullPath(System.IO.Path.Combine(Application.dataPath, ".."));
  
        string videoPath = System.IO.Path.Combine(projectFolder, videoDirectoryName);
        // check if the video directory exist if not creae it 
        if (!Directory.Exists(videoPath))
            Directory.CreateDirectory(videoPath);

        // create team video folder 
        string folderName = teamInfos.teamName + "_" + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString("D2") + "-" + DateTime.Now.Day.ToString("D2") + "_" + DateTime.Now.Hour.ToString() + "-" + DateTime.Now.Minute.ToString();
        currentFullDataPath = System.IO.Path.Combine(videoPath, folderName);
        currentDataPath = videoDirectoryName + "/" + folderName;
        Directory.CreateDirectory(currentFullDataPath);

        currentTeamInfos.score = score; // save the score in the information 
		TakePictureController.singleton.StartMovieFramedRecord(currentFullDataPath);
    }



    public void SaveTeamInfoToJsonFile()
    {
        JsonReaderWriter.SaveJsonToFile(currentFullDataPath,"/infoSav",JsonWriter.Serialize(currentTeamInfos));
    }


	public bool IsCurrentTeamokToRecordVideo(TeamInformationData teamInfos)
	{
		if (!teamInfos.isOkWithTerms)
			return false;
		if (teamInfos.teamEmail == "" || teamInfos.teamEmail == "NONE")
			return false;

			return true;
	}
}
