﻿using UnityEngine;
using System.Collections;

public class PlayVideo : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	void Update () {
		if (Input.GetButtonDown ("Jump")) {
			/*
			Renderer r = GetComponent<Renderer>();
			MovieTexture movie = (MovieTexture)r.material.mainTexture;


			if (movie.isPlaying) {
				movie.Pause();
			}
			else {
				movie.Play();
			}
			*/

			StartCoroutine(loadAndPlay());
		}
	}


	IEnumerator loadAndPlay(){

		Debug.Log("loadand");
		WWW diskMovieDir = new WWW("file:///C:/yourvideoDir/video.mp4");

		//Wait for file finish loading
		while(!diskMovieDir.isDone){
			yield return null;
		}

		Debug.Log (diskMovieDir.bytes);

		//Save the loaded movie from WWW to movetexture
		MovieTexture movieToPlay = diskMovieDir.GetMovieTexture();

		//Hook the movie texture to the current renderer
		MeshRenderer ren = this.GetComponent<MeshRenderer>();
		ren.material.mainTexture = movieToPlay ;    

		movieToPlay.Play();
	}



}
