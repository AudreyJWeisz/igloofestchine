﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net;
using System.IO;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System;
using System.Text;
using System.Xml.Linq;
using Pathfinding.Serialization.JsonFx;
using System.Linq;

public class ImgurUploadInformation{

	public string teamName;
	public string deletehash;

	public ImgurUploadInformation (){
	}

	public ImgurUploadInformation(string inTeamName,string inDeletehash )
	{
		teamName = inTeamName;
		deletehash = inDeletehash;
	}

}
	

public class GifUploadToImgur : MonoBehaviour {

	public string filename;
    public static GifUploadToImgur Instance;

    public string linkFileImgur;
    public bool succed;
	public bool error;
	public bool didfinished;
	string path;


    void Awake()
    {
        Instance = this;
		path= System.IO.Path.GetFullPath(System.IO.Path.Combine(Application.dataPath, ".."));
    }

	void Update()
	{
	//	if (Input.GetKeyUp (KeyCode.W))
		//	SendGif ();
	}


	public void SendGif(string filepath)
	{
		Debug.Log ("StartSendGif");
		StartCoroutine (SendToimgurCoroutine (filepath));
	}


	public IEnumerator SendToimgurCoroutine(string filepath)
	{
		Debug.Log ("Send to Imgur");
        if (Application.internetReachability == NetworkReachability.NotReachable)
        { 
            UploadFail();
            Debug.Log("No internet connection");
            yield break;
        }
        succed = false;
		didfinished = false;
		error=false;

        //Make sure that the file save properly
        //		float startTime = Time.time;
        //		while (false == File.Exists(Application.dataPath + filename))
        //		{
        //			if (Time.time - startTime > 5.0f)
        //			{
        //				yield break;
        //			}
        //			yield return null;
        //		}

		//string fileFolder = System.IO.Path.GetFullPath(System.IO.Path.Combine(Application.dataPath, ".."));
         // =  System.IO.Path.Combine(fileFolder, filename);

		//Read the saved file back into bytes
		byte[] rawImage = File.ReadAllBytes(filepath);

		//Before we try uploading it to Imgur we need a Server Certificate Validation Callback
		ServicePointManager.ServerCertificateValidationCallback = MyRemoteCertificateValidationCallback;

		//Attempt to upload the image
		using (var w = new WebClient())
		{
			string clientID = "046dd18591aafed";
			w.Headers.Add("Authorization", "Client-ID " + clientID);
			var values = new NameValueCollection
			{
				{ "image", Convert.ToBase64String(rawImage) },
				{ "type", "base64" },
			};
			w.UploadValuesCompleted += new UploadValuesCompletedEventHandler (UploadComplete);
			try { 
				
				//byte[] response = w.UploadValues("https://api.imgur.com/3/image.xml", values);
				w.UploadValuesAsync(new Uri("https://api.imgur.com/3/image.xml"), values);
                // CALL fail for timeout 
			}
			catch (Exception s)
			{
                Debug.Log("error imgur"+s);
                UploadFail();
			}
        }

        // after 5s if no succes we stop all 
		yield return new WaitForSeconds(20f);
        if (!succed)
            UploadFail();
    }

	public void UploadComplete(object sender, UploadValuesCompletedEventArgs e)
	{
		if (didfinished)return;
        Debug.Log("UploadComplete");
		XDocument result = XDocument.Load(new MemoryStream(e.Result));
		//Debug.Log(result);
		linkFileImgur = result.Element("data").Element("gifv").Value;
		SavDeletHash (result.Element("data").Element("deletehash").Value); // sav delet hash just in case we want to delete file

		succed = true;
		didfinished = true;
	}

    public void UploadFail()
    {
		Debug.Log ("UploadFail");
        succed = false;
        error = true;
        didfinished = true;
        CameraRecordSystem.Instance.SaveTeamInfoToJsonFile();
    }

	public bool MyRemoteCertificateValidationCallback(System.Object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
	{
		bool isOk = true;
		// If there are errors in the certificate chain, look at each error to determine the cause.
		if (sslPolicyErrors != SslPolicyErrors.None)
		{
			for (int i = 0; i < chain.ChainStatus.Length; i++)
			{
				if (chain.ChainStatus[i].Status != X509ChainStatusFlags.RevocationStatusUnknown)
				{
					chain.ChainPolicy.RevocationFlag = X509RevocationFlag.EntireChain;
					chain.ChainPolicy.RevocationMode = X509RevocationMode.Online;
					//chain.ChainPolicy.UrlRetrievalTimeout = new TimeSpan(0, 1, 0);
					chain.ChainPolicy.VerificationFlags = X509VerificationFlags.AllFlags;
					bool chainIsValid = chain.Build((X509Certificate2)certificate);
					if (!chainIsValid)
					{
						isOk = false;
					}
				}
			}
		}
		return isOk;
	}

	// save deletehash from imgur if in any case we want to delet some of the sended gif
	public void SavDeletHash(string deleteHash)
	{

		List<ImgurUploadInformation> data;

		if (File.Exists (path + "/imgurDeletHash"))
		{
			ImgurUploadInformation[]   tmpdata = JsonReader.Deserialize<ImgurUploadInformation[]>(JsonReaderWriter.ReadContents (path + "/imgurDeletHash.txt"));
			data = tmpdata.ToList ();
		}
		else 
			data = new List<ImgurUploadInformation>();

		data.Add (new ImgurUploadInformation (CameraRecordSystem.Instance.currentTeamInfos.teamName,deleteHash));

		JsonReaderWriter.SaveJsonToFile(path,"/imgurDeletHash",JsonWriter.Serialize(data));
	}
}
