﻿using UnityEngine;
using UnityEngine.UI; 
using System.Collections;
using System.Collections.Generic;

public class PictureMotion : MonoBehaviour {

	public static PictureMotion Instance;
	public bool isSpriteRenderer=true;
	private SpriteRenderer spRenderer;
	private Image UIimage;


	public float intervalBframe=0.5f;

	List<Sprite> sprites;
	private int index=0;
	private bool forward = true;

	void Awake()
	{
		Instance = this;
		spRenderer = this.GetComponent<SpriteRenderer> ();
		UIimage = this.GetComponent<Image> ();
	}


	public void PlayAnimation(List<Sprite> inSprites)
	{
		sprites = inSprites;
		StartCoroutine ("Animation");
	}

	public void StopAnimation ()
	{
		StopCoroutine ("Animation");
	}


	IEnumerator Animation()
	{
		while(true)
		{
			if(isSpriteRenderer)
				spRenderer.sprite = sprites[incrementIndex()];
			else
				UIimage.sprite = sprites[incrementIndex()]; 
			yield return new WaitForSeconds (intervalBframe);
		}
	}


	private int incrementIndex()
	{
		if(forward)
		{	
			index++;
			if (index >= sprites.Count)
			{	
				index --;
				forward = false;
			}

			return index;
		}
		else 
		{	
			index--;
			if (index < 0)
			{	
				index ++;
				forward = true;
			}
			return index;
		}
	}


}
